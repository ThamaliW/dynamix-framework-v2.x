/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import org.ambientdynamix.api.application.BundleContextInfo;
import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextPluginInformationResult;
import org.ambientdynamix.api.application.DynamixFeature;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextInfo;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.PluginConstants.LogPriority;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Base class for various ContextPluginRuntimes, which represents entities that performs the actual work of context
 * interactions within the Dynamix Framework. Each ContextPluginRuntime operate in conjunction with an associated
 * ContextPlugin that provides meta-data describing the plugin's various attributes.
 *
 * @author Darren Carlson
 */
public abstract class ContextPluginRuntime {
    // Private data
    private final String TAG = this.getClass().getSimpleName();
    private IPluginEventHandler eventHandler;
    private IPluginFacade pluginFacade;
    private UUID sessionId;

    /**
     * Called once when the ContextPluginRuntime is first initialized. The implementing subclass should acquire the
     * resources necessary to run, but should not start running yet (wait for start to be called). If initialization is
     * unsuccessful, the plug-ins should throw an exception and release any acquired resources.
     */
    public abstract void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception;

    /**
     * Called by Dynamix to start the plug-in. Once started, the plug-in should begin performing its context interaction
     * tasks, and/or be ready to handle calls to 'handleContextRequest' and/or 'handleConfiguredContextRequest' (if
     * supported).
     */
    public abstract void start() throws Exception;

    /**
     * Called by Dynamix to stop context interaction tasks. Ongoing context requests should be cancelled (e.g., by
     * calling 'sendContextRequestError' for each ongoing request; however, acquired resources should be maintained,
     * since start may be called again.
     */
    public abstract void stop() throws Exception;

    /**
     * Called by Dynamix to stop the runtime (if necessary) and release all acquired resources in preparation for
     * garbage collection. Once this method has been called, this plug-in instance may not be re-started and will be
     * reclaimed by garbage collection sometime in the indefinite future.
     */
    public abstract void destroy() throws Exception;

    /**
     * Sets this runtime's PowerScheme to the incoming value. Note that runtimes must be capable of dynamically
     * adjusting their power consumption to match new PowerScheme values (which may change over time).
     *
     * @param scheme The new PowerScheme to apply.
     */
    public abstract void setPowerScheme(PowerScheme scheme) throws Exception;

    /**
     * Called when new ContextPluginSettings are available for the runtime. When this method is called, the runtime
     * should immediately change its runtime behavior to match the new settings (if started). New settings may be
     * provided by external means, for example from a 2d-barcode.
     *
     * @param settings The new ContextPluginSettings to apply.
     */
    public void updateSettings(ContextPluginSettings settings) throws Exception {
        // Base implementation, override as needed
    }

    /**
     * Returns the default configuration view, or null if no view is available.
     */
    public IPluginView getDefaultConfigurationView() {
        // Base implementation, override as needed
        return null;
    }

    /**
     * Returns the configuration view for the specified viewConfig Bundle, or null if no view is available.
     */
    public IPluginView getConfigurationView(Bundle viewConfig) {
        // Base implementation, override as needed
        return null;
    }

    /**
     * Performs a single context interaction for the specified requestId. Results for this method should be delivered
     * using the standard 'sendContextEvent' methods (or 'sendContextRequestSuccess' / 'sendContextRequestError'),
     * providing the originating requestId as the responseId. This method may be called simultaneously by multiple
     * threads, so care should be taken to always handle responses using the originating requestId of a given request
     * thread. Importantly, if 'stop' or 'destroy' are called, the runtime MUST terminate all request processing
     * immediately (and cancel outstanding requests).
     * <p/>
     * Note: Implementations should endeavor to conserve local resources and battery power according to the current
     * PowerScheme (which may change over time).
     * <p/>
     * Note: Implementations must be able to handle multiple context requests simultaneously.
     * <p/>
     * Note: This method will be called on a dedicated thread, so it may block (if needed).
     * <p/>
     * Note: If not overridden, the base implementation calls 'sendContextRequestError' with ErrorCodes.NOT_SUPPORTED.
     *
     * @param requestId   The request identifier to be included in the results
     * @param contextType The context type for this request
     */
    public void handleContextRequest(UUID requestId, String contextType) {
        // Base implementation, override as needed
        sendContextRequestError(requestId, "handleContextRequest is NOT_SUPPORTED by this plug-in",
                ErrorCodes.NOT_SUPPORTED);
    }

    /**
     * Performs a single context interaction for the specified requestId, using the specified request configuration.
     * Results for this method should be delivered using the standard 'sendContextEvent' methods (or
     * 'sendContextRequestSuccess' / 'sendContextRequestError'), providing the originating requestId as the responseId.
     * This method may be called simultaneously by multiple threads, so care should be taken to always handle responses
     * using the originating requestId of a given request thread. Importantly, if 'stop' or 'destroy' are called, the
     * runtime MUST terminate all request processing immediately (and cancel outstanding requests) according to the
     * semantics defined in ContextPluginRuntime.
     * <p/>
     * Note: Implementations should endeavor to conserve local resources and battery power according to the current
     * PowerScheme (which may change over time).
     * <p/>
     * Note: Implementations must be able to handle multiple configured context requests simultaneously.
     * <p/>
     * Note: This method will be called on a dedicated thread, so it may block (if needed).
     * <p/>
     * Note: If not overridden, the base implementation calls 'sendContextRequestError' with ErrorCodes.NOT_SUPPORTED.
     *
     * @param requestId     The request identifier to be included in the results
     * @param contextType   The context type of this request
     * @param requestConfig An optional configuration Bundle that can be used to control the context interaction. The data in the
     *                      requestConfig Bundle plug-in dependent and must be carefully documented by the plug-in developer.
     */
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle requestConfig) {
        // Base implementation, override as needed
        sendContextRequestError(requestId, "handleConfiguredContextRequest is NOT_SUPPORTED by this plug-in",
                ErrorCodes.NOT_SUPPORTED);
    }

    /**
     * Returns the list of Dynamix features provided by this runtime. A Dynamix feature is a piece of runnable code that
     * plug-ins can provide to be executed directly within the Dynamix UI.
     */
    public List<DynamixFeature> getDynamixFeatures() {
        // Default is empty list
        return new ArrayList<DynamixFeature>();
    }

    /**
     * Returns true if this plug-in provides Dynamix features; false otherwise.
     */
    public boolean hasDynamixFeatures() {
        List<DynamixFeature> features = getDynamixFeatures();
        return features != null && features.size() > 0;
    }

    /**
     * Adds a context listener to receive ongoing context result events (e.g., pedometer info or media track position
     * updates). A context listener is of type 'ContextListenerInformation', which represents an entity listenering to
     * context listener a specific context type (and optional listener configuration). Context results for specific
     * listeners should be sent by calling any of the runtime's 'sendContextEvent' methods, passing in the listenerId as
     * the event's responseId. Context results can be broadcast to all listeners by calling any of the
     * 'sendBroadcastContextEvent' methods in this class.
     *
     * @param listener The context listener to add.
     * @return True if the listener was successfully added; false otherwise.
     */
    public boolean addContextlistener(ContextListenerInformation listener) {
        // Base implementation, override as needed
        return false;
    }

    /**
     * Removes a previously added context listener.
     *
     * @param listenerId The listener id to remove.
     * @return True if the subscription was removed; false otherwise.
     */
    public boolean removeContextListener(UUID listenerId) {
        // Base implementation, override as needed
        return false;
    }

    /**
     * Returns the Dynamix Framework version.
     */
    public VersionInfo getDynamixVersion() {
        try {
            return getPluginFacade().getDynamixFacade(getSessionId()).getDynamixVersion();
        } catch (RemoteException e) {
            Log.w(TAG, "Could not find Dynamix version!");
        }
        return null;
    }

    /**
     * Returns a list of all known context plug-ins (installed, installing and not installed).
     */
    public List<ContextPluginInformation> getAllContextPluginInformation() {
        try {
            return getPluginFacade().getDynamixFacade(getSessionId()).getAllContextPluginInformation()
                    .getContextPluginInformation();
        } catch (RemoteException e) {
            Log.w(TAG, "Could not find get all plug-in information!");
        }
        return new ArrayList<ContextPluginInformation>();
    }

    /**
     * Returns a list of all installed context plug-ins.
     */
    public List<ContextPluginInformation> getInstalledContextPluginInformation() {
        try {
            return getPluginFacade().getDynamixFacade(getSessionId()).getInstalledContextPluginInformation()
                    .getContextPluginInformation();
        } catch (RemoteException e) {
            Log.w(TAG, "Could not find get installed plug-in information!");
        }
        return new ArrayList<ContextPluginInformation>();
    }

    /**
     * Returns a list of all context plug-ins for the specified context type, or null if none can be found
     */
    public List<ContextPluginInformation> getAllContextPluginInformationForType(String contextType)
            throws RemoteException {
        ContextPluginInformationResult result = getPluginFacade().getDynamixFacade(getSessionId())
                .getAllContextPluginInformationForType(contextType);
        if (result != null)
            return result.getContextPluginInformation();
        else
            return new ArrayList<ContextPluginInformation>();
    }

    /**
     * Returns the list of current context listeners that are registered to this plug-in.
     */
    public List<ContextListenerInformation> getContextListeners() {
        return getPluginFacade().getContextListeners(getSessionId());
    }

    /**
     * Returns the list of current context listeners that are registered to this plug-in for the specified context type.
     */
    public List<ContextListenerInformation> getContextListeners(String contextType) {
        return getPluginFacade().getContextListeners(getSessionId(), contextType);
    }

    /**
     * Adds the Dynamix Application specified by the appDetails Bundle.
     *
     * @deprecated
     */
    public boolean addDynamixApplication(Bundle bundle) {
        return getPluginFacade().addDynamixApplication(getSessionId(), bundle);
    }

    /**
     * Returns this ContextPluginRuntime's associated IPluginFacade.
     *
     * @see IPluginFacade
     */
    public final IPluginFacade getPluginFacade() {
        return pluginFacade;
    }

    /**
     * Returns this ContextPluginRuntime's associated IDynamixFacade.
     *
     * @see IDynamixFacade
     */
    public final IDynamixFacade getDynamixFacade() {
        return pluginFacade.getDynamixFacade(getSessionId());
    }

    /**
     * Returns this ContextPluginRuntime's event handler.
     *
     * @see IPluginEventHandler
     */
    private final IPluginEventHandler getEventHandler() {
        return this.eventHandler;
    }

    /**
     * Utility method that returns the SecuredContext for this runtime.
     */
    public final Context getSecuredContext() {
        return getPluginFacade().getSecuredContext(getSessionId());
    }

    /**
     * Sets the ContextPluginRuntime's associated ContextPlugin to the configured or unconfigured state.
     *
     * @param configured True if the plugin is properly configured; false otherwise Returns true if the configuration status
     *                   was set; false otherwise.
     */
    public boolean setPluginConfiguredStatus(boolean configured) {
        return getPluginFacade().setPluginConfiguredStatus(getSessionId(), configured);
    }

    /**
     * Returns the ContextPluginSettings persisted for this runtime in the Dynamix Framework (may be null).
     */
    public final ContextPluginSettings getContextPluginSettings() {
        return getPluginFacade().getContextPluginSettings(getSessionId());
    }

    /**
     * Stores the ContextPluginSettings in the Dynamix Framework on behalf on the calling ContextPluginRuntime.
     *
     * @param settings The ContextPluginSettings to store Returns true if the settings were successfully stored; false
     *                 otherwise.
     */
    public final boolean storeContextPluginSettings(ContextPluginSettings settings) {
        return getPluginFacade().storeContextPluginSettings(getSessionId(), settings);
    }

    /**
     * Returns a serialized version of the IContextInfo in text format, using default encoding format (JSON).
     */
    public final String serializeIContextInfo(IContextInfo data) {
        return getPluginFacade().serializeIContextInfo(getSessionId(), data);
    }

    /**
     * Returns a serialized version of the IContextInfo in text format, using specified text-based encoding format.
     */
    public final String serializeIContextInfo(IContextInfo data, String format) {
        return getPluginFacade().serializeIContextInfo(getSessionId(), data, format);
    }

    /**
     * Returns the UUID session id that uniquely identifies a ContextPluginRuntime to the Dynamix Framework during
     * runtime. This UUID may change each time Dynamix starts.
     */
    public final UUID getSessionId() {
        return this.sessionId;
    }

    /**
     * Sends a logging event. Logging events are retained by the Dynamix Framework and not passed to applications.
     *
     * @param priority   The priority of the message.
     * @param logMessage The message content.
     */
    public final void sendLoggingEvent(LogPriority priority, String logMessage) {
        ContextInfoSet dataSet = ContextInfoSet.createLoggingContextInfoSet(priority, logMessage);
        if (getEventHandler() != null)
            getEventHandler().sendEvent(this, dataSet);
        else
            Log.w(TAG, "Can't sendContextEvent because the event handler was null");
    }

    /**
     * Sends a message Bundle to the latest plug-in.
     *
     * @param targetPluginId The id of the plug-in message receiver.
     * @param messageBundle  The message to send.
     * @param resultHandler  A handler for message results.
     * @return An IdResult indicating if the message was sent or not. If it was sent, the IdResult's id is the requestId
     * that can be used to differentiate associated results in an IMessageResultHandler.
     */
    public final IdResult sendMessage(String targetPluginId, Bundle messageBundle, IMessageResultHandler resultHandler) {
        return getPluginFacade().sendMessage(getSessionId(), targetPluginId, messageBundle, resultHandler);
    }

    /**
     * Sends a message Bundle to the specified plug-in.
     *
     * @param targetPluginId The id of the plug-in message receiver.
     * @param pluginVersion  The version of the plug-in message receiver.
     * @param messageBundle  The message Bundle to send.
     * @param resultHandler  A handler for message results (can be null).
     * @return An IdResult indicating if the message was sent or not. If it was sent, the IdResult's id is the requestId
     * that can be used to differentiate associated results in an IMessageResultHandler.
     */
    public IdResult sendMessage(String targetPluginId, String pluginVersion, final Bundle messageBundle,
                                final IMessageResultHandler resultHandler) {
        return getPluginFacade().sendMessage(getSessionId(), targetPluginId, pluginVersion, messageBundle,
                resultHandler);
    }

    /**
     * Sends a message Bundle to the latest plug-in without an IMessageResultHandler
     *
     * @param targetPluginId The id of the plug-in message receiver.
     * @param messageBundle  The message to send.
     * @return An IdResult indicating if the message was sent or not.
     */
    public final IdResult sendMessage(String targetPluginId, Bundle messageBundle) {
        return getPluginFacade().sendMessage(getSessionId(), targetPluginId, messageBundle, null);
    }

    /**
     * Sends a message Bundle to the specified plug-in without an IMessageResultHandler
     *
     * @param targetPluginId The id of the plug-in message receiver.
     * @param pluginVersion  The version of the plug-in receiver.
     * @param messageBundle  The message to send.
     * @return An IdResult indicating if the message was sent or not.
     */
    public final IdResult sendMessage(String targetPluginId, String pluginVersion, Bundle messageBundle) {
        return getPluginFacade().sendMessage(getSessionId(), targetPluginId, pluginVersion, messageBundle, null);
    }

    /**
     * Request the Dynamix open the specified IPluginView.
     */
    public Result openView(IPluginView view) {
        return getPluginFacade().openView(getSessionId(), view);
    }

    /**
     * Sends a PluginAlert to Dynamix, which will show it to the user if the plug-in has permission.
     *
     * @param alert The Bundle containing the alert. Note that Bundle keys must be set properly. For example, to show a
     *              toast alert to the user, the Bundle keys must be set as bundle.setString(PluginConstants.ALERT_TYPE,
     *              PluginConstants.ALERT_TYPE_TOAST) and bundle.setString(PluginConstants.ALERT_TOAST_VALUE,
     *              "Toast Value to Show") and bundle.setInt(PluginConstants.ALERT_TOAST_DURATION, 5000). See
     *              PluginConstants for other alert types (e.g., PluginConstants.ALERT_TYPE_OPEN_SETTINGS).
     * @return A Result indicating success or failure.
     */
    public Result sendPluginAlert(Bundle alert) {
        return getPluginFacade().sendPluginAlert(getSessionId(), alert);
    }

    /**
     * Adds a listener that will receive NFC events from Android
     *
     * @param listener The listener that should receive the event (as an Intent)
     * @return True if the listener was added; false otherwise.
     */
    public boolean addNfcListener(NfcListener listener) {
        return getPluginFacade().addNfcListener(getSessionId(), listener);
    }

    /**
     * Removes a previously added NfcListener
     *
     * @param listener The NfcListener that should be removed.
     * @return True if the listener was removed; false otherwise.
     */
    public boolean removeNfcListener(NfcListener listener) {
        return getPluginFacade().removeNfcListener(getSessionId(), listener);
    }

    /**
     * Perform a system-level operation on the Dynamix Framework instance. Note that plug-ins require system-level
     * permission to use this method. Not for general use.
     *
     * @param operationDetails An Android Bundle containing the configuration of the operation.
     */
    public Result dynamixSystemLevelOperation(Bundle operationDetails) {
        return getPluginFacade().dynamixSystemLevelOperation(getSessionId(), operationDetails);
    }

    /**
     * Perform a system-level operation on the Dynamix Framework instance. Note that plug-ins require system-level
     * permission to use this method. Not for general use.
     *
     * @param operationDetails An Android Bundle containing the configuration of the operation.
     * @param callback         A callback containing the result of the operation.
     */
    public void dynamixSystemLevelOperation(Bundle operationDetails, Callback callback){
        getPluginFacade().dynamixSystemLevelOperation(getSessionId(), operationDetails, callback);
    }

    /**
     * Adds a listener that will receive Dynamix events using a simple event type string.
     * See the IDynamixEventListener class for available strings. For more complex event
     * listener setup, use the 'addDynamixEventListener', which takes a Bundle as a configuration argument.
     *
     * @param listener  The listener that should receive the event (as an Intent).
     * @param eventType The Android event type.
     * @return True if the listener was added; false otherwise.
     * @
     */
    public boolean addDynamixEventListener(String eventType, IDynamixEventListener listener) {
        return getPluginFacade().addDynamixEventListener(getSessionId(), eventType, listener);
    }

    /**
     * Adds a listener that will receive Dynamix events. See developer docs
     * for details on the eventConfiguration.
     *
     * @param listener           The listener that should receive the event (as an Intent).
     * @param eventConfiguration The Android event configuration (e.g., event type and arguments).
     * @return True if the listener was added; false otherwise.
     * @
     */
    public boolean addDynamixEventListener(Bundle eventConfiguration, IDynamixEventListener listener) {
        return getPluginFacade().addDynamixEventListener(getSessionId(), eventConfiguration, listener);
    }

    /**
     * Removes a previously added IDynamixEventListener.
     *
     * @param listener The listener that should be removed.
     * @return True if the listener was removed; false otherwise.
     */
    public boolean removeDynamixEventListener(IDynamixEventListener listener) {
        return getPluginFacade().removeDynamixEventListener(getSessionId(), listener);
    }

    /**
     * Sends the event to Dynamix.
     *
     * @param event The event data to send.
     * @return A Result indicating success or failure.
     */
    public Result sendDynamixEvent(Intent event) {
        return getPluginFacade().sendDynamixEvent(getSessionId(), event);
    }

    /**
     * Obtains various information from Dynamix. See developer docs for details.
     *
     * @param sessionID The unique session id of the calling ContextPluginRuntime.
     * @param config    The configuration for the call (indicating which info to obtain).
     * @return A Bundle with the result, or null of the information cannot be obtained.
     */
    public Bundle getDynamixInformation(UUID sessionID, Intent config) {
        return getPluginFacade().getDynamixInformation(getSessionId(), config);
    }

    /**
     * Raised when the plug-in has received a new message.
     */
    public void onMessageReceived(Message message) {
        Log.d(TAG, "Received message but 'onMessageReceived' is not implemented!");
    }

    /**
     * Called by subclasses in order to send non-expiring context events to the Dynamix Framework
     *
     * @param data The SecuredContextData to send
     */
    public final void sendBroadcastContextEvent(IContextInfo data) {
        sendBroadcastContextEvent(data, -1);
    }

    /**
     * Called by subclasses in order to send non-expiring context events to the Dynamix Framework
     *
     * @param data   The SecuredContextData to send
     * @param intent The optional intent associated with this event
     */
    public final void sendBroadcastContextEvent(IContextInfo data, Intent intent) {
        sendBroadcastContextEvent(data, -1, intent);
    }

    /**
     * Called by subclasses in order to send expiring context events to the Dynamix Framework
     *
     * @param data        The List of SecuredContextData to send
     * @param expireMills The length of time the SecuredContextData are valid (in milliseconds)
     */
    public final void sendBroadcastContextEvent(IContextInfo data, int expireMills) {
        sendBroadcastContextEvent(data, expireMills, null);
    }

    /**
     * Called by subclasses in order to send expiring context events to the Dynamix Framework
     *
     * @param data        The List of SecuredContextData to send
     * @param expireMills The length of time the SecuredContextData are valid (in milliseconds)
     * @param intent      The optional intent associated with this event
     */
    public final void sendBroadcastContextEvent(IContextInfo data, int expireMills, Intent intent) {
        // Create a BROADCAST ContextDataSet
        ContextInfoSet dataSet = ContextInfoSet.createBroadcastContextInfoSet(data, expireMills);
        dataSet.setIntent(intent);
        autoParametrizeIntent(intent, dataSet.getContextType());
        if (dataSet != null) {
            // Delegates behavior to the injected IPluginEventHandler
            if (getEventHandler() != null)
                getEventHandler().sendEvent(this, dataSet);
            else
                Log.w(TAG, "Can't sendContextEvent because the event handler was null");
        } else
            Log.w(TAG, "Can't sendContextEvent because createBroadcastContextDataSet returned null");
    }

    /**
     * Automatically creates and broadcasts a BundleContextInfo to all context type listeners.
     *
     * @param data        The Bundle to send
     * @param contextType The type of context information contained within the Bundle.
     */
    public final void sendBroadcastContextEvent(Bundle data, String contextType) {
        this.sendBroadcastContextEvent(new BundleContextInfo(data, contextType));
    }

    /**
     * Automatically creates and broadcasts a BundleContextInfo to all context type listeners.
     *
     * @param data        The Bundle to send
     * @param contextType The type of context information contained within the Bundle.
     * @param intent      The optional intent associated with this event
     */
    public final void sendBroadcastContextEvent(Bundle data, String contextType, Intent intent) {
        this.sendBroadcastContextEvent(new BundleContextInfo(data, contextType), intent);
    }

    /**
     * Automatically creates and sends a BundleContextInfo based on the incoming bundle.
     *
     * @param data        The Bundle to send
     * @param contextType The type of context information contained within the Bundle.
     * @param expireMills The length of time the Bundle data are valid (in milliseconds)
     */
    public final void sendBroadcastContextEvent(Bundle data, String contextType, int expireMills) {
        this.sendBroadcastContextEvent(new BundleContextInfo(data, contextType), expireMills);
    }

    /**
     * Automatically creates and sends a BundleContextInfo based on the incoming bundle.
     *
     * @param data        The Bundle to send
     * @param contextType The type of context information contained within the Bundle.
     * @param expireMills The length of time the Bundle data are valid (in milliseconds)
     * @param intent      The optional intent associated with this event
     */
    public final void sendBroadcastContextEvent(Bundle data, String contextType, int expireMills, Intent intent) {
        this.sendBroadcastContextEvent(new BundleContextInfo(data, contextType), expireMills, intent);
    }

    /**
     * Called by subclasses in order to send a non-expiring context events to the receiver associated with the
     * responseId.
     *
     * @param responseId The UUID of the originating request.
     * @param data       The IContextInfo to send.
     */
    public final void sendContextEvent(UUID responseId, IContextInfo data) {
        sendContextEvent(responseId, data, null);
    }

    /**
     * Called by subclasses in order to send a non-expiring context events to the receiver associated with the
     * responseId.
     *
     * @param responseId The UUID of the originating request.
     * @param data       The IContextInfo to send.
     * @param intent     The optional intent associated with this event
     */
    public final void sendContextEvent(UUID responseId, IContextInfo data, Intent intent) {
        // Create a UNICAST ContextDataSet
        ContextInfoSet dataSet = ContextInfoSet.createUnicastContextInfoSet(data, responseId);
        dataSet.setIntent(intent);
        autoParametrizeIntent(intent, dataSet.getContextType());
        if (dataSet != null) {
            // Delegates behavior to the injected IPluginEventHandler
            if (getEventHandler() != null)
                getEventHandler().sendEvent(this, dataSet);
            else
                Log.w(TAG, "Can't sendContextEvent because the event handler was null");
        } else
            Log.w(TAG, "Can't sendContextEvent because createBroadcastContextDataSet returned null");
    }

    /**
     * Called by subclasses in order to send expiring context events to the receiver associated with the responseId.
     *
     * @param responseId  The UUID of the originating request
     * @param data        The IContextInfo to send
     * @param expireMills The length of time the SecuredContextData are valid (in milliseconds)
     */
    public final void sendContextEvent(UUID responseId, IContextInfo data, int expireMills) {
        sendContextEvent(responseId, data, expireMills, null);
    }

    /**
     * Called by subclasses in order to send expiring context events to the receiver associated with the responseId.
     *
     * @param responseId  The UUID of the originating request
     * @param data        The IContextInfo to send
     * @param expireMills The length of time the SecuredContextData are valid (in milliseconds)
     * @param intent      The optional intent associated with this event
     */
    public final void sendContextEvent(UUID responseId, IContextInfo data, int expireMills, Intent intent) {
        // Create a UNICAST ContextDataSet
        ContextInfoSet dataSet = ContextInfoSet.createUnicastContextInfoSet(data, responseId, expireMills);
        dataSet.setIntent(intent);
        autoParametrizeIntent(intent, dataSet.getContextType());
        if (dataSet != null) {
            // Delegates behavior to the injected IPluginEventHandler
            if (getEventHandler() != null)
                getEventHandler().sendEvent(this, dataSet);
            else
                Log.w(TAG, "Can't sendContextEvent because the event handler was null");
        } else
            Log.w(TAG, "Can't sendContextEvent because createBroadcastContextDataSet returned null");
    }

    /**
     * Automatically creates and sends a non-expiring BundleContextInfo to the receiver associated with the responseId.
     *
     * @param responseId The UUID of the originating request
     * @param data       The Bundle to send
     */
    public final void sendContextEvent(UUID responseId, Bundle data, String contextType) {
        this.sendContextEvent(responseId, new BundleContextInfo(data, contextType));
    }

    /**
     * Automatically creates and sends a non-expiring BundleContextInfo to the receiver associated with the responseId.
     *
     * @param responseId The UUID of the originating request
     * @param data       The Bundle to send
     * @param intent     The optional intent associated with this event
     */
    public final void sendContextEvent(UUID responseId, Bundle data, String contextType, Intent intent) {
        this.sendContextEvent(responseId, new BundleContextInfo(data, contextType), intent);
    }

    /**
     * Automatically creates and sends an expiring BundleContextInfo to the receiver associated with the responseId.
     *
     * @param responseId  The UUID of the originating request
     * @param data        The Bundle to send
     * @param contextType The type of context information contained within the Bundle.
     * @param expireMills The length of time the Bundle data are valid (in milliseconds)
     */
    public final void sendContextEvent(UUID responseId, Bundle data, String contextType, int expireMills) {
        this.sendContextEvent(responseId, new BundleContextInfo(data, contextType), expireMills);
    }

    /**
     * Automatically creates and sends an expiring BundleContextInfo to the receiver associated with the responseId.
     *
     * @param responseId  The UUID of the originating request
     * @param data        The Bundle to send
     * @param contextType The type of context information contained within the Bundle.
     * @param expireMills The length of time the Bundle data are valid (in milliseconds)
     * @param intent      The optional intent associated with this event
     */
    public final void sendContextEvent(UUID responseId, Bundle data, String contextType, int expireMills, Intent intent) {
        this.sendContextEvent(responseId, new BundleContextInfo(data, contextType), expireMills, intent);
    }

    /**
     * Called by subclasses to signal context request success without sending an associated event result.
     *
     * @param responseId The UUID of the originating request.
     */
    public final void sendContextRequestSuccess(UUID responseId) {
        // Delegates behavior to the injected IPluginEventHandler
        if (getEventHandler() != null) {
            getEventHandler().sendSuccess(this, responseId);
        } else
            Log.w(TAG, "Can't sendContextRequestSuccess because the event handler was null");
    }

    /**
     * Called by subclasses to signal context request success without sending an associated event result.
     *
     * @param responseId The UUID of the originating request.
     * @param intent     The optional intent associated with this event
     */
    public final void sendContextRequestSuccess(UUID responseId, Intent intent) {
        // Delegates behavior to the injected IPluginEventHandler
        if (getEventHandler() != null) {
            autoParametrizeIntent(intent, responseId);
            getEventHandler().sendSuccess(this, responseId, intent);
        } else
            Log.w(TAG, "Can't sendContextRequestSuccess because the event handler was null");
    }

    /**
     * Called by subclasses to send context request errors to the receiver associated with the responseId.
     *
     * @param responseId   The UUID of the originating request.
     * @param errorMessage The error message.
     * @param errorCode    The error code of the failure.
     */
    public final void sendContextRequestError(UUID responseId, String errorMessage, int errorCode) {
        // Delegates behavior to the injected IPluginEventHandler
        if (getEventHandler() != null)
            getEventHandler().sendError(this, responseId, errorMessage, errorCode);
        else
            Log.w(TAG, "Can't sendContextRequestError because the event handler was null");
    }

    /**
     * Removes the IPluginContextListener in preparation for garbage collection. This method will be automatically
     * called by Dynamix after this plug-in is destroyed.
     *
     * @param dynamixListener
     */
    public final void unbindDynamix(IPluginContextListener dynamixListener) {
        if (eventHandler != null)
            eventHandler.removeContextListener(dynamixListener);
    }

    /**
     * Returns the absolute file path of the plug-in's private data directory on the local filesystem. Note that
     * plug-ins should only store data under this path!
     */
    public final String getPrivateDataDirectory() {
        return getPluginFacade().getPluginInfo(getSessionId()).getExtras().get(PluginConstants.PRIVATE_DATA_DIR);
    }

    /**
     * Returns the ContextPluginInformation associated with this plug-in;
     */
    public final ContextPluginInformation getPluginInfo() {
        return getPluginFacade().getPluginInfo(getSessionId());
    }

    /**
     * Sets the associated IPluginFacade.
     *
     * @see IPluginFacade
     */
    protected final void setPluginFacade(IPluginFacade androidFacade) {
        this.pluginFacade = androidFacade;
    }

    /**
     * Sets this ContextPluginRuntime's event handler.
     *
     * @see IPluginEventHandler
     */
    protected final void setEventHandler(IPluginEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    /**
     * Sets the UUID session id that uniquely identifies a ContextPluginRuntime to the Dynamix Framework during runtime.
     * This UUID may change each time Dynamix starts.
     */
    protected final void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Utility that auto-parametrizes the incoming intent based on the action. Currently adds
     * SUPPORTED_INTERFACES and GATEWAY to all interaction resource bundles within
     * intents with action INTERACTION_EVENT_ACTION.
     */
    private void autoParametrizeIntent(Intent intent, String contextType) {
        if (intent != null) {
            if (intent.getAction() == DynamixHelper.InteractionEvent.INTERACTION_EVENT_ACTION) {
                ArrayList<Bundle> iResources = DynamixHelper.InteractionEvent.getInteractionResourceBundles(intent);
                for (Bundle iResource : iResources) {
                    // Set iResource supported interface to the context type (if not already set)
                    if (!iResource.containsKey(DynamixHelper.InteractionEvent.SUPPORTED_INTERFACES))
                        DynamixHelper.InteractionEvent.setSupportedInterface(iResource, contextType);
                    // Set iResource gateway to the plug-in (if not already set)
                    if (!iResource.containsKey(DynamixHelper.InteractionEvent.GATEWAY))
                        DynamixHelper.InteractionEvent.setGateway(iResource, "dynamix-plugin:" + getPluginInfo().getPluginId());
                }
            }
        }
    }

    /**
     * Utility that auto-parametrizes the incoming intent based on the action.
     */
    private void autoParametrizeIntent(Intent i, UUID requestId) {
        // Grab the context type of the requestId from Dynamix
        Bundle result = getDynamixInformation(getSessionId(), DynamixHelper.DynamixInformation.createRequestContextTypeIntent(requestId));
        if (result != null && result.containsKey(DynamixHelper.DynamixInformation.REQUEST_CONTEXT_TYPE)) {
            autoParametrizeIntent(i, result.getString(DynamixHelper.DynamixInformation.REQUEST_CONTEXT_TYPE));
        } else
            Log.w(TAG, "Could not call GET_REQUEST_CONTEXT_TYPE using Dynamix");
    }

    /**
     * Sample code for receiving and sending interaction events.
     */
    private void interactionEventExample() {
        /**
         * The following demonstrates how listen for and process interaction events.
         */
        addDynamixEventListener(DynamixHelper.InteractionEvent.INTERACTION_EVENT_ACTION,
                new IDynamixEventListener() {
                    @Override
                    public void onDynamixEvent(Intent i) {
                        // Verify the intent is of type interaction event
                        if (DynamixHelper.InteractionEvent.isInteractionEvent(i)) {
                            /**
                             * The example code below simply loops through the interaction resource
                             * bundles associated with the incoming event. Data within the bundles
                             * can be extracted using the DynamixHelper. Several examples of extracting
                             * interaction resource data are shown.
                             */
                            for (Bundle ir : DynamixHelper.InteractionEvent.getInteractionResourceBundles(i)) {
                                // Access the resource address
                                String resourceAddress = DynamixHelper.InteractionEvent.getResourceAddress(ir);
                                // Access the resource type
                                String resourceType = DynamixHelper.InteractionEvent.getResourceType(ir);
                                // Access the connection (see DynamixHelper.Connection for strings)
                                String connection = DynamixHelper.InteractionEvent.getConnection(ir);
                                // Access the gateway
                                String gateway = DynamixHelper.InteractionEvent.getGateway(ir);
                                // Access the interfaces (likely the Dynamix context type)
                                List<String> interfaces = DynamixHelper.InteractionEvent.getSupportedInterfaces(ir);
                                // ...
                                // Handle other interaction resource data as needed (see DynamixHelper.InteractionEvent)
                            }
                        }
                    }
                });
        /**
         * The following shows 3 methods of sending interaction events
         */
        // Create an example interaction event intent
        Intent interactionEvent = DynamixHelper.InteractionEvent.createIntent
                ("resourceAddress", "resourceType", "org.example.context.type",
                        "io.dynamix.pluginid", DynamixHelper.Connection.WIFI);
        // Create a sample IContextInfo (use your custom IContextInfo implementations as needed)
        IContextInfo myContextInfo = new BundleContextInfo(new Bundle(), "org.example.context.type");
        // 1. Send a broadcast context event that includes the interaction event
        sendBroadcastContextEvent(myContextInfo, interactionEvent);
        // Create a sample responseId (normally this is provided by Dynamix in handleContextRequest)
        UUID responseId = UUID.randomUUID();
        // 2. Send a unicast context event that expires and includes the interaction event
        sendContextEvent(responseId, myContextInfo, 20000, interactionEvent);
        // 3. Send a dedicated Dynamix event that includes the interaction event
        sendDynamixEvent(interactionEvent);
    }
}