/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import org.ambientdynamix.api.application.ContextPluginInformation;

/**
 * Base implementation of IPluginStateListener with no functionality. Override as needed.
 * 
 * @see IPluginStateListener
 * @author Darren Carlson
 *
 */
public class PluginStateListener implements IPluginStateListener {
	/**
	 * @inheritDoc
	 */
	@Override
	public void onNew(ContextPluginInformation plug) {
		// Override as needed
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void onInitializing(ContextPluginInformation plug) {
		// Override as needed
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void onInitialized(ContextPluginInformation plug) {
		// Override as needed
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void onStarting(ContextPluginInformation plug) {
		// Override as needed
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void onStarted(ContextPluginInformation plug) {
		// Override as needed
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void onStopping(ContextPluginInformation plug) {
		// Override as needed
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void onDestroyed(ContextPluginInformation plug) {
		// Override as needed
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void onError(ContextPluginInformation plug) {
		// Override as needed
	}
}
