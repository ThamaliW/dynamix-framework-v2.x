/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Helper class for interacting with Dynamix
 *
 * @author Darren Carlson
 */
public class DynamixHelper {
    private DynamixHelper() {
    }

    public enum Connection {
        WIFI("WIFI"),
        BLUETOOTH("BLUETOOTH"),
        BLUETOOTH_LE("BLUETOOTH_LE"),
        USB("USB"),
        ANT("ANT"),
        NFC("NFC"),
        IR("IR");
        private final String name;

        private Connection(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return (otherName == null) ? false : name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }

    /**
     * Utility for creating ContextPluginRuntime.getDynamixInformation requests.
     */
    public static class DynamixInformation {
        public static final String GET_REQUEST_CONTEXT_TYPE = "GET_REQUEST_CONTEXT_TYPE";
        public static final String REQUEST_ID = "REQUEST_ID";
        public static final String REQUEST_CONTEXT_TYPE = "REQUEST_CONTEXT_TYPE";

        public static Intent createRequestContextTypeIntent(UUID requestId) {
            Intent config = new Intent(DynamixHelper.DynamixInformation.GET_REQUEST_CONTEXT_TYPE);
            config.putExtra(DynamixHelper.DynamixInformation.REQUEST_ID, requestId);
            return config;
        }
    }

    /**
     * Utility for creating and parsing interaction event intents. These intents have an
     * action type of INTERACTION_EVENT_ACTION and an associated array of INTERACTION_RESOURCE_BUNDLES.
     */
    public static class InteractionEvent {
        // Intent Keys
        public static final String INTERACTION_EVENT_ACTION = "org.ambientdynamix.api.contextplugin.INTERACTION_EVENT_ACTION";
        public static final String INTERACTION_RESOURCE_BUNDLES = "org.ambientdynamix.api.contextplugin.INTERACTION_RESOURCE_BUNDLES";
        public static final String RESOURCE_ADDRESS = "org.ambientdynamix.api.contextplugin.RESOURCE_ADDRESS";
        public static final String RESOURCE_TYPE = "org.ambientdynamix.api.contextplugin.RESOURCE_TYPE";
        public static final String CONNECTION = "org.ambientdynamix.api.contextplugin.CONNECTION";
        public static final String RESOURCE_NAME = "org.ambientdynamix.api.contextplugin.RESOURCE_NAME";
        public static final String RESOURCE_DESCRIPTION = "org.ambientdynamix.api.contextplugin.RESOURCE_DESCRIPTION";
        public static final String SUPPORTED_INTERFACES = "org.ambientdynamix.api.contextplugin.SUPPORTED_INTERFACES";
        public static final String PAIRING_MODE = "org.ambientdynamix.api.contextplugin.PAIRING_MODE";
        public static final String GATEWAY = "org.ambientdynamix.api.contextplugin.GATEWAY";
        public static final String INTERACTION_DATA_TYPE = "org.ambientdynamix.api.contextplugin.INTERACTION_DATA_TYPE";

        /**
         * Factory method for creating interaction event intents.
         */
        public static Intent createIntent(String resourceAddress) {
            return createIntent(resourceAddress, null, null, null, null, null, null, null);
        }

        /**
         * Factory method for creating interaction event intents.
         */
        public static Intent createIntent(List<String> resourceAddresses) {
            Intent i = new Intent(INTERACTION_EVENT_ACTION);
            ArrayList<Bundle> iResources = new ArrayList<>();
            for (String address : resourceAddresses)
                iResources.add(createInteractionResourceBundle(address));
            i.putParcelableArrayListExtra(INTERACTION_RESOURCE_BUNDLES, iResources);
            return i;
        }

        /**
         * Factory method for creating interaction event intents.
         */
        public static Intent createIntent(Bundle interactionResourceBundle) {
            Intent i = new Intent(INTERACTION_EVENT_ACTION);
            i.putParcelableArrayListExtra(INTERACTION_RESOURCE_BUNDLES,
                    createBundleList(interactionResourceBundle));
            return i;
        }

        /**
         * Factory method for creating interaction event intents.
         */
        public static Intent createIntent(ArrayList<Bundle> interactionResourceBundles) {
            Intent i = new Intent(INTERACTION_EVENT_ACTION);
            i.putParcelableArrayListExtra(INTERACTION_RESOURCE_BUNDLES,
                    interactionResourceBundles);
            return i;
        }

        /**
         * Factory method for creating interaction event intents.
         */
        public static Intent createIntent(String resourceAddress, String resourceType, String supportedInterface, String gateway, Connection connection) {
            ArrayList<String> interfaceList = new ArrayList<>();
            interfaceList.add(supportedInterface);
            return createIntent(resourceAddress, resourceType, interfaceList, gateway, connection);
        }

        /**
         * Factory method for creating interaction event intents.
         */
        public static Intent createIntent(String resourceAddress, String resourceType, ArrayList<String> supportedInterfaces, String gateway, Connection connection) {
            return createIntent(resourceAddress, resourceType, null, null, supportedInterfaces, gateway, null, connection);
        }

        /**
         * Factory method for creating interaction event intents.
         */
        public static Intent createIntent(String resourceAddress, String resourceType, String resourceName,
                                          String resourceDescription, ArrayList<String> supportedInterfaces,
                                          String gateway, String pairingMode, Connection connection) {
            Intent i = new Intent(INTERACTION_EVENT_ACTION);
            i.putParcelableArrayListExtra(INTERACTION_RESOURCE_BUNDLES,
                    createBundleList(createInteractionResourceBundle(resourceAddress, resourceType, resourceName,
                            resourceDescription, supportedInterfaces, gateway, pairingMode, connection)));
            return i;
        }

        /**
         * Returns true if the intent has an interaction event action; false otherwise.
         */
        public static boolean isInteractionEvent(Intent i) {
            return i.getAction() == INTERACTION_EVENT_ACTION;
        }

        /**
         * Returns the count of associated interaction resource bundles within the intent.
         */
        public static int getInteractionResourceBundleCount(Intent interactionEvent) {
            if (interactionEvent.hasExtra(INTERACTION_RESOURCE_BUNDLES))
                return interactionEvent.getParcelableArrayListExtra(INTERACTION_RESOURCE_BUNDLES).size();
            else
                return 0;
        }

        /**
         * Returns the list of interaction resource bundles associated with the intent, or an empty list if none are found.
         */
        public static ArrayList<Bundle> getInteractionResourceBundles(Intent intent) {
            if (intent.hasExtra(INTERACTION_RESOURCE_BUNDLES))
                return intent.getParcelableArrayListExtra(INTERACTION_RESOURCE_BUNDLES);
            else
                return new ArrayList<>();
        }

        /**
         * Sets the list of interaction resource bundles associated with the intent.
         */
        public static void setInteractionResourceBundles(Intent interactionEvent, ArrayList<Bundle> interactionResourceBundles) {
            interactionEvent.putParcelableArrayListExtra(INTERACTION_RESOURCE_BUNDLES, interactionResourceBundles);
        }

        /**
         * Returns the resource address associated with the interaction resource bundle; or null if not set.
         */
        public static String getResourceAddress(Bundle b) {
            return b.getString(RESOURCE_ADDRESS);
        }

        /**
         * Returns the resource type associated with the interaction resource bundle; or null if not set.
         */
        public static String getResourceType(Bundle b) {
            return b.getString(RESOURCE_TYPE);
        }

        /**
         * Returns the resource name associated with the interaction resource bundle; or null if not set.
         */
        public static String getResourceName(Bundle b) {
            return b.getString(RESOURCE_NAME);
        }

        /**
         * Returns true if the interaction resource bundle has a RESOURCE_NAME; false otherwise.
         */
        public static boolean hasResourceName(Bundle b) {
            return b.containsKey(RESOURCE_NAME);
        }

        /**
         * Returns the description associated with the interaction resource; or null if not set.
         */
        public static String getResourceDescription(Bundle b) {
            return b.getString(RESOURCE_DESCRIPTION);
        }

        /**
         * Returns true if the interaction resource bundle has a RESOURCE_DESCRIPTION; false otherwise.
         */
        public static boolean hasResourceDescription(Bundle b) {
            return b.containsKey(RESOURCE_DESCRIPTION);
        }

        /**
         * Returns the interaction resource's supported interfaces; or null if not set.
         */
        public static ArrayList<String> getSupportedInterfaces(Bundle b) {
            return b.getStringArrayList(SUPPORTED_INTERFACES);
        }

        /**
         * Returns the gateway associated with the interaction resource bundle; or null if not set.
         */
        public static String getGateway(Bundle b) {
            return b.getString(GATEWAY);
        }

        /**
         * Returns the connection associated with the interaction resource bundle; or null if not set.
         */
        public static String getConnection(Bundle b) {
            return b.getString(CONNECTION);
        }

        /**
         * Returns the pairing mode associated with the interaction resource bundle; or null if not set.
         */
        public static String getPairingMode(Bundle b) {
            return b.getString(PAIRING_MODE);
        }

        /**
         * Returns true if the interaction resource bundle has a PAIRING_MODE; false otherwise.
         */
        public static boolean hasPairingMode(Bundle b) {
            return b.containsKey(PAIRING_MODE);
        }

        /**
         * Sets the intent action to INTERACTION_EVENT_ACTION.
         */
        public static void setInteractionEventAction(Intent i) {
            i.setAction(INTERACTION_EVENT_ACTION);
        }

        /**
         * Sets the address associated with the interaction resource bundle.
         */
        public static void setResourceAddress(Bundle b, String resourceAddress) {
            b.putString(RESOURCE_ADDRESS, resourceAddress);
        }

        /**
         * Sets the type associated with the interaction resource bundle.
         */
        public static void setResourceType(Bundle b, String resourceType) {
            b.putString(RESOURCE_TYPE, resourceType);
        }

        /**
         * Sets the name associated with the interaction resource bundle.
         */
        public static void setResourceName(Bundle b, String resourceName) {
            b.putString(RESOURCE_NAME, resourceName);
        }

        /**
         * Sets the description associated with the interaction resource bundle.
         */
        public static void setResourceDescription(Bundle b, String resourceDescription) {
            b.putString(RESOURCE_DESCRIPTION, resourceDescription);
        }

        /**
         * Sets the interaction resource's supported interface.
         */
        public static void setSupportedInterface(Bundle b, String supportedInterface) {
            ArrayList<String> supportedInterfaces = new ArrayList<>();
            supportedInterfaces.add(supportedInterface);
            setSupportedInterfaces(b, supportedInterfaces);
        }

        /**
         * Returns true if the resource bundle has an associated data type; false otherwise.
         */
        public static boolean hasInteractionDataType(Bundle b) {
            return b.containsKey(INTERACTION_DATA_TYPE);
        }

        /**
         * Returns the data type used by the resource during the interaction, or null if not set.
         */
        public static String getInteractionDataType(Bundle b) {
            return b.getString(INTERACTION_DATA_TYPE);
        }

        /**
         * Sets the data type used by the resource during the interaction.
         */
        public static void setInteractionDataType(Bundle b, String dataType) {
            b.putString(INTERACTION_DATA_TYPE, dataType);
        }

        /**
         * Sets the interaction resource's supported interfaces.
         */
        public static void setSupportedInterfaces(Bundle b, ArrayList<String> supportedInterfaces) {
            b.putStringArrayList(SUPPORTED_INTERFACES, supportedInterfaces);
        }

        /**
         * Sets the gateway associated with the interaction resource bundle.
         */
        public static void setGateway(Bundle b, String gateway) {
            b.putString(GATEWAY, gateway);
        }

        /**
         * Sets the pairing mode associated with the interaction resource bundle.
         */
        public static void setPairingMode(Bundle b, String pairingMode) {
            b.putString(PAIRING_MODE, pairingMode);
        }

        /**
         * Sets the connection associated with the interaction resource bundle.
         */
        public static void setConnection(Bundle b, Connection connection) {
            b.putString(CONNECTION, connection.toString());
        }

        /**
         * Creates an interaction resource bundle.
         */
        public static Bundle createInteractionResourceBundle(String resourceAddress) {
            return createInteractionResourceBundle(resourceAddress, null, null, null, null, null, null, null);
        }

        /**
         * Creates an interaction resource bundle.
         */
        public static Bundle createInteractionResourceBundle(String resourceAddress, String resourceType, String resourceName,
                                                             String resourceDescription, ArrayList<String> supportedInterfaces,
                                                             String gateway, String pairingMode, Connection connection) {
            Bundle bundle = new Bundle();
            if (resourceAddress != null)
                bundle.putString(RESOURCE_ADDRESS, resourceAddress);
            if (resourceType != null)
                bundle.putString(RESOURCE_TYPE, resourceType);
            if (supportedInterfaces != null)
                bundle.putStringArrayList(SUPPORTED_INTERFACES, supportedInterfaces);
            if (connection != null)
                bundle.putString(CONNECTION, connection.toString());
            if (resourceName != null)
                bundle.putString(RESOURCE_NAME, resourceName);
            if (resourceDescription != null)
                bundle.putString(RESOURCE_DESCRIPTION, resourceDescription);
            if (pairingMode != null)
                bundle.putString(PAIRING_MODE, pairingMode);
            return bundle;
        }

        /**
         * Utility for creating an array list for the incoming bundle.
         */
        private static ArrayList<Bundle> createBundleList(Bundle b) {
            ArrayList<Bundle> returnList = new ArrayList<>();
            returnList.add(b);
            return returnList;
        }
    }
}
