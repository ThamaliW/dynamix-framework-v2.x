/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import android.content.Intent;

import java.util.UUID;

/**
 * Hooks for injection of the Dynamix Framework.
 * 
 * @author Darren Carlson
 */
public interface IPluginEventHandler {
	/**
	 * Registers the specified IPluginEventHandler to receive events.
	 */
	public void addContextListener(IPluginContextListener listener);

	/**
	 * Unregisters the specified IPluginEventHandler from event reception.
	 */
	public void removeContextListener(IPluginContextListener listener);

	/**
	 * Sends a success message for a specific requestId.
	 * 
	 * @param sender
	 *            The event source.
	 * @param responseId
	 *            The original requestId.
	 */
	public void sendSuccess(ContextPluginRuntime sender, UUID responseId);

	/**
	 * Sends a success message for a specific requestId.
	 *
	 * @param sender
	 *            The event source.
	 * @param responseId
	 *            The original requestId.
	 * @param intent The optional intent associated with this event
	 */
	public void sendSuccess(ContextPluginRuntime sender, UUID responseId, Intent intent);

	/**
	 * Sends an error message for a specific requestId.
	 * 
	 * @param sender
	 *            The event source.
	 * @param requestId
	 *            The original requestId.
	 * @param errorMessage
	 *            The error message.
	 */
	public void sendError(ContextPluginRuntime sender, UUID requestId, String errorMessage, int errorCode);

	/**
	 * Sends the specified ContextDataSet to registered listeners using the specified ContextPluginRuntime as the event
	 * sender.
	 */
	public void sendEvent(ContextPluginRuntime sender, ContextInfoSet dataSet);
}