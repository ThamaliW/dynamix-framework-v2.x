/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import android.content.Intent;
import android.util.Log;

import org.ambientdynamix.api.application.AppConstants;
import org.ambientdynamix.api.application.Expirable;
import org.ambientdynamix.api.application.IContextInfo;
import org.ambientdynamix.api.contextplugin.PluginConstants.EventType;
import org.ambientdynamix.api.contextplugin.PluginConstants.LogPriority;

import java.util.UUID;

/**
 * Wrapper for context information sent by plug-ins.
 *
 * @author Darren Carlson
 */
public class ContextInfoSet extends Expirable {
    private static final long serialVersionUID = -6267867748921431783L;
    private static final String TAG = ContextInfoSet.class.getSimpleName();
    private EventType eventType;
    private String implementingClassname;
    private UUID responseId;
    private String contextType;
    private String logMessage;
    private LogPriority priority;
    private IContextInfo contextInfo;
    private boolean autoWebEncode = true;
    private String webEncodingFormat = AppConstants.JSON_WEB_ENCODING;
    private Intent intent;

    /**
     * Creates a BROADCAST ContextInfoSet. Private method, use factory methods for creation.
     *
     * @param contextInfo The context info associated with this event.
     */
    private ContextInfoSet(IContextInfo contextInfo, int expireMills, EventType type) {
        super(expireMills);
        this.contextInfo = contextInfo;
        this.implementingClassname = contextInfo.getImplementingClassname();
        this.contextType = contextInfo.getContextType();
        eventType = type;
    }

    /**
     * Creates a UNICAST ContextInfoSet. Private method, use factory methods for creation.
     *
     * @param contextInfo The context info associated with this event.
     * @param responseId  The responseId of the specific request, which must match the associated requestId from the
     *                    'handleContextRequest'.
     */
    private ContextInfoSet(IContextInfo contextInfo, UUID responseId, int expireMills, EventType type) {
        super(expireMills);
        this.contextInfo = contextInfo;
        this.responseId = responseId;
        this.implementingClassname = contextInfo.getImplementingClassname();
        this.contextType = contextInfo.getContextType();
        eventType = type;
    }

    /**
     * Creates a UNICAST ContextInfoSet. Private method, use factory methods for creation.
     */
    private ContextInfoSet(LogPriority priority, String logMessage) {
        super(-1);
        eventType = EventType.LOGGING;
        this.logMessage = logMessage;
    }

    /**
     * Factory method that creates a BROADCAST ContextInfoSet.
     *
     * @param contextInfo The context info associated with this event.
     */
    public static ContextInfoSet createBroadcastContextInfoSet(IContextInfo contextInfo) {
        if (verifyContextInfo(contextInfo))
            return new ContextInfoSet(contextInfo, -1, EventType.BROADCAST);
        return null;
    }

    /**
     * Factory method that creates a BROADCAST ContextInfoSet.
     *
     * @param contextInfo The context info associated with this event.
     * @param expireMills How long until the ContextInfoSet expires
     */
    public static ContextInfoSet createBroadcastContextInfoSet(IContextInfo contextInfo, int expireMills) {
        if (verifyContextInfo(contextInfo))
            return new ContextInfoSet(contextInfo, expireMills, EventType.BROADCAST);
        return null;
    }

    /**
     * Sends a logging message.
     */
    public static ContextInfoSet createLoggingContextInfoSet(LogPriority priority, String logMessage) {
        return new ContextInfoSet(priority, logMessage);
    }

    /**
     * Factory method that creates a non-expiring UNICAST ContextInfoSet.
     *
     * @param contextInfo The context info associated with this event.
     * @param responseId  The responseId of the specific request, which must match the associated requestId from the
     *                    'handleContextRequest' method.
     */
    public static ContextInfoSet createUnicastContextInfoSet(IContextInfo contextInfo, UUID responseId) {
        if (verifyContextInfo(contextInfo))
            return new ContextInfoSet(contextInfo, responseId, -1, EventType.UNICAST);
        return null;
    }

    /**
     * Factory method that creates an expiring UNICAST ContextInfoSet, which is normally used by reactive
     * ContextPluginRuntimes.
     *
     * @param contextInfo The context info associated with this event.
     * @param responseId  The responseId of the specific request, which must match the associated requestId from the
     *                    'handleContextRequest' method.
     * @param expireMills How long until the ContextInfoSet expires
     */
    public static ContextInfoSet createUnicastContextInfoSet(IContextInfo contextInfo, UUID responseId, int expireMills) {
        if (verifyContextInfo(contextInfo))
            return new ContextInfoSet(contextInfo, responseId, expireMills, EventType.UNICAST);
        return null;
    }

    /**
     * Returns true if all SecuredContextInfo have the same context type; false otherwise.
     */
    private static boolean verifyContextInfo(IContextInfo data) {
        if (data != null) {
            if (data.getContextType() != null && data.getContextType().length() > 0) {
                if (data.getImplementingClassname() != null && data.getImplementingClassname().length() > 0) {
                    return true;
                } else {
                    Log.w(TAG, "verifyContextInfo: No implementing classname for " + data);
                }
            } else {
                Log.w(TAG, "verifyContextInfo: No context type for " + data);
            }
        } else {
            Log.w(TAG, "verifyContextInfo: Null IContextInfo (ignoring)");
        }
        return false;
    }

    /**
     * Returns the context type of the underlying IContextInfo entities (each will be of the same type).
     */
    public String getContextType() {
        return contextType;
    }

    /**
     * Returns the EventType of this ContextInfoSet (e.g. BROADCAST or UNICAST).
     */
    public EventType getEventType() {
        return eventType;
    }

    /**
     * Returns the implementing classname of the embedded ContextInfoSet.
     */
    public String getImplementingClassname() {
        return implementingClassname;
    }

    /**
     * Returns the debug message (if the event is of type LOG).
     */
    public String getLogMessage() {
        return logMessage;
    }

    /**
     * Returns the log priority.
     */
    public LogPriority getLogPriority() {
        return priority;
    }

    /**
     * Returns the responseId.
     */
    public UUID getResponseId() {
        return responseId;
    }

    /**
     * Sets the responseId.
     */
    public void setResponseId(UUID id) {
        this.responseId = id;
    }

    /**
     * Returns the IContextInfo associated with this SecuredContextData.
     */
    public IContextInfo getContextInfo() {
        return contextInfo;
    }

    /**
     * Returns the requested auto web encoding format.
     */
    public String getWebEncodingFormat() {
        return this.webEncodingFormat;
    }

    public void setWebEncodingFormat(String webEncodingFormat) {
        this.webEncodingFormat = webEncodingFormat;
    }

    /**
     * Returns true if Dynamix should perform auto-web-encoding for the event.
     */
    public boolean autoWebEncode() {
        return autoWebEncode;
    }

    /**
     * Sets web auto-web-encoding to true of false.
     */
    public void setWebEncode(boolean autoWebEncode) {
        this.autoWebEncode = autoWebEncode;
    }

    /**
     * Returns the intent associated with this result.
     */
    public Intent getIntent() {
        return this.intent;
    }

    /**
     * Returns true if this result has an associated intent; false otherwise.
     */
    public boolean hasIntent() {
        return this.intent != null;
    }

    /**
     * Sets the intent associated with this result.
     */
    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}