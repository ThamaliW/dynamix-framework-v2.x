/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.io.Serializable;

import org.ambientdynamix.api.application.VersionInfo;

/**
 * Represents basic information about a plug-in dependency.
 * 
 * @author Darren Carlson
 * 
 */
public class ContextPluginDependency implements Serializable {
	private static final long serialVersionUID = 5284958004227230652L;
	private String pluginId;
	private VersionInfo pluginVersion = new VersionInfo(0, 0, 0);
	private String pluginName;
	private String installUrl;

	/**
	 * Creates a ContextPluginDependency.
	 */
	public ContextPluginDependency(String pluginId) {
		this.pluginId = pluginId;
	}

	/**
	 * Creates a ContextPluginDependency.
	 */
	public ContextPluginDependency(String pluginId, VersionInfo pluginVersion) {
		this.pluginId = pluginId;
		this.pluginVersion = pluginVersion;
	}

	/**
	 * Creates a ContextPluginDependency.
	 */
	public ContextPluginDependency(String pluginId, VersionInfo pluginVersion, String pluginName, String installUrl) {
		this.pluginId = pluginId;
		this.pluginVersion = pluginVersion;
		this.pluginName = pluginName;
		this.installUrl = installUrl;
	}

	/**
	 * Returns the plug-in id associated with this dependency.
	 */
	public String getPluginId() {
		return pluginId;
	}

	/**
	 * Returns the plug-in version associated with this dependency.
	 */
	public VersionInfo getPluginVersion() {
		return pluginVersion;
	}

	/**
	 * Returns the plug-in name associated with this dependency.
	 */
	public String getPluginName() {
		return pluginName;
	}

	/**
	 * Returns the plug-in install url associated with this dependency.
	 */
	public String getInstallUrl() {
		return installUrl;
	}

	@Override
	public String toString() {
		return pluginId + ": version " + getPluginVersion();
	}

	@Override
	public boolean equals(Object candidate) {
		// Check for null
		if (candidate == null)
			return false;
		// Determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Make sure the id's and version numbers are the same
		ContextPluginDependency other = (ContextPluginDependency) candidate;
		if (other.getPluginId().equalsIgnoreCase(this.getPluginId())) {
			if (this.pluginVersion != null && other.pluginVersion != null) {
				if (other.getPluginVersion().equals(this.getPluginVersion()))
					return true;
			} else {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		int result = 17;
		if (this.pluginVersion != null)
			return 31 * result + getPluginId().hashCode() + getPluginVersion().hashCode();
		else
			return 31 * result + getPluginId().hashCode();
	}
}