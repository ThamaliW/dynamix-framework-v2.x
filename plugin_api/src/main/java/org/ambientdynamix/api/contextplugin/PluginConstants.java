/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

/**
 * Constants for the Plug-in API.
 * 
 * @author Darren Carlson
 */
public final class PluginConstants {
	// Singleton constructor
	private PluginConstants() {
	}
	
	public static String PRIVATE_DATA_DIR = "PRIVATE_DATA_DIR";
	
	// Plug-in alert constants
	public static String ALERT_TYPE = "ALERT_TYPE";
	public static String ALERT_TYPE_TOAST = "ALERT_TYPE_TOAST";
	public static String ALERT_TOAST_VALUE = "ALERT_TOAST_VALUE";
	public static String ALERT_TOAST_DURATION = "ALERT_TOAST_DURATION";
	public static String ALERT_TYPE_OPEN_SETTINGS = "ALERT_TYPE_OPEN_SETTINGS";
	public static String ALERT_OPEN_SETTINGS_VALUE = "ALERT_OPEN_SETTINGS_VALUE";
	// Constants for app creation
	// TODO Remove these keys
	public static final String APP_ID = "APP_ID";
	public static final String APP_NAME = "APP_NAME";
	public static final String APP_PAIR_TEMP = "APP_PAIR_TEMP";
	public static final String APP_ENCRYPTION_ENABLED = "APP_ENCRYPTION_ENABLED";
	public static final String APP_ENCRYPTION_KEY = "APP_ENCRYPTION_KEY";

	// Supported WEB_REQUEST constants
	public static final String WEB_REQUEST = "DYNAMIX_WEB_REQUEST";
	public static final String WEB_REQUEST_URI = "DYNAMIX_WEB_REQUEST_URI";
	public static final String WEB_REQUEST_METHOD = "DYNAMIX_WEB_REQUEST_METHOD";
	public static final String WEB_REQUEST_CONTENT_TYPE = "DYNAMIX_WEB_REQUEST_CONTENT_TYPE";
	public static final String WEB_REQUEST_CONTENT_LENGTH = "WEB_REQUEST_CONTENT_LENGTH";
	public static final String WEB_REQUEST_CONTENT = "WEB_REQUEST_CONTENT";

	/**
	 * Event type constants
	 */
	public enum EventType {
		BROADCAST, UNICAST, LOGGING
	}

	/**
	 * Logging priority constants
	 */
	public enum LogPriority {
		ASSERT, VERBOSE, DEBUG, INFO, WARN, ERROR
	}

	/**
	 * Device platform constants
	 */
	public enum PLATFORM {
		ANDROID("android");
		private String name;
		private static PLATFORM[] platforms = new PLATFORM[] { ANDROID };

		PLATFORM(String name) {
			this.name = name;
		}

		public static PLATFORM getPlatformFromString(String platformString) throws Exception {
			if (platformString != null) {
				for (PLATFORM p : platforms) {
					if (p.name.compareToIgnoreCase(platformString.trim()) == 0)
						return p;
				}
			} else
				throw new Exception("Platform string cannot be null");
			throw new Exception("Could not find PLATFORM for string: " + platformString);
		}

		@Override
		public String toString() {
			return name;
		}
	}

	/**
	 * Update priority values.
	 */
	public enum UpdatePriority {
		OPTIONAL, NORMAL, IMPORTANT, CRITICAL, MANDATORY
	}
}
