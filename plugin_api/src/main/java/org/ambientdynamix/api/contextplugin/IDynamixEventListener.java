/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import android.content.Intent;

/**
 * Interface for handling Dynamix events sent to plug-ins. Examples may include NFC events,
 * activity recognition, interaction reports.
 *
 * @author Darren Carlson
 */
public interface IDynamixEventListener {
    String ACTIVITY_RECOGNITION_EVENT = "org.ambientdynamix.api.contextplugin.ACTIVITY_RECOGNITION_EVENT";

    /**
     * Event is raised when Dynamix dispatches an event to registered plug-ins
     */
    void onDynamixEvent(Intent i);
}