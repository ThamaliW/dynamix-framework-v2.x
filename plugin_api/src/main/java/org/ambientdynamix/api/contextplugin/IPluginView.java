/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import android.content.Intent;
import android.view.View;

/**
 * Interface for plug-in view (e.g., feature views, configuration views of context interaction views).
 * 
 * @author Darren Carlson
 */
public interface IPluginView {
	/**
	 * Sets the activity controller, which can be used to close the host activity. This method will be called before all
	 * others.
	 */
	public void setActivityController(ActivityController controller);

	/**
	 * Returns the View to be injected into the host activity.
	 */
	public View getView();

	/**
	 * Specifies that the view should release any acquired resources and prepare for garbage collection. Note that if a
	 * context request is ongoing, the underlying runtime should call "sendContextRequestError" with the reason
	 * REQUEST_CANCELLED; for example: runtime.sendContextRequestError(requestId, "Barcode Scan: REQUEST_CANCELLED",
	 * ErrorCodes.REQUEST_CANCELLED);
	 */
	public void destroyView();

	/**
	 * Relays Intents received by the HostActivity to the IPluginView.
	 */
	public void handleIntent(Intent newIntent);

	/**
	 * Returns the view's preferred orientation. An orientation constant as used in ActivityInfo.<screenOrientation>.
	 * For example, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE. For details, see
	 * http://developer.android.com/reference/android/R.attr.html#screenOrientation.
	 */
	public int getPreferredOrientation();
}
