/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update.contextplugin;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.IPluginInstallCallback;

/**
 * Base class for plug-in installation events. Implements all methods with no behavior, allowing overrides.
 * 
 * @see IPluginInstallCallback
 * @author Darren Carlson
 */
public class PluginInstallCallback extends IPluginInstallCallback.Stub {
	@Override
	public void onInstallComplete(ContextPluginInformation plug) {
		// No behavior
	}

	@Override
	public void onInstallFailed(ContextPluginInformation plug, String message, int errorCode) {
		// No behavior
	}

	@Override
	public void onInstallProgress(ContextPluginInformation plug, int percentComplete) {
		// No behavior
	}

	@Override
	public void onInstallStarted(ContextPluginInformation plug) {
		// No behavior
	}
}
