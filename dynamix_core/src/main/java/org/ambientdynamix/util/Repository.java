/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.util;

import java.io.Serializable;

import org.ambientdynamix.api.application.RepositoryInfo;

import android.util.Log;

/**
 * Generalized model of a repository, including alias, url and type.
 * 
 * @author Darren Carlson
 */
public class Repository implements Serializable {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private String id;
	private String alias;
	private String url;
	private String type;
	private String username;
	private String password;
	private boolean removable = true;
	private boolean enabled = true;
	private boolean dynamixRepo = false;
	private boolean remove = false;

	/**
	 * Creates a repository to remove. It does not require anything except for an id.
	 */
	public Repository(String id) {
		this.id = id;
		this.remove = true;
	}

	/**
	 * Creates a Repository.
	 */
	public Repository(String id, String alias, String url, boolean dynamixRepo) {
		this(id, alias, url, null, null, dynamixRepo, true);
	}

	/**
	 * Creates a Repository.
	 */
	public Repository(String id, String alias, String url, boolean dynamixRepo, boolean enabled) {
		this(id, alias, url, null, null, dynamixRepo, enabled);
	}

	/**
	 * Creates a Repository.
	 */
	public Repository(String id, String alias, String url, String username, String password, boolean dynamixRepo) {
		this(id, alias, url, username, password, dynamixRepo, true);
	}

	/**
	 * Creates a Repository.
	 */
	public Repository(String id, String alias, String url, String username, String password, boolean dynamixRepo,
			boolean enabled) {
		if (id == null)
			throw new RuntimeException("Id cannot be null!");
		this.id = id;
		if (alias == null)
			throw new RuntimeException("Alias cannot be null!");
		this.alias = alias;
		if (url == null)
			throw new RuntimeException("Url cannot be null!");
		this.url = url;
		this.username = username;
		this.password = password;
		this.dynamixRepo = dynamixRepo;
		this.enabled = enabled;
		this.type = findType(url);
		trimEverything();
	}

	/**
	 * Returns true if this is an official Dynamix repo; false otherwise.
	 */
	public boolean isDynamixRepo() {
		return dynamixRepo;
	}

	/**
	 * Returns the id of this repository.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns the alias of the server (i.e. a descriptive server name used for display to the user).
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Returns the URL of the server.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the repo's URL.
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Returns the type of the server.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the repo's password, if there is one.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Sets the repo's username, if there is one.
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Returns true if this repo has a username; false otherwise.
	 */
	public boolean hasUsername() {
		return username != null && username.length() > 0;
	}

	/**
	 * Returns the username, or null if there is none.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Returns true if this repo has a password; false otherwise.
	 */
	public boolean hasPassword() {
		return password != null && password.length() > 0;
	}

	/**
	 * Returns the password, or null if there is none.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Returns true if the repo is removable from the database; false otherwise.
	 */
	public boolean isRemovable() {
		return removable;
	}

	/**
	 * Set true if the repo is removable from the database; false otherwise.
	 */
	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	/**
	 * Returns true if this repo should be forcefully removed, which can be directed by certain Dynamix framework
	 * updates.
	 */
	public boolean forceRemove() {
		return this.remove;
	}

	/**
	 * Returns true if the repo is enabled; false otherwise;
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Set true if the repo is enabled; false otherwise;
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Sets this repo's id.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Sets this repo's alias.
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Sets this repo's type.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Returns read-only info about this repo.
	 */
	public RepositoryInfo getRepositoryInfo() {
		return new RepositoryInfo(id, alias, url, type, enabled, dynamixRepo);
	}

	/**
	 * Returns true if the Repository contains no errors; false otherwise.
	 */
	public boolean validateServerInfo() {
		if (url != null) {
			if (alias == null || alias.length() == 0) {
				Log.w(TAG, "Repo is missing alias: " + alias);
			}
			// We're simply returning true here, since urls may be local
			return true;
		} else
			Log.w(TAG, "Missing repo URL");
		return false;
	}

	/**
	 * Returns true if this source is on a network; false otherwise (e.g., file-system).
	 */
	public boolean isNetworkSource() {
		return !type.equalsIgnoreCase(RepositoryInfo.SIMPLE_FILE_SOURCE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// Check for null
		if (candidate == null)
			return false;
		// Determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Check if the urls are the same
		Repository other = (Repository) candidate;
		if (other.getId().equalsIgnoreCase(this.getId()))
			return true;
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + getId().hashCode();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getUrl();
	}

	/*
	 * Utility for trimming strings, which is sometimes needed if source xml has whitespace
	 */
	private void trimEverything() {
		if (id != null)
			id = id.trim();
		if (alias != null)
			alias = alias.trim();
		if (url != null)
			url = url.trim();
		if (type != null)
			type = type.trim();
		if (password != null)
			password = password.trim();
		if (username != null)
			username = username.trim();
	}

	/*
	 * Utility for guessing the repo type based on the url
	 */
	private String findType(String url) {
		String typeGuess = "";
		if (url.startsWith("http") || url.startsWith("ftp")) {
			// Guess network, ignoring maven for the moment
			typeGuess = RepositoryInfo.SIMPLE_NETWORK_SOURCE;
		} else {
			// Otherwise guess file, ignoring maven for the moment
			typeGuess = RepositoryInfo.SIMPLE_FILE_SOURCE;
		}
		return typeGuess;
	}
}