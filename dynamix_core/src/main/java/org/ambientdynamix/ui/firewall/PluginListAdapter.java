/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.firewall;

import java.util.ArrayList;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.core.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PluginListAdapter extends BaseAdapter {
	ArrayList<ContextPluginInformation> supportedPlugins;
	Activity activity;
	LayoutInflater layoutInflater;

	public PluginListAdapter(Activity activity, ArrayList<ContextPluginInformation> supportedPlugins) {
		this.supportedPlugins = supportedPlugins;
		this.activity = activity;
		layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return supportedPlugins.size();
	}

	@Override
	public Object getItem(int arg0) {
		return supportedPlugins.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder;
		ContextPluginInformation contextPluginInformation = supportedPlugins.get(position);
		Boolean verified = Boolean.getBoolean(contextPluginInformation.getExtras().get(
				ContextPluginInformation.EXTRAS_DYNAMIX_REPO));
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.plugin_list_item_layout, null);
			holder.pluginName = (TextView) convertView.findViewById(R.id.pluginName);
			holder.pluginProvider = (TextView) convertView.findViewById(R.id.pluginProvider);
			holder.verifiedStatusIcon = (ImageView) convertView.findViewById(R.id.verifiedStatusImage);
			holder.verifiedStatusText = (TextView) convertView.findViewById(R.id.verifiedStatusText);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.pluginName.setText(contextPluginInformation.getPluginName().trim());
		holder.pluginProvider.setText(contextPluginInformation.getVersion().getValue());
		if (!verified) {
			holder.verifiedStatusIcon.setImageResource(R.drawable.unverified);
			holder.verifiedStatusText.setText("unverified");
		} else {
			holder.verifiedStatusIcon.setImageResource(R.drawable.verified);
			holder.verifiedStatusText.setText("verified");
		}
		return convertView;
	}

	private static class ViewHolder {
		TextView pluginName;
		TextView pluginProvider;
		ImageView verifiedStatusIcon;
		TextView verifiedStatusText;
	}
}
