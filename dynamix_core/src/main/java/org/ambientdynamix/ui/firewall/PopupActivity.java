/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.firewall;

import java.util.ArrayList;
import java.util.List;

import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.FirewallRequest;
import org.ambientdynamix.core.FirewallRule;
import org.ambientdynamix.core.FrameworkConstants;
import org.ambientdynamix.core.R;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PopupActivity extends FragmentActivity {
	static boolean active = false;
	private final static String TAG = PopupActivity.class.getSimpleName();
	private static FirewallRequest request;
	static FirewallRulesListAdapter adapter;
	static ArrayList<FirewallRule> addContextSupportList;
	static Activity activity;
	private boolean userInteracted = false;
	ListView listView;
	RelativeLayout container;

	/**
	 * Returns true if the popup is processing a request; false otherwise.
	 */
	public static boolean isProcessingRequest() {
		return request != null;
	}

	/**
	 * Called by Dynamix when a newly bound app requests context support.
	 */
	public static void setFirewallRequest(FirewallRequest request) {
		PopupActivity.request = request;
	}

	/**
	 * Called by Dynamix when a previously bound app requests additional context support.
	 */
	public static boolean updateFirewallRequest(FirewallRule newRule) {
		List<FirewallRule> rules = new ArrayList<FirewallRule>();
		rules.add(newRule);
		return updateFirewallRequest(rules);
	}

	/**
	 * Called by Dynamix when a previously bound app requests additional context support.
	 */
	public static boolean updateFirewallRequest(List<FirewallRule> newRules) {
		if (PopupActivity.request != null) {
			synchronized (PopupActivity.request) {
				// Update the request
				request.addFirewallRules(newRules);
				// Update the ui
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						adapter.notifyDataSetChanged();
					}
				});
				return true;
			}
		} else {
			Log.w(TAG, "No request to update!");
			return false;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.popup);
		container = (RelativeLayout) findViewById(R.id.container);
		adapter = new FirewallRulesListAdapter(this, request.getFirewallRules());
		listView = (ListView) findViewById(R.id.contextSupportRequestList);
		listView.setAdapter(adapter);
		Button okay = (Button) findViewById(R.id.okay);
		Button dismiss = (Button) findViewById(R.id.dismiss);
		adapter.notifyDataSetChanged();
		setupPopupTitle();
		dismiss.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancelFirewallRequest();
				finish();
			}
		});
		okay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				completeFirewallRequest();
				finish();
			}
		});
	}

	private void setupPopupTitle() {
		TextView popupTitle = (TextView) findViewById(R.id.popupTitle);
		DynamixApplication app = request.getApp();
		switch (app.getAppType()) {
		case WEB:
			popupTitle.setText("The " + app.getName()
					+ " web app is requesting access to the following resources from Dynamix");
			break;
		case AIDL:
			popupTitle.setText("The " + app.getName()
					+ " android app is requesting access to the following resources from Dynamix");
			break;
		default:
			popupTitle.setText("The " + app.getName()
					+ " app is requesting access to the following resources from Dynamix");
			break;
		}
	}

	public static boolean isActive() {
		return active;
	}

	/*
	 * Completes the firewall request
	 */
	private void completeFirewallRequest() {
		userInteracted = true;
		if (request != null)
			synchronized (request) {
				// Send the response back to Dynamix
				Intent responseIntent = new Intent(FrameworkConstants.ACTION_FIREWALL);
				responseIntent.putExtra(FrameworkConstants.FIREWALL_INTERACTION, request);
				sendBroadcast(responseIntent);
				request = null;
			}
	}

	/*
	 * Example for canceling a request; for example, if the user dismisses the dialog.
	 */
	private void cancelFirewallRequest() {
		userInteracted = true;
		if (request != null)
			synchronized (request) {
				request.setCancelled(true);
				Intent responseIntent = new Intent(FrameworkConstants.ACTION_FIREWALL);
				responseIntent.putExtra(FrameworkConstants.FIREWALL_INTERACTION, request);
				sendBroadcast(responseIntent);
				request = null;
			}
	}

	@Override
	public void onStart() {
		super.onStart();
		active = true;
	}

	@Override
	public void onStop() {
		super.onStop();
		active = false;
		request = null;
	}

	@Override
	protected void onPause() {
		super.onPause();
		/*
		 * If the user hasn't interacted with the firewall, he or she has navigated away from the popup without making a
		 * choice... so cancel the request.
		 */
		if (!userInteracted) {
			cancelFirewallRequest();
			finish();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		/*
		 * View rootView = getWindow().getDecorView().findViewById(android.R.id.content); // Checks the orientation of
		 * the screen if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) { // RelativeLayout.LayoutParams
		 * params = (RelativeLayout.LayoutParams) listView.getLayoutParams(); // params.height =
		 * Math.round(convertDpToPixel(128, activity)); // listView.setLayoutParams(params);
		 * rootView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
		 * Math.round(convertDpToPixel(256, activity)))); rootView.requestLayout(); } else if (newConfig.orientation ==
		 * Configuration.ORIENTATION_PORTRAIT) { // RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
		 * listView.getLayoutParams(); // params.height = Math.round(convertDpToPixel(256, activity)); //
		 * listView.setLayoutParams(params); rootView.setLayoutParams(new
		 * FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, Math.round(convertDpToPixel(420,
		 * activity)))); rootView.requestLayout(); }
		 */
	}
	/*
	 * public static float convertDpToPixel(float dp, Context context) { Resources resources = context.getResources();
	 * DisplayMetrics metrics = resources.getDisplayMetrics(); float px = dp * (metrics.densityDpi / 160f); return px; }
	 */
}