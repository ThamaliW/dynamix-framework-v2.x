package org.ambientdynamix.ui.dataobject;

/**
 * Created by shivam on 12/15/14.
 */
public class PluginsListViewItem {
	int type;
	Object object;

	public PluginsListViewItem(int type, Object object) {
		this.type = type;
		this.object = object;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
