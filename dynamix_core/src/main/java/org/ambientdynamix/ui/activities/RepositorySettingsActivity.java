/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.activities;

import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.fragments.RepositoriesFragment;
import org.ambientdynamix.util.Repository;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RepositorySettingsActivity extends FragmentActivity {
	RepositoriesFragment fragment;
	Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_external_repo_settings);
		activity = this;
		if (savedInstanceState == null) {
			fragment = new RepositoriesFragment();
			getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.external_repo_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			final Dialog d = new Dialog(RepositorySettingsActivity.this);
			d.setContentView(R.layout.dialog);
			d.setTitle("Add new repository address");
			final EditText repoURL = (EditText) d.findViewById(R.id.repoURL);
			final EditText password = (EditText) d.findViewById(R.id.password);
			final EditText username = (EditText) d.findViewById(R.id.userName);
			final EditText alias = (EditText) d.findViewById(R.id.alias);
			Button save = (Button) d.findViewById(R.id.save);
			Button dismiss = (Button) d.findViewById(R.id.dismiss);
			dismiss.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					d.dismiss();
				}
			});
			save.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String url = repoURL.getText().toString().trim();
					String pwd = password.getText().toString().trim();
					String uname = username.getText().toString().trim();
					String aliasText = alias.getText().toString().trim();
					if (url.equals("")) {
						Toast.makeText(activity, "Please enter a valid URL", Toast.LENGTH_LONG).show();
					} else {
						Repository info = new Repository(url, aliasText, url, uname, pwd, false);
						DynamixService.addContextPluginRepo(info);
						fragment.addToList(info);
						d.dismiss();
					}
				}
			});
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			Window window = d.getWindow();
			lp.copyFrom(window.getAttributes());
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
			window.setAttributes(lp);
			d.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
