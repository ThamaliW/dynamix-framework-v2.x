/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.adapters;

import java.util.ArrayList;
import java.util.List;

import org.ambientdynamix.api.application.AppConstants;
import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.dataobject.InstalledPluginListItemType;
import org.ambientdynamix.ui.dataobject.PluginsListViewItem;

import android.app.Activity;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class InstalledPluginsListAdapter extends BaseAdapter {
	Activity activity;
	ArrayList<PluginsListViewItem> installedPluginsListItems;
	private String LOGTAG = getClass().getSimpleName();
	private ArrayList<ContextPluginInformation> mCheckedState;
	ArrayList<ContextPluginInformation> pluginsToUninstall;
	ArrayList<ContextPluginInformation> uninstalling;
	// Stores a ContextPluginInformation Object if the item was in a collapsed state.
	ArrayList<ContextPluginInformation> mOptionsVisible;

	public InstalledPluginsListAdapter(Activity activity, ArrayList<PluginsListViewItem> installedPluginsListItems,
			ArrayList<ContextPluginInformation> uninstalling) {
		this.activity = activity;
		this.installedPluginsListItems = installedPluginsListItems;
		this.uninstalling = uninstalling;
		pluginsToUninstall = new ArrayList<ContextPluginInformation>();
		mCheckedState = new ArrayList<ContextPluginInformation>();
		mOptionsVisible = new ArrayList<ContextPluginInformation>();
	}

	@Override
	public int getItemViewType(int position) {
		synchronized (installedPluginsListItems) {
			return installedPluginsListItems.get(position).getType();
		}
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getCount() {
		synchronized (installedPluginsListItems) {
			return installedPluginsListItems.size();
		}
	}

	@Override
	public Object getItem(int arg0) {
		synchronized (installedPluginsListItems) {
			return installedPluginsListItems.get(arg0);
		}
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup arg2) {
		LayoutInflater inflater = activity.getLayoutInflater();
		Object listObject = null;
		synchronized (installedPluginsListItems) {
			listObject = installedPluginsListItems.get(position).getObject();
		}
		switch (getItemViewType(position)) {
		case InstalledPluginListItemType.CONTEXT_PLUGIN_VIEW:
			final ViewHolderPlugin holder;
			final ContextPluginInformation plugin = (ContextPluginInformation) listObject;
			if (convertView == null) {
				holder = new ViewHolderPlugin();
				convertView = inflater.inflate(R.layout.installed_plugin_list_item_layout, null);
				holder.pluginIcon = (ImageView) convertView.findViewById(R.id.pluginIcon);
				holder.pluginName = (TextView) convertView.findViewById(R.id.pluginName);
				holder.pluginVersion = (TextView) convertView.findViewById(R.id.pluginVersion);
				holder.settings = (Button) convertView.findViewById(R.id.settingsButton);
				holder.uninstall = (Button) convertView.findViewById(R.id.uninstallButton);
				holder.disable = (Button) convertView.findViewById(R.id.disableButton);
				holder.enable = (Button) convertView.findViewById(R.id.enableButton);
				holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
				holder.progressBar = (ProgressBar) convertView.findViewById(R.id.pBar);
				holder.expand = (ImageView) convertView.findViewById(R.id.expand);
				holder.options = (LinearLayout) convertView.findViewById(R.id.options);
				holder.repo = (TextView) convertView.findViewById(R.id.repoName);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolderPlugin) convertView.getTag();
			}
			// Plugin is currently being uninstalled,
			if (plugin.getInstallStatus().equals(AppConstants.PluginInstallStatus.INSTALLING)) {
				holder.checkBox.setVisibility(View.GONE);
				holder.progressBar.setVisibility(View.VISIBLE);
			} else {
				holder.checkBox.setVisibility(View.VISIBLE);
				holder.progressBar.setVisibility(View.GONE);
			}
			// Plugin is currently being uninstalled,
			if (uninstalling.contains(plugin)) {
				holder.checkBox.setVisibility(View.GONE);
				holder.progressBar.setVisibility(View.VISIBLE);
			} else {
				holder.checkBox.setVisibility(View.VISIBLE);
				holder.progressBar.setVisibility(View.GONE);
			}
			if (plugin.isEnabled()) {
				holder.disable.setVisibility(View.VISIBLE);
				holder.enable.setVisibility(View.GONE);
				holder.pluginIcon.setImageResource(R.drawable.plugin_enabled);
			} else {
				holder.disable.setVisibility(View.GONE);
				holder.enable.setVisibility(View.VISIBLE);
				holder.pluginIcon.setImageResource(R.drawable.plugin_disabled);
			}
			holder.pluginName.setText(plugin.getPluginName());
			holder.pluginVersion.setText(plugin.getVersion().getValue());
			holder.repo.setText(plugin.getRepository().getAlias());
			holder.disable.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					DynamixService.disableContextPlugin(plugin);
					holder.disable.setVisibility(View.GONE);
					holder.enable.setVisibility(View.VISIBLE);
					holder.pluginIcon.setImageResource(R.drawable.plugin_disabled);
					Log.d(LOGTAG, "Disabling plugin : " + plugin.getPluginName());
				}
			});
			holder.enable.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					DynamixService.enableContextPlugin(plugin);
					holder.disable.setVisibility(View.VISIBLE);
					holder.enable.setVisibility(View.GONE);
					holder.pluginIcon.setImageResource(R.drawable.plugin_enabled);
					Log.d(LOGTAG, "Enabling plugin : " + plugin.getPluginName());
				}
			});
			if (mOptionsVisible.contains(plugin)) {
				// Plugin was not in the collapsed state hence make the options visible
				holder.expand.setImageResource(R.drawable.ic_action_collapse);
				holder.options.setVisibility(View.VISIBLE);
			} else {
				// Plugin was in the collapsed state hence hide the options
				holder.expand.setImageResource(R.drawable.ic_action_expand);
				holder.options.setVisibility(View.GONE);
			}
			holder.uninstall.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.d(LOGTAG, "Uninstalling : " + plugin.getPluginName());
					holder.checkBox.setVisibility(View.GONE);
					holder.progressBar.setVisibility(View.VISIBLE);
					DynamixService.uninstallPlugin(plugin, new Callback() {
						@Override
						public void onSuccess() throws RemoteException {
							Log.d(LOGTAG, "Successfully Uninstalled : " + plugin.getPluginName());
							super.onSuccess();
						}

						@Override
						public void onFailure(String message, int errorCode) throws RemoteException {
							Log.d(LOGTAG, "Failed to uninstall : " + plugin.getPluginName());
							holder.checkBox.setVisibility(View.VISIBLE);
							holder.progressBar.setVisibility(View.GONE);
							super.onFailure(message, errorCode);
						}
					});
				}
			});
			holder.settings.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Result r = DynamixService.openContextPluginConfigurationForFramework(plugin.getPluginId(), plugin
							.getVersion().toString());
					if (!r.wasSuccessful()) {
						Toast.makeText(activity, r.getMessage(), Toast.LENGTH_LONG).show();
					}
				}
			});
			holder.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					if (arg1) {
						mCheckedState.add(plugin);
						pluginsToUninstall.add(plugin);
					} else {
						mCheckedState.remove(plugin);
						pluginsToUninstall.remove(plugin);
					}
				}
			});
			holder.checkBox.setChecked(mCheckedState.contains(plugin));
			convertView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (holder.options.getVisibility() == View.VISIBLE) {
						mOptionsVisible.remove(plugin);
						holder.expand.setImageResource(R.drawable.ic_action_expand);
						holder.options.setVisibility(View.GONE);
					} else {
						mOptionsVisible.add(plugin);
						holder.expand.setImageResource(R.drawable.ic_action_collapse);
						holder.options.setVisibility(View.VISIBLE);
					}
				}
			});
			return convertView;
		case InstalledPluginListItemType.HEADER_VIEW:
			ViewHolderHeader viewHolderHeader;
			String updateStatus = (String) listObject;
			if (convertView == null) {
				viewHolderHeader = new ViewHolderHeader();
				convertView = inflater.inflate(R.layout.header_view_pending_plugins, null);
				viewHolderHeader.updateStatus = (TextView) convertView.findViewById(R.id.headerText);
				convertView.setTag(viewHolderHeader);
			} else {
				viewHolderHeader = (ViewHolderHeader) convertView.getTag();
			}
			viewHolderHeader.updateStatus.setText(updateStatus);
			return convertView;
		}
		return convertView;
	}

	private static class ViewHolderPlugin {
		ImageView expand;
		LinearLayout options;
		ImageView updateFlag;
		ImageView pluginIcon;
		TextView pluginName;
		TextView pluginVersion;
		Button settings;
		Button uninstall;
		Button disable;
		Button enable;
		CheckBox checkBox;
		ProgressBar progressBar;
		TextView repo;
	}

	private static class ViewHolderHeader {
		TextView updateStatus;
	}

	public List<ContextPluginInformation> getPluginsToUninstall() {
		return new ArrayList<ContextPluginInformation>(pluginsToUninstall);
	}

	public void onPluginUninstalled(ContextPluginInformation plugin) {
		if (mCheckedState.contains(plugin)) {
			mCheckedState.remove(plugin);
		}
		// This ensures if a plugin was uninstalled with the plugin options visible,
		// and then reinstalled, it will not automatically go to the options visible state
		if (mOptionsVisible.contains(plugin)) {
			mOptionsVisible.remove(plugin);
		}
		synchronized (pluginsToUninstall) {
			if (pluginsToUninstall.contains(plugin)) {
				pluginsToUninstall.remove(plugin);
			}
		}
	}
}
