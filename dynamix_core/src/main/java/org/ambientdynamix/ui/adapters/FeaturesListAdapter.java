/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.adapters;

import java.util.ArrayList;

import org.ambientdynamix.api.application.DynamixFeature;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FeaturesListAdapter extends BaseAdapter {
	ArrayList<DynamixFeature> featuresList;
	Activity activity;

	public FeaturesListAdapter(Activity activity, ArrayList<DynamixFeature> featuresList) {
		this.featuresList = featuresList;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		return featuresList.size();
	}

	@Override
	public Object getItem(int arg0) {
		return featuresList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder;
		DynamixFeature feature = featuresList.get(position);
		if (convertView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.feature_list_item, null);
			holder = new ViewHolder();
			holder.nameTextView = (TextView) convertView.findViewById(R.id.name);
			holder.descriptionTextView = (TextView) convertView.findViewById(R.id.description);
			holder.pluginName = (TextView) convertView.findViewById(R.id.pluginName);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.nameTextView.setText(feature.getName());
		holder.descriptionTextView.setText(feature.getDescription());
		String pluginName = DynamixService.getContextPluginInfo(feature.getPlugId(), feature.getPluginVersion())
				.getPluginName();
		Log.d("FeaturesListAdapter", feature.getName() + " : " + pluginName);
		holder.pluginName.setText(pluginName);
		return convertView;
	}

	private static class ViewHolder {
		TextView nameTextView;
		TextView descriptionTextView;
		TextView pluginName;
	}
}
