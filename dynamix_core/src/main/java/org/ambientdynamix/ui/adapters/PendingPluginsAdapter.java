/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import org.ambientdynamix.api.application.AppConstants.ContextPluginType;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.dataobject.PendingPluginListItemType;
import org.ambientdynamix.ui.dataobject.PluginsListViewItem;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PendingPluginsAdapter extends BaseAdapter {
	Activity activity;
	ArrayList<PluginsListViewItem> pendingPluginsListItems;
	HashMap<ContextPluginInformation, Integer> installProgressMap;
	private String LOGTAG = getClass().getSimpleName();
	private ArrayList<ContextPluginInformation> pluginsToInstall;
	private HashMap<ContextPluginInformation, Boolean> mCheckedState;

	public PendingPluginsAdapter(Activity activity, ArrayList<PluginsListViewItem> pendingPluginsListItems,
			HashMap<ContextPluginInformation, Integer> installProgressMap) {
		this.activity = activity;
		this.pendingPluginsListItems = pendingPluginsListItems;
		this.installProgressMap = installProgressMap;
		pluginsToInstall = new ArrayList<ContextPluginInformation>();
		mCheckedState = new HashMap<ContextPluginInformation, Boolean>();
	}

	@Override
	public int getItemViewType(int position) {
		synchronized (pendingPluginsListItems) {
			return pendingPluginsListItems.get(position).getType();
		}
	}

	@Override
	public int getViewTypeCount() {
		return 3;
	}

	@Override
	public int getCount() {
		synchronized (pendingPluginsListItems) {
			return pendingPluginsListItems.size();
		}
	}

	@Override
	public Object getItem(int position) {
		synchronized (pendingPluginsListItems) {
			return pendingPluginsListItems.get(position);
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		LayoutInflater inflater = activity.getLayoutInflater();
		Object listObject = null;
		synchronized (pendingPluginsListItems) {
			listObject = pendingPluginsListItems.get(position).getObject();
		}
		switch (getItemViewType(position)) {
		case PendingPluginListItemType.HEADER_VIEW:
			String repositoryName = (String) listObject;
			ViewHolderHeader viewHolderHeader;
			if (convertView == null) {
				viewHolderHeader = new ViewHolderHeader();
				convertView = inflater.inflate(R.layout.header_view_pending_plugins, null);
				viewHolderHeader.repositoryName = (TextView) convertView.findViewById(R.id.headerText);
				convertView.setTag(viewHolderHeader);
			} else {
				viewHolderHeader = (ViewHolderHeader) convertView.getTag();
			}
			viewHolderHeader.repositoryName.setText(repositoryName);
			return convertView;
		case PendingPluginListItemType.CONTEXT_PLUGIN_VIEW:
			final ContextPluginInformation pendingContextPlugin = (ContextPluginInformation) listObject;
			final ViewHolderPlugin viewHolderPlugin;
			if (convertView == null) {
				viewHolderPlugin = new ViewHolderPlugin();
				convertView = inflater.inflate(R.layout.pending_plugin_list_item_layout, null);
				viewHolderPlugin.expand = (ImageView) convertView.findViewById(R.id.expand);
				viewHolderPlugin.options = (LinearLayout) convertView.findViewById(R.id.options);
				viewHolderPlugin.pluginIcon = (ImageView) convertView.findViewById(R.id.pluginIcon);
				viewHolderPlugin.pluginName = (TextView) convertView.findViewById(R.id.pluginName);
				viewHolderPlugin.pluginVersion = (TextView) convertView.findViewById(R.id.pluginVersion);
				viewHolderPlugin.description = (TextView) convertView.findViewById(R.id.pluginDescription);
				viewHolderPlugin.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
				viewHolderPlugin.pBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
				convertView.setTag(viewHolderPlugin);
			} else {
				viewHolderPlugin = (ViewHolderPlugin) convertView.getTag();
			}
			viewHolderPlugin.pluginName.setText(pendingContextPlugin.getPluginName());
			viewHolderPlugin.pluginVersion.setText(pendingContextPlugin.getVersion().getValue());
			viewHolderPlugin.description.setText(pendingContextPlugin.getPluginDescription());
			if (installProgressMap.containsKey(pendingContextPlugin)) {
				viewHolderPlugin.pBar.setVisibility(View.VISIBLE);
				viewHolderPlugin.pBar.setProgress(installProgressMap.get(pendingContextPlugin));
			} else {
				viewHolderPlugin.pBar.setVisibility(View.GONE);
			}
			if (pendingContextPlugin.getContextPluginType() == ContextPluginType.LIBRARY) {
				viewHolderPlugin.pluginIcon.setImageResource(R.drawable.library);
			} else {
				viewHolderPlugin.pluginIcon.setImageResource(R.drawable.plugin_enabled);
			}
			convertView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (viewHolderPlugin.options.getVisibility() == View.VISIBLE) {
						viewHolderPlugin.expand.setImageResource(R.drawable.ic_action_expand);
						viewHolderPlugin.options.setVisibility(View.GONE);
					} else {
						viewHolderPlugin.expand.setImageResource(R.drawable.ic_action_collapse);
						viewHolderPlugin.options.setVisibility(View.VISIBLE);
					}
				}
			});
			viewHolderPlugin.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					if (arg1) {
						mCheckedState.put(pendingContextPlugin, true);
						pluginsToInstall.add(pendingContextPlugin);
					} else {
						mCheckedState.remove(pendingContextPlugin);
						if (pluginsToInstall.contains(pendingContextPlugin))
							pluginsToInstall.remove(pendingContextPlugin);
					}
				}
			});
			viewHolderPlugin.checkBox.setChecked(mCheckedState.containsKey(pendingContextPlugin));
			return convertView;
		case PendingPluginListItemType.NO_ITEMS_VIEW:
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.no_plugins_list_item, null);
			}
			return convertView;
		}
		return convertView;
	}

	private static class ViewHolderPlugin {
		ImageView expand;
		LinearLayout options;
		ImageView pluginIcon;
		TextView pluginName;
		TextView pluginVersion;
		TextView description;
		CheckBox checkBox;
		ProgressBar pBar;
	}

	private static class ViewHolderHeader {
		TextView repositoryName;
	}

	public ArrayList<ContextPluginInformation> getPluginsToInstall() {
		return pluginsToInstall;
	}

	public void onPluginInstalled(ContextPluginInformation plugin) {
		// Since a new Plugin is installed, it was previously in this pending plugins list with the mCheckedState set to
		// true
		// but it is not anymore so we should update the checked state.
		// This ensures that it does not automatically get checked if it is uninstalled by the user and comes back to
		// the pending plugins list.
		if (mCheckedState.containsKey(plugin)) {
			mCheckedState.remove(plugin);
		}
		if (pluginsToInstall.contains(plugin)) {
			pluginsToInstall.remove(plugin);
		}
	}
}
