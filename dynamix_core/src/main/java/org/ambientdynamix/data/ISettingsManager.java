/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.data;

import java.util.Date;
import java.util.List;

import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.core.ContextPlugin;
import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;
import org.ambientdynamix.util.Repository;

/**
 * Primary interface for user-based settings management. The use of this interface decouples the Dynamix Framework from
 * its underlying database technology, allowing the database type to be changed if needed.
 * 
 * @author Darren Carlson
 */
public interface ISettingsManager {
	public abstract boolean addDynamixApplication(DynamixApplication app);

	/**
	 * Replaces the application with the incoming application, regardless if the app is pending or authorized. Returns
	 * true if the application was updated; false, otherwise.
	 */
	public abstract boolean updateDynamixApplication(DynamixApplication app);

	/**
	 * Removes a DynamixApplication from the List of authorized/pending applications. Returns true if the application
	 * was remove; false, otherwise.
	 */
	public abstract boolean removeDynamixApplication(DynamixApplication app);

	public abstract List<DynamixApplication> getAllDynamixApplications();

	/**
	 * Returns the application for the provided appID from *either* the authorized or pending application lists. Returns
	 * NULL, if the appID is not found.
	 */
	public abstract DynamixApplication getDynamixApplication(String appId);

	/**
	 * Adds the specified ClassLoader to the ISettingsManager. Useful in some object-based database implementations.
	 */
	public abstract void addClassLoader(ClassLoader loader);

	/**
	 * Adds the ContextPlugin to the DynamixSettings object, for each authorized and pending application.
	 */
	public abstract boolean addContextPlugin(ContextPlugin plugin);

	/**
	 * Clears existing settings and creates a new, default settings object.
	 */
	public abstract void clearSettings();

	/**
	 * Closes the database and releases any acquired resources.
	 */
	public abstract void closeDatabase();

	/**
	 * Returns a read-only List of installed ContextPlugins.
	 */
	public abstract List<ContextPlugin> getInstalledContextPlugins();

	/**
	 * Returns the ContextPluginSettings for the specified ContextPlugin
	 */
	public abstract ContextPluginSettings getContextPluginSettings(ContextPlugin plug);

	/**
	 * Returns the List of pending ContextPlugins that have been discovered but not installed.
	 */
	public abstract List<PendingContextPlugin> getPendingContextPlugins();

	/**
	 * Returns the current Dynamix Framework PowerScheme.
	 */
	public abstract PowerScheme getPowerScheme();

	/**
	 * Returns the DynamixSettings object managed by the ISettingsManager
	 */
	public abstract DynamixSettings getSettings();

	/**
	 * Returns True if Dynamix had a clean exit; false otherwise.
	 */
	public abstract Boolean hadCleanExit();

	/**
	 * Opens the database using the specified database path. This method will always be called by the Dynamix Framework
	 * before all other ISettingsManager methods. Implementations should open the database using the incoming path and
	 * acquire any necessary resources needed to manage settings. the Dynamix Framework will not start if this method
	 * throws an exception.
	 * 
	 * @param path
	 *            The path of the database to open.
	 */
	public abstract void openDatabase(String path) throws Exception;

	/**
	 * Removes the specified ClassLoader from the ISettingsManager. Useful in some object-based database
	 * implementations.
	 */
	public abstract void removeClassLoader(ClassLoader loader);

	/**
	 * Removes the ContextPlugin from the List of ContextPlugins.
	 */
	public abstract boolean removeInstalledContextPlugin(ContextPlugin plugin);

	/**
	 * Removes the ContextPluginSettings for the specified ContextPlugin.
	 * 
	 * @param plug
	 *            Returns true if the ContextPluginSettings was removed; false otherwise.
	 */
	public abstract boolean removeContextPluginSettings(ContextPlugin plug);

	/**
	 * Replaces the original plugin with the new plugin while maintaining all original settings
	 */
	public abstract boolean replaceContextPlugin(ContextPlugin originalPlugin, ContextPlugin newPlugin);

	/**
	 * Sets if Dynamix had a clean exit.
	 * 
	 * @param cleanExit
	 *            True if the exit was clean (no errors); false otherwise.
	 */
	public abstract void setCleanExit(Boolean cleanExit);

	/**
	 * Sets the list of available PendingContextPlugins.
	 */
	public abstract void setPendingContextPlugins(List<PendingContextPlugin> pendingPlugins);

	/**
	 * Sets the current Dynamix Framework PowerScheme.
	 */
	public abstract void setPowerScheme(PowerScheme newScheme);

	/**
	 * Replaces the settings for the specified ContextPlugin with the incoming ContextPluginSettings.
	 * 
	 * @param plug
	 *            The ContextPlugin's settings to update
	 * @param plugSet
	 *            The new ContextPluginSettings object that should replace the existing settings.
	 * @return True if the ContextPluginSettings was stored for the ContextPlugin; false otherwise.
	 */
	public abstract boolean storeContextPluginSettings(ContextPlugin plug, ContextPluginSettings settings);

	/**
	 * Updates the specified ContextPlugin.
	 */
	public abstract boolean updateContextPlugin(ContextPlugin plug);

	/**
	 * Returns the last time the repo was modified.
	 */
	public Date getRepoLastModified(String repoId);

	/**
	 * Sets the last time the repo was modified.
	 */
	public void setRepoLastModified(String repoId, Date lastModified);

	/**
	 * Removes the repo from the last modified map.
	 */
	public void clearRepoLastModified(String repoId);

	/**
	 * Clears all existing repos and adds the list of repos.
	 */
	public void setPluginRepositories(List<Repository> repos);

	/**
	 * Adds the plug-in repository to the set of repositories, replacing an existing repo if it was already persisted.
	 */
	public boolean addPluginRepository(Repository repo);

	/**
	 * Removes the plug-in repository from the set of repositories.
	 */
	public boolean removePluginRepository(Repository repo);

	/**
	 * Updates the repository with the incoming values contained in the RepositoryInfo.
	 */
	public boolean updatePluginRepository(Repository repo);

	/**
	 * Returns the repository specified by the url, or null if no mapping could be found.
	 */
	public Repository getPluginRepository(String id);

	/**
	 * Returns the list of persisted repositories.
	 */
	public List<Repository> getPluginRepositories();
}