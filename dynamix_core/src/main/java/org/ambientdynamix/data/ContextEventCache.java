/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.core.ContextPlugin;
import org.ambientdynamix.core.DynamixPreferences;
import org.ambientdynamix.core.FrameworkConstants;
import org.ambientdynamix.core.Utils;
import org.ambientdynamix.event.SourcedContextInfoSet;

import android.util.Log;

/**
 * A self-managing, threaded context result cache that supports several configuration options. The ContextEventCache is
 * used to maintain a recent history of context results, along with their associated security information that is used
 * when re-provisioning context data requested by DynamixApplications.
 * 
 * @author Darren Carlson
 */
public class ContextEventCache implements Runnable {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private int maxCacheMills;
	private int cullInterval;
	private int maxCapacity;
	private volatile boolean running;
	private ArrayBlockingQueue<ContextResultCacheEntry> queue;

	/**
	 * Creates a new ContextEventCache.
	 * 
	 * @param maxCacheMills
	 *            : The maximum time (in milliseconds) a CacheEntry is allowed to stay in the cache.
	 * @param cullInterval
	 *            : How often (in milliseconds) to check for CacheEntry expiration.
	 * @param maxCapacity
	 *            : The maximum capacity of the cache. If exceeded, the cache will remove the oldest CacheEntry to make
	 *            room.
	 */
	public ContextEventCache(int maxCacheMills, int cullIntervalMills, int maxCapacity) {
		this.maxCacheMills = maxCacheMills;
		this.cullInterval = cullIntervalMills;
		this.maxCapacity = maxCapacity;
		queue = new ArrayBlockingQueue<ContextResultCacheEntry>(this.maxCapacity, true);
	}

	public synchronized void cacheResult(ContextPlugin source, SourcedContextInfoSet eventGroup) {
		if (FrameworkConstants.DISABLE_CONTEXT_RESULT_CACHING)
			return;
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.v(TAG, "Caching SourcedContextInfoSet: " + eventGroup + " from " + source);
		if (eventGroup.getTotalIContextInfoBytes() <= 51200) {
			// Only cache if the event doesn't expire, or if the expiration time is longer than 0 milliseconds
			if (!eventGroup.expires() || eventGroup.getExireMills() > 0) {
				// Don't cache large events
				if (!queue.offer(new ContextResultCacheEntry(source, eventGroup, new Timestamp(new Date().getTime())))) {
					if (DynamixPreferences.isDetailedLoggingEnabled())
						Log.d(TAG, "Cache was full... removing an element to make space");
					queue.remove();
					cacheResult(source, eventGroup);
				}
			}
		} else if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.w(TAG, "Can't cache event of size: " + eventGroup.getTotalIContextInfoBytes());
	}

	public synchronized void cacheResult(IContextHandler targetHandler, ContextPlugin source,
			SourcedContextInfoSet eventGroup) {
		if (FrameworkConstants.DISABLE_CONTEXT_RESULT_CACHING)
			return;
		cacheResult(targetHandler, null, source, eventGroup);
	}

	public synchronized void cacheResult(IContextHandler targetHandler, IContextListener listener,
			ContextPlugin source, SourcedContextInfoSet eventGroup) {
		if (FrameworkConstants.DISABLE_CONTEXT_RESULT_CACHING)
			return;
		if (targetHandler == null)
			Log.e(TAG, "cacheResult: Target handler NULL for " + source);
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.v(TAG, "Caching SourcedContextInfoSet: " + eventGroup + " from " + source + " for handler "
					+ targetHandler);
		if (eventGroup.getTotalIContextInfoBytes() <= 51200) {
			// Only cache if the event doesn't expire, or if the expiration time is longer than 0 milliseconds
			if (!eventGroup.expires() || eventGroup.getExireMills() > 0) {
				ContextResultCacheEntry entry = null;
				if (listener != null)
					entry = new ContextResultCacheEntry(targetHandler, listener, source, eventGroup, new Timestamp(
							new Date().getTime()));
				else
					entry = new ContextResultCacheEntry(targetHandler, source, eventGroup, new Timestamp(
							new Date().getTime()));
				if (!queue.offer(entry)) {
					if (DynamixPreferences.isDetailedLoggingEnabled())
						Log.d(TAG, "Cache was full... removing an element to make space");
					queue.remove();
					cacheResult(targetHandler, listener, source, eventGroup);
				}
			}
		} else if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.w(TAG, "Can't cache event of size: " + eventGroup.getTotalIContextInfoBytes());
	}

	public List<ContextResultCacheEntry> getCachedEvents() {
		return new ArrayList<ContextResultCacheEntry>(Arrays.asList(queue.toArray(new ContextResultCacheEntry[queue
				.size()])));
	}

	public List<ContextResultCacheEntry> getCachedEvents(String contextType) {
		List<ContextResultCacheEntry> snapshot = getCachedEvents();
		for (ContextResultCacheEntry e : snapshot) {
			if (!e.getSourcedContextEventSet().getContextType().equals(contextType))
				snapshot.remove(e);
		}
		return snapshot;
	}

	public void removeContextEvents(ContextPlugin plug) {
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.v(TAG, "Removing events for: " + plug);
		for (ContextResultCacheEntry event : queue) {
			if (event.getEventSource().equals(plug)) {
				if (DynamixPreferences.isDetailedLoggingEnabled())
					Log.v(TAG, "Removing: " + event);
				queue.remove(event);
			}
		}
	}

	public void removeContextResults(IContextListener listener, String contextType) {
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.v(TAG, "Removing events for: " + listener);
		List<ContextResultCacheEntry> remove = new ArrayList<ContextResultCacheEntry>();
		synchronized (queue) {
			for (ContextResultCacheEntry event : queue) {
				if (event.hasTargetListener() && event.getTargetHandler().asBinder().equals(listener.asBinder())) {
					if (event.getSourcedContextEventSet().getContextType().equalsIgnoreCase(contextType)) {
						if (DynamixPreferences.isDetailedLoggingEnabled())
							Log.v(TAG, "Removing: " + event);
						remove.add(event);
					}
				}
			}
			queue.removeAll(remove);
		}
	}

	public void removeContextResults(IContextHandler handler, String contextType) {
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.v(TAG, "Removing events for: " + handler);
		List<ContextResultCacheEntry> remove = new ArrayList<ContextResultCacheEntry>();
		synchronized (queue) {
			for (ContextResultCacheEntry event : queue) {
				if (event.hasTargetHandler() && event.getTargetHandler().asBinder().equals(handler.asBinder())) {
					if (event.getSourcedContextEventSet().getContextType().equalsIgnoreCase(contextType)) {
						if (DynamixPreferences.isDetailedLoggingEnabled())
							Log.v(TAG, "Removing: " + event);
						remove.add(event);
					}
				}
			}
			queue.removeAll(remove);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ambientdynamix.data.IContextEventCache#run()
	 */
	@Override
	public void run() {
		Log.i(TAG, "ContextEventCache is running!");
		while (running) {
			// Sleep for the cullInterval
			try {
				Thread.sleep(cullInterval);
			} catch (InterruptedException e1) {
				Log.e(TAG, e1.getMessage());
				e1.printStackTrace();
				running = false;
			}
			List<ContextResultCacheEntry> remove = new ArrayList<ContextResultCacheEntry>();
			// Check for expired ContextData elements
			Date now = new Date();
			for (ContextResultCacheEntry e : queue) {
				// Check for autoExpire
				if (now.getTime() - e.getCachedTime().getTime() > this.maxCacheMills) {
					remove.add(e);
					if (DynamixPreferences.isDetailedLoggingEnabled())
						Log.v(TAG, "CacheEntry " + e + " auto expired after " + maxCacheMills + "ms");
					break;
				}
				// Check if the EventGroup itself has expired
				if (e.getSourcedContextEventSet().expires()) {
					// Error check first
					if (e.getSourcedContextEventSet().getExpireTime() == null) {
						Log.w(TAG, "ContextEventSet had null ExpireTime: " + e.getSourcedContextEventSet());
					} else if (now.after(e.getSourcedContextEventSet().getExpireTime())) {
						if (DynamixPreferences.isDetailedLoggingEnabled())
							Log.v(TAG, "CacheEntry expired: " + e);
						remove.add(e);
					}
				}
			}
			// Remove out-dated cache entries
			for (ContextResultCacheEntry e : remove) {
				queue.remove(e);
			}
		}
		Log.d(TAG, "ContextEventCache has stopped!");
	}

	/*
	 * (non-Javadoc)
	 * @see org.ambientdynamix.data.IContextEventCache#start()
	 */
	public synchronized void start() {
		if (!running) {
			Log.d(TAG, "Starting ContextEventCache...");
			running = true;
			Utils.dispatch(true, this);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ambientdynamix.data.IContextEventCache#stop()
	 */
	public synchronized void stop() {
		if (running) {
			Log.d(TAG, "Stopping ContextEventCache...");
			running = false;
			queue.clear();
		}
	}
}