/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginStateListener;
import org.ambientdynamix.api.contextplugin.Message;
import org.ambientdynamix.api.contextplugin.PluginState;

import android.util.Log;

/**
 * Stateful wrapper for ContextPluginRuntimes.
 * 
 * @author Darren Carlson
 */
public class ContextPluginRuntimeWrapper {
	private final String TAG = this.getClass().getSimpleName();
	// Private data
	private volatile PluginState currentState;
	private volatile ContextPluginRuntime runtime;
	private volatile ContextPlugin plugin;
	private volatile boolean executing;
	private List<IPluginStateListener> stateListeners = new ArrayList<IPluginStateListener>();
	private MessageHandler handler = null;

	/**
	 * Creates a ContextPluginRuntimeWrapper with state new.
	 */
	public ContextPluginRuntimeWrapper(ContextPlugin plugin) {
		this.plugin = plugin;
		setState(PluginState.NEW, true);
	}

	/**
	 * Adds a IPluginStateListener.
	 */
	public void addStateListener(IPluginStateListener listener) {
		if (listener != null)
			synchronized (stateListeners) {
				if (!stateListeners.contains(listener))
					stateListeners.add(listener);
			}
	}

	/**
	 * Removes a previously registered IPluginStateListener.
	 */
	public boolean removeStateListener(IPluginStateListener listener) {
		synchronized (stateListeners) {
			if (listener == null)
				return false;
			else
				return stateListeners.remove(listener);
		}
	}

	/**
	 * Clears all current state listeners.
	 */
	public void clearAllStateListeners() {
		synchronized (stateListeners) {
			stateListeners.clear();
		}
	}

	/**
	 * Returns true if the runtime is executing (started).
	 * 
	 * @return
	 */
	public boolean isExecuting() {
		return executing;
	}

	/**
	 * Set true if the runtime is executing (started); false otherwise.
	 * 
	 * @param executing
	 */
	public void setExecuting(boolean executing) {
		this.executing = executing;
	}

	/**
	 * Returns the plug-in associated with this wrapper.
	 */
	public ContextPlugin getParentPlugin() {
		return plugin;
	}

	/**
	 * Returns the wrapped ContextPluginRuntime.
	 */
	public ContextPluginRuntime getContextPluginRuntime() {
		return runtime;
	}

	/**
	 * Sets the ContextPluginRuntime.
	 */
	public void setContextPluginRuntime(ContextPluginRuntime runtime) {
		this.runtime = runtime;
		if (handler != null)
			handler.destroy();
		this.handler = new MessageHandler(runtime);
	}

	/**
	 * Returns the current PluginState.
	 * 
	 * @see PluginState
	 */
	public final synchronized PluginState getState() {
		return this.currentState;
	}

	/**
	 * Sets the current plug-in state, sending related events to registered state listeners.
	 * 
	 * @see PluginState
	 * @see IPluginStateListener
	 */
	public final synchronized void setState(PluginState newState, boolean sendStateEvent) {
		if (newState == null)
			throw new RuntimeException("PluginState cannot be NULL!");
		else {
			this.currentState = newState;
			if (newState == PluginState.STARTED) {
				if (handler != null)
					handler.start();
			} else if (newState == PluginState.STOPPING) {
				// Stop message processing
				if (handler != null)
					handler.stop();
			} else if (newState == PluginState.DESTROYED || newState == PluginState.ERROR) {
				if (handler != null)
					handler.destroy();
			}
			if (sendStateEvent) {
				synchronized (stateListeners) {
					for (final IPluginStateListener listener : stateListeners) {
						if (currentState == PluginState.NEW) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									listener.onNew(plugin.getContextPluginInformation());
								}
							});
						}
						if (currentState == PluginState.INITIALIZING) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									listener.onInitialized(plugin.getContextPluginInformation());
								}
							});
						}
						if (currentState == PluginState.INITIALIZED) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									listener.onInitialized(plugin.getContextPluginInformation());
								}
							});
						}
						if (currentState == PluginState.STARTING) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									listener.onStarting(plugin.getContextPluginInformation());
								}
							});
						}
						if (currentState == PluginState.STARTED) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									listener.onStarted(plugin.getContextPluginInformation());
								}
							});
						}
						if (currentState == PluginState.STOPPING) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									listener.onStopping(plugin.getContextPluginInformation());
								}
							});
						}
						if (currentState == PluginState.DESTROYED) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									listener.onDestroyed(plugin.getContextPluginInformation());
								}
							});
						}
						if (currentState == PluginState.ERROR) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									listener.onError(plugin.getContextPluginInformation());
								}
							});
						}
					}
				}
			}
		}
	}

	/**
	 * Adds the message to the message queue, which processes messages when the plug-in is in state STARTED.
	 * 
	 */
	public final void enqueueMessage(Message m) {
		if (handler != null)
			handler.enqueueMessage(m);
	}

	/**
	 * Clears the message queue.
	 */
	public final void clearMessageQueue() {
		if (handler != null)
			handler.clear();
	}

	private class MessageHandler implements Runnable {
		private LinkedBlockingQueue<Message> messageQ = null;
		private boolean processing = false;
		private volatile ContextPluginRuntime runtime;

		MessageHandler(ContextPluginRuntime runtime) {
			this.runtime = runtime;
		}

		public synchronized void start() {
			if (!processing) {
				this.processing = true;
				Utils.dispatch(true, this);
			}
		}

		public void stop() {
			this.processing = false;
		}

		public void destroy() {
			stop();
			clear();
		}

		public void clear() {
			if (messageQ != null)
				messageQ.clear();
		}

		public synchronized void enqueueMessage(Message m) {
			if (messageQ != null)
				messageQ.add(m);
		}

		@Override
		public void run() {
			if (messageQ == null)
				messageQ = new LinkedBlockingQueue<Message>();
			Log.v(TAG, "Starting message processing for wrapper: " + this);
			while (processing) {
				try {
					// Try to grab an installed (the queue will block until something is inserted)
					Message m = messageQ.poll(100, TimeUnit.MILLISECONDS);
					if (m != null) {
						Log.v(TAG, "Sending message: " + m);
						runtime.onMessageReceived(m);
					}
				} catch (Exception e) {
					Log.w(TAG, "Message processor exception: " + e);
				}
			}
			Log.v(TAG, "Stopped message processing for wrapper: " + this);
		}
	}
}
