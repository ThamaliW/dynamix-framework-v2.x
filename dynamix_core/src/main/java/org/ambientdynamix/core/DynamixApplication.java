/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.Utils;
import org.ambientdynamix.api.contextplugin.security.Permission;
import org.ambientdynamix.core.AppFacadeBinder.RemoteProcessMonitor;
import org.ambientdynamix.core.FirewallRule.FirewallAccess;
import org.ambientdynamix.remote.RemotePairing;

import java.util.*;

/**
 * Represents the state of a Dynamix-based application, which interacts with the Dynamix Framework via the application
 * API. Interactions between applications and Dynamix are mediated by Dynamix's security model, which uses process
 * identification along with context firewall rules to determine which context types are accessible by the application.
 * 
 * @author Darren Carlson
 */
public class DynamixApplication implements Parcelable {
	/**
	 * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
	 */
	public static final Parcelable.Creator<DynamixApplication> CREATOR = new Parcelable.Creator<DynamixApplication>() {
		@Override
		public DynamixApplication createFromParcel(Parcel in) {
			return new DynamixApplication(in);
		}

		@Override
		public DynamixApplication[] newArray(int size) {
			return new DynamixApplication[size];
		}
	};
	/**
	 * Key for the application's package name (String), which is available in the App Details bundle for AIDL apps (see
	 * 'getAppDetails').
	 */
	public final static String PACKAGE_NAME = "PACKAGE_NAME";
	/**
	 * Key for the application's version code (int), which is available in the App Details bundle for AIDL apps (see
	 * 'getAppDetails').
	 */
	public final static String VERSION_CODE = "VERSION_CODE";
	/**
	 * Key for the application's version name (String), which is available in the App Details bundle for AIDL apps (see
	 * 'getAppDetails').
	 */
	public final static String VERSION_NAME = "VERSION_NAME";
	/**
	 * Key for the application's favicon uri (String), which is available in the App Details bundle for Web apps (see
	 * 'getAppDetails').
	 */
	public final static String FAVICON_URI = "FAVICON_URI";
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private static final long serialVersionUID = 7240238549835623638L;
	private String appID;
	private String packageName;
	private int versionCode;
	private String versionName;
	private String appName;
	private String appDescription;
	private boolean enabled = true;
	private boolean mayInstallPlugins = false;
	private Date lastContact;
	private List<FirewallRule> appFirewallRules = new ArrayList<FirewallRule>();
	private transient List<FirewallRule> sessionFirewallRules = new ArrayList<FirewallRule>();
	private transient List<RemoteProcessMonitor> processMonitors = new ArrayList<RemoteProcessMonitor>();
	private List<Permission> permissions = new ArrayList<Permission>();
	private boolean admin = false;
	private Map<String, String> appDetails = new HashMap<String, String>();
	private APP_TYPE appType;
	private boolean blocked = false;
	private RemotePairing pairing;

	public enum APP_TYPE {
		AIDL, WEB, PLUG_IN, INTERNAL
	}

	/**
	 * Empty constructor required for DB4o.
	 */
	public DynamixApplication() {
	}

	/**
	 * Creates an AIDL-based DynamixApplication from the incoming Android ApplicationInfo.
	 */
	public DynamixApplication(PackageManager pm, PackageInfo pkg, ApplicationInfo info) {
		/*
		 * Tips on getting detailed App Info (icon, etc.):
		 * http://android.git.kernel.org/?p=platform/packages/apps/Settings
		 * .git;a=blob;f=src/com/android/settings/InstalledAppDetails .java;h=d05014bfc027dc7e280de2eeb07f80207cefd32c
		 * ;hb=68b8069862314a26dbacd28d13dd4c6bea8b6141
		 */
		this.appName = info.loadLabel(pm).toString();
		this.appDescription = "Directory: " + info.sourceDir;
		this.packageName = info.packageName;
		this.versionCode = pkg.versionCode;
		this.versionName = pkg.versionName;
		this.appType = APP_TYPE.AIDL;
		this.appID = Integer.toString(info.uid);
		this.appDetails.put(PACKAGE_NAME, packageName);
		this.appDetails.put(VERSION_CODE, Integer.toString(versionCode));
		this.appDetails.put(VERSION_NAME, versionName);
		Log.v(TAG, "Created: " + this);
	}

	/**
	 * Creates a DynamixApplication for the specified id, name and type. For Web applications, the appName is the
	 * applications URL, including the hostname + port + path. For specifically named resources, the id includes the
	 * name of the resource listed on the path (e.g., ambientdynamix.org/example/index.htm). For unnamed resources, the
	 * id always includes the final trailing slash (e.g., ambientdynamix.org/example/).
	 */
	public DynamixApplication(APP_TYPE appType, String id, String appName) {
		this.appType = appType;
		this.appName = appName;
		this.appID = id;
		Log.v(TAG, "Created: " + this);
	}

	/**
	 * Creates a remotely paired web app.
	 */
	public DynamixApplication(String id, String appName, RemotePairing pairing) {
		this.appType = APP_TYPE.WEB;
		this.appName = appName;
		this.appID = id;
		this.pairing = pairing;
	}

	/*
	 * TODO: Experiments in getting favicon. This probably doesn't work.
	 * http://stackoverflow.com/questions/2690503/can-bitmapfactory
	 * -decodefile-handle-ico-windows-icons-files/2690923#2690923
	 * http://stackoverflow.com/questions/18678848/is-there-a-way-to-decode-a-ico-file-to-a-resolution-bigger-than-16x16
	 * http://stackoverflow.com/questions/5830533/how-can-i-convert-an-icon-to-an-image
	 */
	// private void setupWebApp() throws IOException {
	// Connection connection = Jsoup.connect(appID);
	// Document doc = connection.get();
	// Element faviconElement = doc.head().select("link[href~=.*\\.ico]").first();
	// String faviconUrl = faviconElement.attr("href");
	// Response resultImageResponse = Jsoup.connect(faviconUrl).ignoreContentType(true).execute();
	// byte[] icoBytes = resultImageResponse.bodyAsBytes();
	// Bitmap icon = BitmapFactory.decodeByteArray(icoBytes, 0, icoBytes.length);
	// }
	/**
	 * Returns true if this application has been completely blocked by the firewall; false otherwise.
	 */
	public boolean isBlocked() {
		return blocked;
	}

	/**
	 * Adds the key and value to the app's details.
	 */
	public void addAppDetail(String key, String value) {
		synchronized (appDetails) {
			appDetails.put(key, value);
		}
	}

	/**
	 * Removes the key (and associated value) from the app's details.
	 */
	public void removeAppDetail(String key) {
		synchronized (appDetails) {
			appDetails.remove(key);
		}
	}

	/**
	 * Removes all of the app's details.
	 */
	public void clearAppDetails() {
		synchronized (appDetails) {
			appDetails.clear();
		}
	}

	/**
	 * Set true if this application has been completely blocked by the firewall; false otherwise.
	 */
	public void setBlocked(boolean isBlocked) {
		this.blocked = isBlocked;
	}

	/**
	 * Returns true if this app is remotely paired; false otherwise.
	 */
	public boolean isRemotelyPaired() {
		return pairing != null;
	}

	/**
	 * Returns the remote pairing, or null if this app is not remotely paired.
	 */
	public RemotePairing getRemotePairing() {
		return this.pairing;
	}

	/**
	 * Sets the remote pairing, which may be null if this app is not remotely paired.
	 */
	public void setRemotePairing(RemotePairing pairing) {
		this.pairing = pairing;
	}

	/**
	 * Returns true if firewall access is granted for the specified context type and plug-in; false otherwise.
	 */
	public boolean isAccessGranted(String contextType, ContextPluginInformation plug) {
		if (admin)
			return true;
		else {
			FirewallRule rule = getFirewallRule(contextType, plug);
			if (rule != null) {
				return (rule.getFirewallAccess() == FirewallAccess.ALLOWED_ALWAYS || rule.getFirewallAccess() == FirewallAccess.ALLOWED_SESSION);
			} else
				return false;
		}
	}

	/**
	 * Returns true if firewall access is granted for the specified context type and plug-in list; false otherwise.
	 */
	public boolean isAccessGranted(String contextType, List<ContextPluginInformation> plugins) {
		if (admin)
			return true;
		else {
			FirewallRule rule = getFirewallRule(contextType, plugins);
			if (rule != null) {
				return (rule.getFirewallAccess() == FirewallAccess.ALLOWED_ALWAYS || rule.getFirewallAccess() == FirewallAccess.ALLOWED_SESSION);
			} else
				return false;
		}
	}

	/**
	 * Returns all firewall rules associated with this app (both session and persisted).
	 */
	public List<FirewallRule> getAllFirewallRules() {
		synchronized (appFirewallRules) {
			List<FirewallRule> returnList = new ArrayList<FirewallRule>(appFirewallRules);
			returnList.addAll(sessionFirewallRules);
			return returnList;
		}
	}

	/**
	 * Returns the firewall rule associated with the incoming context type and plug-in, or null if no rule exists.
	 */
	public FirewallRule getFirewallRule(String contextType, ContextPluginInformation plugin) {
		synchronized (appFirewallRules) {
			// FirewallRule searchRule = FirewallRule.createSearchRule(contextType, plugins);
			for (FirewallRule rule : getAllFirewallRules()) {
				if (rule.getContextTypeId().equalsIgnoreCase(contextType))
					if (rule.getPlugins().contains(plugin))
						return rule;
			}
		}
		return null;
	}

	/**
	 * Returns the firewall rule associated with the incoming context type and plug-in list, or null if no rule exists.
	 */
	public FirewallRule getFirewallRule(String contextType, List<ContextPluginInformation> plugins) {
		synchronized (appFirewallRules) {
			for (FirewallRule rule : getAllFirewallRules()) {
				if (rule.getContextTypeId().equalsIgnoreCase(contextType))
					if (rule.getPlugins().containsAll(plugins))
						return rule;
			}
		}
		return null;
	}

	/**
	 * Replaces all firewall rules with the incoming firewall rule list.
	 */
	public void setFirewallRules(List<FirewallRule> newRules) {
		synchronized (appFirewallRules) {
			clearAllFirewallRules();
			for (FirewallRule rule : newRules)
				addFirewallRule(rule);
		}
	}

	/**
	 * Adds the firewall rule to the app.
	 */
	public boolean addFirewallRule(FirewallRule rule) {
		// First, clear the rule completely
		sessionFirewallRules.remove(rule);
		appFirewallRules.remove(rule);
		// Then handle session-based rules
		synchronized (appFirewallRules) {
			if (rule.getFirewallAccess() == FirewallAccess.ALLOWED_SESSION) {
				return sessionFirewallRules.add(rule);
			}
		}
		// Finally, handle app-based rules
		synchronized (appFirewallRules) {
			return appFirewallRules.add(rule);
		}
	}

	/**
	 * Removes the firewall rule from the app.
	 */
	public boolean removeFirewallRule(FirewallRule rule) {
		synchronized (appFirewallRules) {
			if (rule.getFirewallAccess() == FirewallAccess.ALLOWED_SESSION) {
				return sessionFirewallRules.remove(rule);
			} else {
				synchronized (appFirewallRules) {
					if (appFirewallRules.contains(rule))
						return appFirewallRules.remove(rule);
					else
						return false;
				}
			}
		}
	}

	/**
	 * Clears all firewall rules for this app (both session and persisted).
	 */
	public void clearAllFirewallRules() {
		synchronized (appFirewallRules) {
			appFirewallRules.clear();
			sessionFirewallRules.clear();
		}
	}

	/**
	 * Clears all session firewall rules, leaving persisted rules.
	 */
	public void clearSessionFirewallRules() {
		synchronized (sessionFirewallRules) {
			sessionFirewallRules.clear();
		}
	}

	/**
	 * Returns true if this app has the incoming firewall rule (session or persisted).
	 */
	public boolean hasFirewallRule(FirewallRule rule) {
		synchronized (appFirewallRules) {
			return appFirewallRules.contains(rule) || sessionFirewallRules.contains(rule);
		}
	}

	/**
	 * Removes the firewall rules registered to the specified plug-in.
	 */
	public synchronized void removeFirewallRulesForPlugin(ContextPluginInformation plug) {
		synchronized (appFirewallRules) {
			List<FirewallRule> removeList = new ArrayList<FirewallRule>();
			for (FirewallRule rule : getAllFirewallRules()) {
				if (rule.getPlugins().equals(plug))
					removeList.add(rule);
			}
			appFirewallRules.removeAll(removeList);
			sessionFirewallRules.removeAll(removeList);
		}
	}

	/**
	 * Used to remove any ALLOWED_SESSION and DENIED firewall rules from the app's persisted rules. Mixups in
	 * ALLOWED_SESSION rules can happen if the rules have been modified after storage.
	 */
	public void refreshFirewallRules() {
		synchronized (appFirewallRules) {
			List<FirewallRule> removeList = new ArrayList<FirewallRule>();
			for (FirewallRule rule : appFirewallRules)
				if (rule.getFirewallAccess() == FirewallAccess.ALLOWED_SESSION
						|| rule.getFirewallAccess() == FirewallAccess.DENIED)
					removeList.add(rule);
			// Remove misrouted rules
			appFirewallRules.removeAll(removeList);
			// Route ALLOWED_SESSION into session rules
			for (FirewallRule rule : removeList) {
				if (!sessionFirewallRules.contains(rule))
					if (rule.getFirewallAccess() == FirewallAccess.ALLOWED_SESSION)
						sessionFirewallRules.add(rule);
			}
		}
	}

	/**
	 * Adds a process monitor to the app, which is used to detect if remote apps disconnect from Dynamix.
	 */
	public void addProcessMonitor(RemoteProcessMonitor monitor) {
		synchronized (processMonitors) {
			if (!processMonitors.contains(monitor))
				processMonitors.add(monitor);
		}
	}

	/**
	 * Removes the process monitor from the app, calling unlinkToDeath on the monitor.
	 */
	public void removeProcessMonitor(IBinder binder) {
		synchronized (processMonitors) {
			RemoteProcessMonitor found = null;
			for (RemoteProcessMonitor monitor : processMonitors) {
				if (monitor.getBinder().equals(binder)) {
					monitor.getBinder().unlinkToDeath(monitor, 0);
					found = monitor;
					break;
				}
			}
			if (found != null)
				processMonitors.remove(found);
		}
	}

	/**
	 * Returns this application's name.
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * Returns this application's description.
	 */
	public String getAppDescription() {
		return appDescription;
	}

	/**
	 * Sets this application's description.
	 */
	public void setAppDescription(String appDescription) {
		this.appDescription = appDescription;
	}

	/**
	 * Returns this application's APP_TYPE.
	 */
	public APP_TYPE getAppType() {
		return this.appType;
	}

	@Override
	public boolean equals(Object candidate) {
		// First determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Ok, they are the same class... check if their id's are the same
		DynamixApplication other = (DynamixApplication) candidate;
		return this.getAppID().equalsIgnoreCase(other.getAppID());
	}

	// HashCode Example: http://www.javafaq.nu/java-example-code-175.html
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.appID.hashCode();
		return result;
	}

	/**
	 * Returns the unique identifier of this application.
	 */
	public String getAppID() {
		return this.appID;
	}

	/**
	 * Returns the description of the application as a string.
	 */
	public String getDescription() {
		return this.appDescription;
	}

	/**
	 * Returns the 'friendly' name of the application as a string.
	 */
	public String getName() {
		return this.appName;
	}

	/**
	 * Returns a List of type Permission for the application
	 */
	public List<Permission> getPermissions() {
		return permissions;
	}

	/**
	 * Returns a status message for this application intended for display.
	 */
	public String getStatusString() {
		return "ID: " + this.getAppID();
	}

	/**
	 * Returns true if this application enabled; false otherwise
	 */
	public boolean isEnabled() {
		return this.enabled;
	}

	/**
	 * Returns whether or not this app may install context plugins
	 */
	public boolean mayInstallPlugins() {
		return mayInstallPlugins;
	}

	/**
	 * Returns true if this is an admin app (full permissions); false otherwise;
	 */
	public boolean isAdmin() {
		return admin;
	}

	/**
	 * Set true if this is an admin app (full permissions); false otherwise;
	 */
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	/**
	 * Update our last contact time to the current system time.
	 */
	public void pingConnected() {
		lastContact = new Date();
	}

	/**
	 * Sets whether this application is enabled or not
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Sets whether or not this app may install context plugins
	 */
	public void setMayInstallPlugins(boolean mayInstallPlugins) {
		this.mayInstallPlugins = mayInstallPlugins;
	}

	/**
	 * Sets a List of type Permission for the application
	 */
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	@Override
	public String toString() {
		return "Dynamix application " + this.appName + " with id " + this.appID + " of type " + appType;
	}

	public Map<String, String> getAppDetails() {
		return appDetails;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	private DynamixApplication(Parcel in) {
		this.appID = in.readString();
		this.appType = getAppType(in.readInt());
		this.appName = in.readString();
		this.packageName = in.readString();
		this.versionCode = in.readInt();
		this.versionName = in.readString();
		this.appDescription = in.readString();
		this.enabled = Utils.readBoolean(in);
		this.admin = Utils.readBoolean(in);
		this.mayInstallPlugins = Utils.readBoolean(in);
		this.lastContact = (Date) in.readSerializable();
		in.readList(appFirewallRules, getClass().getClassLoader());
		in.readList(sessionFirewallRules, getClass().getClassLoader());
		in.readList(permissions, getClass().getClassLoader());
		this.appDetails = in.readHashMap(getClass().getClassLoader());
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(this.appID);
		out.writeInt(getAppTypeInt(appType));
		out.writeString(this.appName);
		out.writeString(this.packageName);
		out.writeInt(this.versionCode);
		out.writeString(this.versionName);
		out.writeString(appDescription);
		Utils.writeBoolean(out, enabled);
		Utils.writeBoolean(out, admin);
		Utils.writeBoolean(out, mayInstallPlugins);
		out.writeSerializable(lastContact);
		out.writeList(appFirewallRules);
		out.writeList(sessionFirewallRules);
		out.writeList(permissions);
		out.writeMap(appDetails);
	}

	private APP_TYPE getAppType(int i) {
		switch (i) {
		case 0:
			return APP_TYPE.AIDL;
		case 1:
			return APP_TYPE.INTERNAL;
		case 2:
			return APP_TYPE.PLUG_IN;
		case 3:
			return APP_TYPE.WEB;
		}
		Log.w(TAG, "No APP_TYPE for int: " + i);
		return null;
	}

	private int getAppTypeInt(APP_TYPE type) {
		switch (type) {
		case AIDL:
			return 0;
		case INTERNAL:
			return 1;
		case PLUG_IN:
			return 2;
		case WEB:
			return 3;
		}
		Log.w(TAG, "No int for APP_TYPE: " + type);
		return -1;
	}
}