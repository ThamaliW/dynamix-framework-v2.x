/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources.NotFoundException;
import android.os.Binder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

import org.ambientdynamix.api.application.AppConstants.PluginInstallStatus;
import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.DynamixFeature;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.IContextSupportCallback;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IPluginInstallCallback;
import org.ambientdynamix.api.application.ISessionCallback;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.RepositoryInfo;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.application.VersionedPlugin;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.IPluginView;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.api.contextplugin.PluginState;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.core.ContextPluginRuntimeMethodRunners.HandleContextRequest;
import org.ambientdynamix.core.FirewallRule.FirewallAccess;
import org.ambientdynamix.core.FrameworkConstants.StartState;
import org.ambientdynamix.core.UpdateManager.IContextPluginUpdateListener;
import org.ambientdynamix.core.UpdateManager.IDynamixUpdateListener;
import org.ambientdynamix.data.ContextEventCache;
import org.ambientdynamix.data.DB4oSettingsManager;
import org.ambientdynamix.data.FrameworkConfiguration;
import org.ambientdynamix.data.ISettingsManager;
import org.ambientdynamix.security.CryptoUtils;
import org.ambientdynamix.security.TrustedCert;
import org.ambientdynamix.ui.activities.HomeActivity;
import org.ambientdynamix.ui.firewall.PopupActivity;
import org.ambientdynamix.update.DynamixUpdates;
import org.ambientdynamix.update.TrustedCertBinder;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;
import org.ambientdynamix.util.AndroidForeground;
import org.ambientdynamix.util.Repository;
import org.ambientdynamix.web.WebConnector;
import org.osgi.framework.ServiceEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;

/**
 * The DynamixService is the primary implementation of the Dynamix Framework on the Android platform. Broadly, Dynamix
 * provides dynamic and secure context interactions for Android-based devices. Dynamix continually analyzes the hardware
 * platform and software capabilities of the device, dynamically discovering, downloading and integrating appropriate
 * context plug-ins on-the-fly. Context plugins can be installed from a variety of local or network based repositories.
 * Context plugins are executed within a secure sandbox provided by the OSGi security model and the Dynamix Framework
 * security model. During runtime, ContextPlugins operate in conjunction with an associated ContextPluginRuntime, which
 * does the actual context interaction work; generating ContextEvents as new (or updated) IContextInfo are discovered.
 * As new context information is discovered, it is provisioned to authorized Dynamix applications via the Dynamix
 * Application API, which includes the IDynamixFacade interface, IDynamixListener interface, IContextInfo interface and
 * ContextEvent class. Users interact with the DynamixService through its management interface, which consists of a set
 * of Android Activities arranged in a Tab-based layout (see the org.ambientdynamix.android package). In addition,
 * Dynamix uses the Android notification system to alert users of Dynamix events and state changes. Importantly, the
 * Dynamix Service extends Android Service, allowing it to continually serve dependent clients (DynamixApplications) as
 * a background service.
 * <p/>
 * the DynamixService uses several sub-systems. First, it utilizes the ContextManager to manage dynamically installable
 * ContextPlugins. Related, the Dynamix Service utilizes the OSGIManager to handle dynamic download, instantiation and
 * runtime management of ContextPluginRuntimes. Interactions with Dynamix applications over the Dynamix Application API
 * are handled by the AppFacadeBinder, which communicates over AIDL to 'external' Dynamix applications that are running
 * within their own process. Context events are cached and managed by the ContextDataCache, which can be used to
 * securely retrieve a list of recent context events. Interactions with the Android notification system are handled by
 * the Dynamix Service and AndroidNotification objects. The SettingsManager provides high-performance, coordinated
 * access to Dynamix Framework settings. The SettingsManager uses Dynamix's persistence layer, which provides support
 * for a variety of underlying database technologies through the ISettingsManager interface.
 *
 * @author Darren Carlson
 * @see OSGIManager
 * @see ContextManager
 * @see ISettingsManager
 * @see AppFacadeBinder
 * @see ContextEventCache
 */
public final class DynamixService extends Service {
    // Private static data
    private final static String TAG = DynamixService.class.getSimpleName();
    private static FrameworkConfiguration config;
    private static OSGIManager OsgiMgr;
    private static ContextManager ContextMgr;
    private static DynamixService service;
    private static Context androidContext;
    private static volatile boolean startRequested;
    private static volatile boolean androidServiceRunning;
    private static volatile BootState bootState = BootState.NOT_BOOTED;
    private static volatile StartState startState = StartState.STOPPED;
    private static Activity baseActivity;
    private static UUID frameworkSessionId;
    private static Handler uiHandler = new Handler();
    private static CountDownTimer contextPlugUpdateTimer;
    private static AppFacadeBinder facadeBinder = null;
    private static ISettingsManager SettingsManager;
    private static boolean embeddedMode = false;
    private static ClassLoader embeddedHostClassLoader;
    private static List<IDynamixFrameworkListener> frameworkListeners = new ArrayList<IDynamixFrameworkListener>();
    private static PendingIntent RESTART_INTENT;
    private static String keyStorePath;
    private static FirewallReceiver fwReceiver = new FirewallReceiver();
    private static Thread firewallRequestMonitor;
    private static Queue<FirewallBinding> firewallBindingQueue = new LinkedList<FirewallBinding>();
    private static FirewallBinding currentFirewallBinding;
    private final static Handler uiHandle = new Handler();
    // Private instance data
    private BroadcastReceiver sleepReceiver;
    private BroadcastReceiver wakeReceiver;
    private PendingIntent restartIntent;
    private static boolean recoveryMode = false;
    private AndroidForeground foregroundHandler;

    /**
     * Adds a Dynamix Framework listener.
     */
    public static void addDynamixFrameworkListener(IDynamixFrameworkListener listener) {
        synchronized (frameworkListeners) {
            if (listener != null && !frameworkListeners.contains(listener))
                frameworkListeners.add(listener);
        }
    }

    /**
     * Returns true if Dynamix is running in recovery mode (e.g., after a crash); false, otherwise. In recovery mode,
     * Dynamix will initialize but not start (to allow problemantic plug-ins to be uninstalled).
     *
     * @return
     */
    public static boolean isInRecoveryMode() {
        return recoveryMode;
    }

    /**
     * Removes a previously registered Dynamix Framework listener.
     *
     * @param listener
     */
    public static void removeDynamixFrameworkListener(IDynamixFrameworkListener listener) {
        synchronized (frameworkListeners) {
            if (listener != null)
                frameworkListeners.remove(listener);
        }
    }

    /**
     * Returns the FrameworkConfiguration.
     */
    static FrameworkConfiguration getConfig() {
        return config;
    }

    /**
     * Returns true if Dynamix is running in embedded mode; false otherwise.
     *
     * @return
     */
    public static boolean isEmbedded() {
        return embeddedMode;
    }

    /**
     * Registers the facade for receiving the session opened callback.
     */
    static void registerSessionOpenedCallback(ISessionCallback callback, IDynamixFacade facade) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        SessionManager.registerSessionOpenedCallback(callback, facade);
    }

    /**
     * Returns true if the appId has been registered; false otherwise.
     */
    static boolean hasDynamixApplication(String appId) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        return getDynamixApplication(appId) != null;
    }

    /**
     * Adds the Dynamix Application to the settings.
     */
    public static boolean addDynamixApplicationToSettings(DynamixApplication app) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        boolean result = SettingsManager.addDynamixApplication(app);
        if (result) {
            onDynamixApplicationAdded(app);
        }
        return result;
    }

    /**
     * Returns the ContextPluginSettings for the specified plug-in.
     */
    static ContextPluginSettings getContextPluginSettings(ContextPlugin plug) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        return SettingsManager.getContextPluginSettings(plug);
    }

    /**
     * Updates the specified ContextPlugin.
     */
    static boolean updateContextPlugin(ContextPlugin plug) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        return SettingsManager.updateContextPlugin(plug);
    }

    /**
     * Replaces the settings for the specified ContextPlugin with the incoming ContextPluginSettings.
     *
     * @param plug     The ContextPlugin's settings to update
     * @param settings The new ContextPluginSettings object that should replace the existing settings.
     * @return True if the ContextPluginSettings was stored for the ContextPlugin; false otherwise.
     */
    static boolean storeContextPluginSettings(ContextPlugin plug, ContextPluginSettings settings) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        return SettingsManager.storeContextPluginSettings(plug, settings);
    }

    /**
     * Returns the current Dynamix Framework PowerScheme.
     */
    public static PowerScheme getPowerScheme() {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        return SettingsManager.getPowerScheme();
    }

    /**
     * Returns the list of updates for installed context plug-ins.
     */
    public static List<PendingContextPlugin> getContextPluginUpdates() {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        return UpdateManager.getContextPluginUpdates();
    }

    /**
     * Returns the List of pending plug-ins that have been discovered but have not yet been installed (or are
     * installing).
     */
    public static List<PendingContextPlugin> getPendingContextPlugins() {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        // This returns the live list of plug-ins with updated state.
        return UpdateManager.getPendingContextPlugins();
    }

    /**
     * Returns the list of pending plug-ins that have been previously discovered and stored in the SettingsManager.
     */
    static List<PendingContextPlugin> getPendingContextPluginsFromDatabase() {
        List<ContextPlugin> installed = getInstalledContextPluginsFromDatabase();
        List<PendingContextPlugin> returnList = new ArrayList<PendingContextPlugin>();
        for (PendingContextPlugin pending : SettingsManager.getPendingContextPlugins()) {
            if (!pending.isUpdate()) {
                boolean found = false;
                for (ContextPlugin installedPlug : installed) {
                    if (installedPlug.equals(pending.getPendingContextPlugin())) {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    returnList.add(pending);
            }
        }
        return returnList;
    }

    /**
     * Returns the list of all Dynamix applications.
     */
    public static List<DynamixApplication> getAllDynamixApplications() {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        List<DynamixApplication> returnList = new ArrayList<DynamixApplication>();
        for (DynamixSession s : SessionManager.getAllSessions()) {
            returnList.add(s.getApp());
        }
        for (DynamixApplication app : SettingsManager.getAllDynamixApplications()) {
            if (!returnList.contains(app))
                returnList.add(app);
        }
        return returnList;
    }

    /**
     * Returns the remotely paired Dynamix application associated with the token (e.g., an http authorization token), or
     * null if no mapping is found.
     */
    public static DynamixApplication getRemotelyPairedDynamixApplication(String token) {
        List<DynamixApplication> apps = DynamixService.getAllDynamixApplications();
        for (DynamixApplication app : apps) {
            if (app.isRemotelyPaired() && app.getRemotePairing().getHttpToken().equalsIgnoreCase(token))
                return app;
        }
        return null;
    }

    /**
     * Returns the application associated with the incoming appId, or null if no application is found.
     */
    public static DynamixApplication getDynamixApplication(String appId) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        for (DynamixApplication app : getAllDynamixApplications())
            if (app.getAppID().equalsIgnoreCase(appId))
                return app;
        return null;
    }

    /**
     * Returns the in-memory application associated with the incoming (potentially) stale application, or null if no
     * application is found.
     */
    public static DynamixApplication refreshDynamixApplication(DynamixApplication staleApp) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        for (DynamixApplication app : getAllDynamixApplications())
            if (app.equals(staleApp))
                return app;
        return null;
    }

    /**
     * Returns a list of all installed plug-ins from the database (as as List of ContextPlugin)
     */
    static List<ContextPlugin> getInstalledContextPluginsFromDatabase() {
        return SettingsManager.getInstalledContextPlugins();
    }

    /**
     * Returns a list of all installed plug-ins from the database (as as List of ContextPluginInformation)
     */
    public static List<ContextPluginInformation> getInstalledContextPluginInformationFromDatabase() {
        List<ContextPluginInformation> list = new ArrayList<ContextPluginInformation>();
        for (ContextPlugin plug : getInstalledContextPluginsFromDatabase())
            list.add(plug.getContextPluginInformation());
        return list;
    }

    /**
     * Returns true if there is a registered host class loader; false otherwise.
     */
    public static boolean hasEmbeddedHostClassLoader() {
        return DynamixService.embeddedHostClassLoader != null;
    }

    /**
     * Returns the hosting client's class loader.
     */
    public static ClassLoader getEmbeddedHostClassLoader() {
        return DynamixService.embeddedHostClassLoader;
    }

    /**
     * Boots Dynamix in Embedded mode.
     *
     * @param context                 The Android context of the embedding application.
     * @param embeddedHostClassLoader The classloader of the embedding application.
     * @param config                  The Dynamix Framework configuration to use.
     * @return True if the boot sequence was started; false otherwise.
     */
    public static synchronized boolean bootEmbedded(Context context, ClassLoader embeddedHostClassLoader,
                                                    FrameworkConfiguration config) {
        if (bootState == BootState.NOT_BOOTED) {
            embeddedMode = true;
            // In embedded mode, the androidContext is the calling context.
            DynamixService.androidContext = context;
            DynamixService.embeddedHostClassLoader = embeddedHostClassLoader;
            DynamixService.config = config;
            DynamixPreferences.setContext(context);
            DynamixService.service = new DynamixService();
            DynamixService.service.onCreate();
            boot(context, false, false, true);
            return true;
        } else {
            Log.w(TAG, "Cannot boot Dynamix from state: " + bootState);
            return false;
        }
    }

    /**
     * Update's the Dynamix FrameworkConfiguration when running in embedded mode.
     *
     * @param config The new configuration to use
     * @return True if the config was updated; false otherwise.
     */
    public static synchronized boolean updateConfig(FrameworkConfiguration config) {
        if (isEmbedded()) {
            if (config != null) {
                DynamixService.config = config;
                return true;
            } else {
                Log.w(TAG, "Config was null");
                return false;
            }
        } else {
            Log.w(TAG, "Dynamix must be running in embedded mode to update the config");
            return false;
        }
    }

    /**
     * Boots the Dynamix Framework, which initializes all managers and data structures and prepares Dynamix for use.
     * This method is asynchronous and returns immediately.
     */
    public static synchronized void boot(Context context, boolean showProgress, boolean bootFromService,
                                         boolean embeddedMode) {
        Log.d(TAG, "boot called with Context " + context + " and embedded mode " + embeddedMode + " and boot state "
                + bootState);
        synchronized (bootState) {
            // Only boot if we're in state NOT_BOOTED
            if (bootState == BootState.NOT_BOOTED) {
                // Set our boot state to booting
                bootState = BootState.BOOTING;
                // Set Util context
                Utils.setContext(context);
                /*
                 * Start the service (if it's not already running). Note that, if Dynamix crashed, Android will re-start
				 * us using 'onCreate', so the service *will* be running in that case.
				 */
                if (!androidServiceRunning) {
                    // Service is not running, so start it
                    if (!embeddedMode) {
                        // Launch the service using the appContext
                        Log.i(TAG, "Starting the Dynamix Service...");
                        Context appContext = context.getApplicationContext();
                        appContext.startService(new Intent(appContext, DynamixService.class));
                    } else {
                        // Manually call onCreate, since Android won't because we're running embedded
                        service.onCreate();
                    }
                }
            } else {
                Log.w(TAG, "boot called when in bootState: " + bootState);
            }
        }
    }

    /**
     * Starts the WebConnector, which allows web clients to access Dynamix services via its REST interface.
     */
    public static boolean startWebConnector(int port, int checkPeriodMills, int timeoutMills,
                                            List<TrustedCert> authorizedCerts) throws IOException {
        if (config.isWebConnectorEnabled()) {
            if (isFrameworkInitialized()) {
                if (!WebConnector.isStarted()) {
                    // generate new public private key pair on every boot of web connector.
                    try {
                        // CryptoUtils.removeRSAKeyPairFromKeystore(DynamixPreferences.DYNAMIX_KEY);
                        // CryptoUtils.generateRsaKeyPairForKeystore(2048,androidContext,DynamixPreferences.DYNAMIX_KEY);
                        WebConnector.setDynamixRSAKeyPair(CryptoUtils.generateRsaKeyPair(2048));
                        // Log.i(TAG, "Dynamix Public Key: " +
                        // CryptoUtils.createStringFromPublicKey(CryptoUtils.getDynamixPublicRsaKeyFromKeystore()));
                    } catch (Exception e) {
                        Log.w(TAG, "Problem generating RSA keys: " + e);
                    }
                    WebConnector.startServer(getAndroidContext(), ContextMgr, port, checkPeriodMills, timeoutMills, authorizedCerts);
                    return true;
                } else {
                    Log.d(TAG, "Web connector already started");
                    return true;
                }
            } else {
                Log.w(TAG, "Cannot start web connector because Dynamix is not initialized");
                return false;
            }
        } else {
            Log.w(TAG, "Cannot start web connector because it's disabled");
            return false;
        }
    }

    /**
     * Starts the WebConnector using the values specified in framework configuration.
     */
    public static boolean startWebConnectorUsingConfigData() {
        for (int port : config.getWebConnectorPorts()) {
            try {
                return startWebConnector(port, config.getWebConnectorTimeoutCheckMills(),
                        config.getWebConnectorClientTimeoutMills(), getAuthorizedCertsFromKeyStore());
            } catch (IOException e) {
                Log.w(TAG, "Could not start web connector using port: " + port);
            }
        }
        Log.w(TAG, "Failed to start web connector using specified ports: " + config.getWebConnectorPorts());
        return false;
    }

    protected static void addContextSupport(DynamixApplication app, IContextHandler handler, IContextListener listener,
                                            String pluginId, String pluginVersion, String contextType, Bundle config, IContextSupportCallback callback)
            throws Exception {
        if (ContextMgr != null) {
            ContextMgr
                    .addContextSupport(app, handler, contextType, pluginId, pluginVersion, listener, config, callback);
        }
    }

    protected static void removeAllContextSupport(DynamixApplication app, Callback callback) {
        if (ContextMgr != null) {
            ContextMgr.removeAllContextSupport(app, callback);
        } else
            SessionManager.sendCallbackFailure(callback, "STATE_ERROR: Context Manager not found",
                    ErrorCodes.STATE_ERROR);
    }

    protected static void removeContextSupport(DynamixApplication app, IContextHandler handler,
                                               ContextSupportInfo supportInfo, Callback callback) {
        if (ContextMgr != null) {
            ContextMgr.removeContextSupport(app, handler, supportInfo, callback);
        } else
            SessionManager.sendCallbackFailure(callback, "STATE_ERROR: Context Manager not found",
                    ErrorCodes.STATE_ERROR);
    }

    protected static void removeContextSupportForContextType(DynamixApplication app, IContextHandler handler,
                                                             String contextType, Callback callback) {
        if (ContextMgr != null) {
            ContextMgr.removeContextSupportForContextType(app, handler, contextType, callback);
        } else
            SessionManager.sendCallbackFailure(callback, "STATE_ERROR: Context Manager not found",
                    ErrorCodes.STATE_ERROR);
    }

    /*
     * TODO !!! NOT SECURE !!! Experimental method to load authorized certificates from the inbuilt Dynamix keystore.
	 * This is not yet secure, since plug-ins may be able to access the "trusted_certs" resource and there is no
	 * password protection on the keystore.
	 */
    private synchronized static List<TrustedCert> getAuthorizedCertsFromKeyStore() {
        List<TrustedCert> authorizedCerts = new ArrayList<TrustedCert>();
        try {
            KeyStore trusted = KeyStore.getInstance("BKS");
            InputStream in = new FileInputStream(keyStorePath);
            trusted.load(in, "".toCharArray());
            Enumeration<String> aliases = trusted.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                X509Certificate cert = (X509Certificate) trusted.getCertificate(alias);
                // Log.i(TAG, "Got Authorized Cert: " + cert);
                authorizedCerts.add(new TrustedCert(alias, cert));
            }
        } catch (KeyStoreException e) {
            Log.w(TAG, e);
        } catch (NotFoundException e) {
            Log.w(TAG, e);
        } catch (NoSuchAlgorithmException e) {
            Log.w(TAG, e);
        } catch (CertificateException e) {
            Log.w(TAG, e);
        } catch (IOException e) {
            Log.w(TAG, e);
        }
        return authorizedCerts;
    }

    /**
     * Stores the certificate to the Dynamix KeyStore. Updates the WebConnector.
     *
     * @param alias The alias to store the cert under.
     * @param cert  The certificate to store.
     * @throws Exception
     */
    public synchronized static void storeAuthorizedCert(String alias, X509Certificate cert) throws Exception {
        Log.i(TAG, "Storing authorized cert for " + alias);
        // Load the KeyStore
        KeyStore trusted = KeyStore.getInstance("BKS");
        // trusted.
        // SSLContext context = SSLContext.getInstance("TLS");
        // context.init(km, tm, sr)
        // context.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), new SecureRandom());
        InputStream in = new FileInputStream(keyStorePath);
        // http://stackoverflow.com/questions/7245007/runtime-configuration-of-ssl-tls-http-client-on-android-with-client-authenticati
        trusted.load(in, "".toCharArray());
        // Set the cert
        trusted.setCertificateEntry(alias, cert);
        // Store the data
        OutputStream keyStoreStream = new java.io.FileOutputStream(keyStorePath);
        trusted.store(keyStoreStream, "".toCharArray());
        /*
         * NOTE: Store example
		 * https://github.com/k9mail/k-9/blob/master/src/com/fsck/k9/mail/store/TrustManagerFactory.java
		 */
    }

    /**
     * Removes the certificate from the Dynamix KeyStore. Updates the WebConnector.
     *
     * @param alias
     * @throws Exception
     */
    protected synchronized static void removeAuthorizedCert(String alias) throws Exception {
        Log.i(TAG, "Removing authorized cert for " + alias);
        // Load the KeyStore
        KeyStore trusted = KeyStore.getInstance("BKS");
        InputStream in = new FileInputStream(keyStorePath);
        trusted.load(in, "".toCharArray());
        // Remember the cert
        X509Certificate cert = (X509Certificate) trusted.getCertificate(alias);
        // Delete the cert
        trusted.deleteEntry(alias);
        // Store the data
        OutputStream keyStoreStream = new java.io.FileOutputStream(keyStorePath);
        trusted.store(keyStoreStream, "".toCharArray());
        // If the WebConnector is started, update it
        // if (cert != null && WebConnector.isStarted())
        // WebConnector.removeAuthorizedCert(new TrustedCert(alias, cert));
        /*
         * NOTE: Store example
		 * https://github.com/k9mail/k-9/blob/master/src/com/fsck/k9/mail/store/TrustManagerFactory.java
		 */
    }

    /**
     * Exports the certificate keystore to the root of the device's SD card. This method is used internally for
     * gathering authorized certificates from browsers.
     *
     * @throws Exception
     */
    protected static void exportKeyStoreToSDCARD() throws Exception {
        File sourceFile = new File(keyStorePath);
        File destFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"
                + sourceFile.getName());
        if (!destFile.exists()) {
            destFile.createNewFile();
        }
        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    /**
     * Stops the WebConnector, which prevents web clients from accessing Dynamix services via its REST interface.
     */
    public static void stopWebConnector() {
        if (isFrameworkInitialized() && WebConnector.isStarted())
            WebConnector.stopServer();
    }

    /**
     * Sets the time period (in milliseconds) between checks for web client timeouts.
     */
    protected static void setWebClientTimeoutCheckPeriod(int checkPeriodMills) {
        WebConnector.setWebClientTimeoutCheckPeriod(checkPeriodMills);
    }

    /**
     * Sets the web client timeout duration (in milliseconds).
     */
    protected static void setWebClientTimeoutMills(int timeoutMills) {
        WebConnector.setWebClientTimeoutMills(timeoutMills);
    }

    /**
     * Cancels a previously started Bundle installation for the specified ContextPlugin.
     *
     * @return True if the installation was cancelled; false otherwise.
     */
    public static boolean cancelInstallation(ContextPlugin plug) {
        return OsgiMgr.cancelInstallation(plug);
    }

    /**
     * Cancels a previously started plug-in installation.
     */
    public static boolean cancelInstallation(ContextPluginInformation plug) {
        return OsgiMgr.cancelInstallation(getContextPlugin(plug));
    }

    /**
     * Updates the enabled status of the application, notifying the application of the change.
     *
     * @param app     The application to enable or disable.
     * @param enabled True to enable a DynamixApplication; false to disable it.
     */
    static void changeApplicationEnabled(DynamixApplication app, boolean enabled) {
        Log.d(TAG, "changeApplicationEnabled for " + app + " enabled == " + enabled);
        // Set the application to the requested enabled state
        app.setEnabled(enabled);
        // Update the application
        if (updateApplication(app)) {
            // If Dynamix is running, notify the application of the change
            DynamixSession session = SessionManager.getSession(app);
            if (session != null) {
                if (enabled) {
                    SessionManager.notifySessionOpened(app, session.getSessionId().toString());
                    // Notify the application about active state
                    if (isFrameworkStarted())
                        SessionManager.notifyAllDynamixFrameworkActive();
                    else
                        SessionManager.notifyAllDynamixFrameworkInactive();
                } else
                    SessionManager.notifySessionClosed(app);
            } else {
                Log.w(TAG, "changeApplicationEnabled could not find session for app: " + app);
            }
        }
    }

    /**
     * Clears the specified ContextPlugin's statistics.
     *
     * @param plug The Plugin to clear.
     * @return True if the stats were cleared; false otherwise.
     */
    public static boolean clearPluginStats(ContextPlugin plug) {
        if (ContextMgr != null) {
            return ContextMgr.clearPluginStats(plug);
        }
        return false;
    }

    /**
     * Removes the handler using the Facade.
     */
    static void removeContextHandler(IContextHandler handler, Callback callback) {
        try {
            facadeBinder.removeContextHandlerWithCallback(handler, callback);
        } catch (RemoteException e) {
            Log.w(TAG, "Could not remove listener: " + e.toString());
        }
    }

    /**
     * Returns the Dynamix Framework VersionInfo
     */
    public static VersionInfo getDynamixFrameworkVersion() {
        return FrameworkConstants.DYNAMIX_VERSION;
    }

    /**
     * Returns the PluginStats for the incoming plugin id.
     */
    public static PluginStats getPluginStats(ContextPlugin plug) {
        if (ContextMgr != null)
            return ContextMgr.getPluginStats(plug);
        else
            return null;
    }

    /**
     * Returns true if the app has an open session; false otherwise.
     */
    public static boolean hasOpenSession(DynamixApplication app) {
        if (isFrameworkStarted()) {
            return SessionManager.hasOpenSession(app);
        } else
            return false;
    }

    /**
     * Runs the DynamixFeature.
     */
    public static void runDynamixFeature(DynamixFeature feature) {
        if (isFrameworkStarted()) {
            if (ContextMgr != null)
                ContextMgr.runDynamixFeature(feature);
        } else
            Log.w(TAG, "Cannot run features when Dynamix isn't started");
    }

    /**
     * Installs the specified context plug-in update, notifying the listener of the progress (if provided).
     *
     * @param update   The UpdateResult to install.
     * @param listener The listener to update with progress reports, or null.
     */
    static void installContextPluginUpdate(PendingContextPlugin update, IPluginInstallCallback listener) {
        Log.i(TAG, "Updating: " + update.getUpdateTargetPlugin());
        // Grab the originalPlug
        ContextPlugin originalPlug = update.getUpdateTargetPlugin();
        // Grab the originalPlug's existing settings (if there are any)
        ContextPluginSettings originalSettings = SettingsManager.getContextPluginSettings(originalPlug);
        /*
         * Update the originalPlug's Bundle. This will fire off a threaded BundleInstaller that will call the
		 * DynamixService back using 'handleBundleUpdated' when the install completes.
		 */
        OsgiMgr.updatePluginBundle(originalPlug, originalSettings, update.getPendingContextPlugin(), listener);
    }

    /**
     * Installs the specified context plug-in updates, notifying the listener of the progress (if provided).
     *
     * @param updates  The Set of UpdateResults to install.
     * @param listener The listener to update with progress reports, or null.
     */
    static void installContextPluginUpdates(Set<PendingContextPlugin> updates, IPluginInstallCallback listener) {
        for (PendingContextPlugin update : updates)
            installContextPluginUpdate(update, listener);
    }

    /**
     * Installs the specified plug-in.
     *
     * @param plug The plug-in to install.
     */
    public static void installPlugin(ContextPluginInformation plug) {
        installPlugin(getContextPlugin(plug), null);
    }

    /**
     * Installs the specified plug-ins.
     *
     * @param plugInfoList The plug-ins to install.
     */
    public static void installPlugins(List<ContextPluginInformation> plugInfoList) {
        installPlugins(plugInfoList, null);
    }

    /**
     * Installs the specified context plug-in, notifying the callback of the progress (if provided).
     *
     * @param plug     The plug-in to install.
     * @param callback The callback to update with progress reports, or null.
     */
    public static void installPlugin(ContextPluginInformation plug, IPluginInstallCallback callback) {
        installPlugin(getContextPlugin(plug), callback);
    }

    /**
     * Installs the specified context plug-ins, notifying the callback of the progress (if provided).
     *
     * @param plugInfoList The plug-ins to install.
     * @param callback     The callback to update with progress reports, or null.
     */
    public static void installPlugins(List<ContextPluginInformation> plugInfoList, IPluginInstallCallback callback) {
        List<ContextPlugin> plugs = new ArrayList<ContextPlugin>();
        for (ContextPluginInformation plugInfo : plugInfoList) {
            ContextPlugin tmp = getContextPlugin(plugInfo);
            if (tmp != null)
                plugs.add(tmp);
            else
                Log.w(TAG, "Could not find plug-in for " + plugInfo);
        }
        doInstallPlugins(plugs, callback);
    }

    /**
     * Registers the FirewallReceiver to receive intents of type ACTION_FIREWALL, and starts firewall request handling.
     */
    private void registerFirewallReceiver() {
        getAndroidContext().registerReceiver(fwReceiver, new IntentFilter(FrameworkConstants.ACTION_FIREWALL));
        startFirewallRequestHandling();
    }

    /**
     * Starts a background thread that monitors the firewallBindingQueue for new context firewall requests to process.
     * The queue is processed continually while Dynamix is started and incoming requests will be routed to the firewall
     * popup for user interaction.
     */
    static synchronized void startFirewallRequestHandling() {
        if (firewallRequestMonitor == null && isFrameworkStarted()) {
            firewallRequestMonitor = new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "startFirewallRequestHandling started!");
                    while (isFrameworkStarted()) {
                        // Make sure we're not processing a request
                        if (currentFirewallBinding == null) {
                            synchronized (firewallBindingQueue) {
                                // Check for request
                                if (firewallBindingQueue.peek() != null) {
                                    // Check if we can pop the firewall
                                    if (!PopupActivity.isActive() && !PopupActivity.isProcessingRequest()) {
                                        // Setup the currentFirewallBinding
                                        currentFirewallBinding = firewallBindingQueue.poll();
                                        // Create the firewall request
                                        FirewallRequest request = new FirewallRequest(currentFirewallBinding.getApp(),
                                                new ArrayList<FirewallRule>(currentFirewallBinding.createRuleSet()));
                                        // Set the request request's id to the current binding
                                        currentFirewallBinding.setRequestId(request.getRequestId());
                                        // Send the request to the popup
                                        PopupActivity.setFirewallRequest(request);
                                        Log.d(TAG, "Launching firewall popup");
                                        // Launch the popup activity
                                        Intent pi = new Intent(DynamixService.getAndroidContext(), PopupActivity.class);
                                        pi.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        DynamixService.getAndroidContext().startActivity(pi);
                                    }
                                }
                            }
                        }
                        try {
                            Thread.sleep(250);
                        } catch (InterruptedException e) {
                        }
                    }
                    firewallRequestMonitor = null;
                    Log.d(TAG, "startFirewallRequestHandling stopped!");
                }
            });
            firewallRequestMonitor.setDaemon(true);
            firewallRequestMonitor.start();
        } else {
            Log.d(TAG, "startFirewallRequestHandling cannot start: frameworkstarted: " + isFrameworkStarted());
        }
    }

    /**
     * Creates a firewall request using the incoming PendingContextSupportRequest and associated firewall rules. The
     * request is routed to either the firewallBindingQueue or the currentFirewallBinding, depending on the status of
     * the firewall (i.g., if it's bound to a given application). Requests are automatically created as new or appended
     * to an existing request if one exists. This is due to the fact that apps provide multiple context support requests
     * within a few milliseconds, which should be aggregated into a single firewall popup that the user interacts with.
     * Multiple requests for the same context type and associated plug-ins are only shown once to the user to make
     * firewall interaction simpler.
     */
    static void addContextFirewallRequest(PendingContextSupportRequest pending, List<FirewallRule> firewallRules) {
        if (!isFrameworkInitialized())
            throw new RuntimeException("Not initialized!");
        /*
         * Before adding to firewall, make sure all incoming rules are set to the default of once (i.e.,
		 * ALLOWED_SESSION)
		 */
        for (FirewallRule r : firewallRules) {
            r.setAccess(FirewallAccess.ALLOWED_SESSION);
        }
        Log.d(TAG, "addContextFirewallRequest for " + pending + " with rules " + firewallRules);
        // Log.i(TAG, "launchFirewallPopup for " + pending.getApp() + " for context type " + pending.getContextType() +
        // " with binding " + currentFirewallBinding);
        synchronized (firewallBindingQueue) {
			/*
			 * Sync on the currentFirewallBinding (if present), just in case onReceive above is processing results.
			 */
            if (currentFirewallBinding != null) {
                synchronized (currentFirewallBinding) {
                }
            }
            // Check if current binding can handle the request
            if (currentFirewallBinding != null && currentFirewallBinding.getApp().equals(pending.getApp())) {
                // Log.i(TAG, "launchFirewallPopup is updating a running FirewallBinding for " + pending.getApp()
                // + " for context type " + pending.getContextType() + " with binding " + currentFirewallBinding);
				/*
				 * Check if the current binding already knows about the incoming rules. If so, it means that they were
				 * already sent to the firewall.
				 */
                boolean processingRules = currentFirewallBinding.hasFirewallRules(firewallRules);
                // Persist the pending request and the associated rules
                currentFirewallBinding.addPendingRequest(pending, firewallRules);
                // Handle popup
                if (!processingRules) {
                    PopupActivity.updateFirewallRequest(firewallRules);
                }
            } else {
                // Check for an existing binding in the queue
                boolean found = false;
                for (FirewallBinding binding : firewallBindingQueue) {
                    if (pending.getApp().equals(binding.getApp())) {
                        // Log.i(TAG, "launchFirewallPopup is updating a waiting FirewallBinding for " +
                        // pending.getApp()
                        // + " for context type " + pending.getContextType() + " with binding "
                        // + currentFirewallBinding);
                        // Persist the pending request and the associated rules
                        binding.addPendingRequest(pending, firewallRules);
                        found = true;
                        break;
                    }
                }
                // If no binding is found, create one
                if (!found) {
                    // Log.i(TAG, "launchFirewallPopup is creating a new FirewallBinding for " + pending.getApp()
                    // + " for context type " + pending.getContextType() + " with binding "
                    // + currentFirewallBinding);
                    firewallBindingQueue.add(new FirewallBinding(pending.getApp(), pending, firewallRules));
                }
            }
        }
    }

    /**
     * A BroadcastReceiver that is used to received broadcasts of type FIREWALL_INTERACTION, which are returned from the
     * context firewall popup after processing a firewall request. The incoming firewall rules are applied to the
     * currentFirewallBinding's context support requests.
     *
     * @author Darren Carlson
     */
    static class FirewallReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Grab the response
            FirewallRequest response = intent.getExtras().getParcelable(FrameworkConstants.FIREWALL_INTERACTION);
            Log.d(TAG, "FirewallReceiver onReceive: " + response);
            if (response != null) {
                // Make sure we've got a currentFirewallBinding
                if (currentFirewallBinding != null) {
                    synchronized (currentFirewallBinding) {
						/*
						 * Make sure the currentFirewallBinding's request id matches the response's id
						 */
                        if (currentFirewallBinding.getRequestId().equals(response.getRequestId())) {
							/*
							 * Handle cancelled
							 */
                            if (response.isCancelled()) {
                                // Send REQUEST_CANCELLED to all pending requests
                                for (PendingContextSupportRequest r : currentFirewallBinding
                                        .getPendingContextSupportRequests())
                                    SessionManager.sendContextSupportFailure(r.getCallback(),
                                            "Add Context Support CANCELLED By User", ErrorCodes.REQUEST_CANCELLED);
                            } else {
								/*
								 * Process each pending support request in the currentFirewallBinding.
								 */
                                for (PendingContextSupportRequest pendingContextSupport : currentFirewallBinding
                                        .getPendingContextSupportRequests()) {
									/*
									 * Check if any of the incoming rules allow the pending request.
									 */
                                    FirewallRule pendingRule = response.getFirewallRule(
                                            pendingContextSupport.getContextType(),
                                            pendingContextSupport.getAllRequestedPlugins());
									/*
									 * At this point, the pending rule could have been an app rule
									 */
                                    if (pendingRule != null) {
                                        // We had a rule match; handle access levels
                                        if (pendingRule.isAccessGranted()) {
                                            Log.d(TAG, "Firewall rule granted: " + pendingRule);
                                            Log.d(TAG,
                                                    "Firewall rule applied to: "
                                                            + pendingContextSupport.getAllRequestedPluginIds());
                                            // Persist ALLOWED and ALLOWED_SESSION access
                                            pendingContextSupport.getApp().addFirewallRule(pendingRule);
                                            pendingContextSupport.getHandler().doAddContextSupport(
                                                    pendingContextSupport.getListener(),
                                                    pendingContextSupport.getAllRequestedPluginIds(),
                                                    pendingContextSupport.getContextType(),
                                                    pendingContextSupport.isForcePluginInstall(),
                                                    pendingContextSupport.getConfig(),
                                                    pendingContextSupport.getCallback());
                                        } else {
                                            if (pendingRule.getFirewallAccess() == FirewallAccess.BLOCKED) {
                                                // Persist BLOCKED access
                                                pendingContextSupport.getApp().addFirewallRule(pendingRule);
                                                Log.w(TAG, "User BLOCKED access to " + pendingRule);
                                            } else {
                                                // Don't persist DENIED access
                                                Log.w(TAG, "User DENIED access to " + pendingRule);
                                                pendingContextSupport.getApp().removeFirewallRule(pendingRule);
                                            }
                                            // Send NOT_AUTHORIZED the pending request
                                            SessionManager.sendContextSupportFailure(
                                                    pendingContextSupport.getCallback(),
                                                    "Add Context Support not allowed By User: "
                                                            + pendingContextSupport.getContextType(),
                                                    ErrorCodes.NOT_AUTHORIZED);
                                        }
                                    } else {
                                        Log.w(TAG, "Could not find firewall rule in response for: "
                                                + pendingContextSupport);
                                        // Send NOT_AUTHORIZED the pending request
                                        SessionManager.sendContextSupportFailure(
                                                pendingContextSupport.getCallback(),
                                                "Add Context Support not allowed By User: "
                                                        + pendingContextSupport.getContextType(),
                                                ErrorCodes.NOT_AUTHORIZED);
                                    }
                                }
                            }
                        } else {
                            Log.w(TAG, "FirewallReceiver onReceive: Unknown currentFirewallRequest: " + response);
                        }
                        // Null out the binding
                        currentFirewallBinding = null;
                    }
                } else {
                    Log.w(TAG, "FirewallReceiver onReceive: No currentFirewallRequest registered to handle response!");
                }
            } else {
                Log.w(TAG, "FirewallReceiver received null response");
            }
        }
    }

    /**
     * Installs a new plug-in.
     *
     * @param plug The ContextPlugin to install.
     */
    static void installPlugin(ContextPlugin plug) {
        installPlugin(plug, null);
    }

    /**
     * Installs a new plug-in.
     *
     * @param plug     The ContextPlugin to install.
     * @param callback The callback to update with progress reports, or null.
     */
    static void installPlugin(ContextPlugin plug, final IPluginInstallCallback callback) {
        final List<ContextPlugin> plugs = new ArrayList<ContextPlugin>();
        plugs.add(plug);
        doInstallPlugins(plugs, callback);
    }

    /**
     * Installs a List of new plug-ins.
     *
     * @param plugs    The list of ContextPlugins to install.
     * @param callback The callback to update with progress reports, or null.
     */
    synchronized static void doInstallPlugins(final List<ContextPlugin> plugs, final IPluginInstallCallback callback) {
		/*
		 * For the moment, we're not refreshing plug-ins before install since we need a better way to handle this
		 * without blocking
		 */
        // Log.d(TAG, "Refreshing context plug-ins...");
        // Refresh plug-in list in blocking-mode
        // DynamixService.checkForContextPluginUpdates(false)
        // Log.d(TAG, "Refreshing context plug-ins... DONE!");
        for (final ContextPlugin plug : plugs) {
            Log.d(TAG, "Dynamix Service is checking install state for " + plug);
            if (plug.isInstalled()) {
                Log.w(TAG, "installPlugins called for previously installed plug-in: " + plug);
                SessionManager.sendPluginInstallProgress(callback, plug.getContextPluginInformation(), 100);
                SessionManager.sendPluginInstallComplete(callback, plug.getContextPluginInformation());
            } else {
                // Check if this a network-based plug-in
                if ((plug.getRepoSource().isNetworkSource() || plug.hasRemoteDependencies())
                        && Utils.isWaitingForWifi()) {
                    // Can't access network... send error
                    if (plug.getRepoSource().isNetworkSource()) {
                        // Not allowed since the plug-in is remote
                        SessionManager.sendPluginInstallFailed(callback, plug.getContextPluginInformation(),
                                "Waiting for WIFI (plug-in is remote)", ErrorCodes.CANNOT_ACCESS_NETWORK);
                    } else {
                        // Not allowed since a dependency is remote
                        SessionManager.sendPluginInstallFailed(callback, plug.getContextPluginInformation(),
                                "Waiting for WIFI (a dependency is remote)", ErrorCodes.CANNOT_ACCESS_NETWORK);
                    }
                } else {
                    // Allowed to install, so handle states
                    switch (plug.getInstallStatus()) {
                        case INSTALLED:
                            Log.w(TAG, "installPlugins called for previously installed plug-in: " + plug);
                            SessionManager.sendPluginInstallProgress(callback, plug.getContextPluginInformation(), 100);
                            SessionManager.sendPluginInstallComplete(callback, plug.getContextPluginInformation());
                            break;
                        case INSTALLING:
                        case PENDING_INSTALL:
                        case WAITING_FOR_DEPENDENCY:
                            Log.d(TAG, "Plug-in already installing: " + plug);
                            break;
                        case NOT_INSTALLED:
                            Log.d(TAG, "doInstallPlugins is installing plug-in: " + plug);
                            plug.setInstallStatus(PluginInstallStatus.PENDING_INSTALL);
                            // Use the OsgiMgr to install the plugin's OSGi Bundle
                            if (OsgiMgr.installBundle(plug, callback)) {
                                // Add the ContextPlugin to the ContextManager along with its runtime factory
                                final ContextPluginSettings settings = SettingsManager.getContextPluginSettings(plug);
                                Utils.dispatch(true, new Runnable() {
                                    @Override
                                    public void run() {
                                        ContextMgr.initializeContextPlugin(plug,
                                                OsgiMgr.getContextPluginRuntimeFactory(plug), settings, new Callback() {
                                                    @Override
                                                    public void onSuccess() throws RemoteException {
                                                        SessionManager.sendPluginInstallComplete(callback,
                                                                plug.getContextPluginInformation());
                                                    }

                                                    @Override
                                                    public void onFailure(String message, int errorCode)
                                                            throws RemoteException {
                                                        SessionManager.sendPluginInstallFailed(callback,
                                                                plug.getContextPluginInformation(),
                                                                "Plugin failed to init after install: " + message,
                                                                errorCode);
                                                    }
                                                });
                                    }
                                });
                            } else {
							/*
							 * The Bundle was not yet available in the OSGi manager, so add the ContextPlugin to the
							 * ContextManager without its runtime factory. The runtime will be added to the context
							 * manager once the bundle is installed - see 'handleBundleInstalled'
							 */
                                ContextMgr.addNewContextPlugin(plug);
                            }
                            break;
                        default:
						/*
						 * Handles states: ERROR, INSTALLED, NETWORK_PROBLEM, UNINSTALLING
						 */
                            Log.w(TAG, "Plug-in CANNOT be installed from state (" + plug.getInstallStatus() + "): " + plug);
                            SessionManager.sendPluginInstallFailed(callback, plug.getContextPluginInformation(),
                                    "Plug-in CANNOT be installed from state (" + plug.getInstallStatus() + "): " + plug,
                                    ErrorCodes.STATE_ERROR);
                            break;
                    }
                }
            }
        }
    }

    /**
     * Reinitializes the plug-in by calling ContextMgr.initializeContextPlugin. This method should only be called for
     * plug-ins that have already been installed.
     */
    synchronized static void reInitializePlugin(ContextPlugin plug) {
        // Call reInitializePlugin without a callback
        reInitializePlugin(plug, null);
    }

    /**
     * Reinitializes the plug-in by calling ContextMgr.initializeContextPlugin. This method should only be called for
     * plug-ins that have already been installed.
     */
    synchronized static void reInitializePlugin(ContextPlugin plug, Callback callback) {
        if (OsgiMgr.isBundleInstalled(plug)) {
            if (DynamixService.isFrameworkStarted()) {
                // Access the plug-ins settings
                ContextPluginSettings settings = SettingsManager.getContextPluginSettings(plug);
                // Use the context manager to init the plug-in
                ContextMgr.initializeContextPlugin(plug, OsgiMgr.getContextPluginRuntimeFactory(plug), settings,
                        callback);
            }
        } else {
            Log.w(TAG, "reInitializePlugin called on plug-in that did not have it's Bundle installed: " + plug);
            SessionManager.sendCallbackFailure(callback,
                    "reInitializePlugin called on plug-in that did not have it's Bundle installed: " + plug,
                    ErrorCodes.STATE_ERROR);
        }
    }

    /**
     * Returns true if the Dynamix Framework is initialized; false otherwise.
     */
    public static boolean isFrameworkInitialized() {
        synchronized (bootState) {
            return bootState == BootState.BOOTED;
        }
    }

    /**
     * Returns true if the Dynamix Framework is initialized; false otherwise.
     */
    public static boolean isFrameworkStarted() {
        synchronized (startState) {
            return startState == StartState.STARTED;
        }
    }

    /**
     * Enables the plug-in described by the ContextPluginInformation.
     *
     * @param plugInfo The ContextPluginInformation for the plug-in to enable.
     * @return True if the request was accepted; false otherwise.
     */
    public static boolean enableContextPlugin(ContextPluginInformation plugInfo) {
        final ContextPlugin plug = getInstalledContextPlugin(plugInfo);
        Log.d(TAG, "enableContextPlugin " + plug);
        if (plug != null) {
            plug.setEnabled(true);
            reInitializePlugin(plug, new Callback() {
                @Override
                public void onSuccess() throws RemoteException {
                    updateContextPluginValues(plug, true);
                }

                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    Log.w(TAG, "enableContextPlugin encountered an error while re-initializing plugin: " + plug);
                }
            });
            return true;
        } else {
            Log.w(TAG, "Could not find plug-in for: " + plugInfo);
            return false;
        }
    }

    /**
     * Disables the plug-in described by the ContextPluginInformation.
     *
     * @param plugInfo The ContextPluginInformation for the plug-in to disable.
     * @return True if the request was accepted; false otherwise.
     */
    public static boolean disableContextPlugin(ContextPluginInformation plugInfo) {
        ContextPlugin plug = getInstalledContextPlugin(plugInfo);
        Log.d(TAG, "disableContextPlugin for " + plug);
        if (plug != null) {
            plug.setEnabled(false);
            updateContextPluginValues(plug, true);
            return true;
        } else {
            Log.w(TAG, "Could not find plug-in for: " + plugInfo);
            return false;
        }
    }

    /**
     * Indicates that the Dynamix plugin status update process has failed.
     *
     * @param message A message describing the failure reasons
     */
    static void onPluginStatusUpdateFailure(String message) {
        Log.w(TAG, "onPluginStatusUpdateFailure: " + message);
        // TODO: Handle event?
    }

    /**
     * Removes the Dynamix application, closing its session and removing it from the settings database.
     */
    public static void removeDynamixApplication(final DynamixApplication app) {
        removeDynamixApplication(app, null);
    }

    /**
     * Removes the Dynamix application, closing its session (if necessary) and removing it from the settings database.
     */
    public static void removeDynamixApplication(final DynamixApplication app, final Callback callback) {
        // First, remove the app from the settings
        if (removeApplicationFromSettings(app)){
            // Next, close any open sessions
            if (SessionManager.hasOpenSession(app)) {
                closeAppSession(app, true, new Callback() {
                    @Override
                    public void onSuccess() throws RemoteException {
                        SessionManager.sendCallbackSuccess(callback);
                    }

                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        Log.w(TAG, "Could not remove Dynamix applicaiton: " + message);
                        // SessionManager.sendCallbackFailure(callback, message, errorCode);
                        SessionManager.sendCallbackSuccess(callback);
                    }
                });
            } else {
                SessionManager.sendCallbackSuccess(callback);
            }
        }
        else{
            Log.w(TAG, "Could not find or remove Dynamix application from settings: " + app);
            SessionManager.sendCallbackFailure(callback,
                    "Could not find or remove Dynamix application from settings: " + app,
                    ErrorCodes.NOT_FOUND);
        }

    }

    /**
     * Removed the application from the settings manager.
     */
    static boolean removeApplicationFromSettings(DynamixApplication app) {
        if (app != null) {
            Log.d(TAG, "removeApplicationFromSettings: " + app);
            if (SettingsManager.removeDynamixApplication(app)) {
                onDynamixApplicationRemoved(app);
                return true;
            } else {
                return false;
            }
        } else {
            Log.w(TAG, "app was null in removeApplicationFromSettings");
            return false;
        }
    }

    /**
     * Closes the app's session without a callback.
     */
    public static void closeAppSession(final DynamixApplication app, final boolean notify) {
        closeAppSession(app, notify, null);
    }

    /**
     * Closes the app's session with a callback.
     */
    public static void closeAppSession(final DynamixApplication app, final boolean notify, final Callback cb) {
        if (app != null) {
            Log.d(TAG, "closeAppSession for: " + app);
			/*
			 * Remove context support using the ContextManager.
			 */
            ContextMgr.removeAllContextSupport(app, new Callback() {
                @Override
                public void onSuccess() throws RemoteException {
                    SessionManager.closeSession(app, notify, cb);
                }

                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    Log.w(TAG, "removeAllContextSupport.onFailure: " + message);
                    SessionManager.closeSession(app, notify, cb);
                }
            });
        } else {
            Log.w(TAG, "app was null in closeAppSession");
        }
    }

    /**
     * Revokes the application's security authorization, removing all context support, closing its session, and removing
     * its entry from the SettingsManager.
     */
    static Result removeApplication(DynamixApplication app) {
        if (app != null) {
            Log.d(TAG, "removeApplication: " + app);
            if (SettingsManager.removeDynamixApplication(app)) {
                SessionManager.closeSession(app, true, null);
                return new Result();
            } else {
                Log.w(TAG, "removeApplication could not find app in SettingsManager: " + app);
                return new Result("revokeSecurityAuthorization could not find app in SettingsManager: " + app,
                        ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
            }
        } else {
            Log.w(TAG, "app was null in removeApplication");
            return new Result("app was null in removeApplication", ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
        }
    }

    /**
     * Dispatches the incoming NFC Intent to registered plug-ins.
     *
     * @param i The NFC Intent.
     */
    static void dispatchNfcEvent(final Intent i) {
        if (ContextMgr != null) {
            // Maintain for backwards compatibility
            ContextMgr.dispatchNfcEvent(i);
            // New dispatch method
            ContextMgr.dispatchDynamixEvent(i);
        }
    }

    /**
     * Dispatches the incoming Intent to registered plug-ins.
     *
     * @param event The event.
     */
    public static void dispatchDynamixEvent(final Intent event) {
        if (ContextMgr != null) {
            ContextMgr.dispatchDynamixEvent(event);
        }
    }

    /**
     * Sets the new PowerScheme.
     */
    public static void setNewPowerScheme(PowerScheme scheme) {
        if (bootState == BootState.BOOTED) {
            SettingsManager.setPowerScheme(scheme);
            ContextMgr.setPowerScheme(scheme);
        } else
            Log.w(TAG, "Dynamix has not booted yet... please wait!");
    }

    /**
     * Starts the Dynamix Framework if it's already initialized and Dynamix is enabled; otherwise, it caches the start
     * request so that Dynamix will start after the boot process completes. If this method is called before Dynamix is
     * initialized, the start request is cached and handled after initialization.
     */
    public static boolean startFramework() {
        Log.i(TAG, "startFramework");
        // Make sure we're initialized
        if (isFrameworkInitialized()) {
            // Make sure we're stopped
            if (!isFrameworkStarted()) {
                service.doStartFramework();
                return true;
            } else
                Log.w(TAG, "Cannot start framework while in state: " + startState);
        } else {
            // Cache the boot request
            Log.i(TAG, "startFramework called while Dynamix is not yet initialized... start request cached");
            startRequested = true;
            return true;
        }
        return false;
    }

    /**
     * Stops the Dynamix Framework.
     */
    public static boolean stopFramework() {
        // Set all disabled in the preferences
        DynamixPreferences.setDynamixEnabledState(false);
        // Make sure we're initialized
        if (isFrameworkInitialized())
            // Make sure we're started
            if (isFrameworkStarted()) {
                // Stop the framework
                service.doStopFramework(false, false, false);
                return true;
            } else
                Log.w(TAG, "Cannot stop framework while in state: " + startState);
        else
            Log.w(TAG, "DynamixService not initialized!");
        return false;
    }

    /**
     * Destroys the Dynamix Framework, killing the service (if necessary) and removing all listeners.
     */
    public static boolean destroyFramework(boolean killProcess, boolean restartProcess) {
        // Make sure we're initialized
        if (isFrameworkInitialized()) {
            // Destroy the framework
            service.doStopFramework(true, killProcess, restartProcess);
            return true;
        } else {
            if (killProcess)
                killDynamixProcess(restartProcess);
            else
                Log.w(TAG, "DynamixService not initialized!");
        }
        return false;
    }

    /**
     * Unceremoniously uninstalls all plug-ins in a radical maneuver that is simultaneously ridiculous and awe
     * inspiring.
     */
    public static void uninstallAllPlugins() {
        for (ContextPlugin plug : getInstalledContextPluginsFromDatabase())
            uninstallPlugin(plug);
    }

    public static void uninstallPlugin(ContextPluginInformation plug) {
        uninstallPlugin(getInstalledContextPlugin(plug), null, null);
    }

    public static void uninstallPlugins(List<ContextPluginInformation> plugs) {
        for (ContextPluginInformation plug : plugs)
            uninstallPlugin(getInstalledContextPlugin(plug), null, null);
    }

    public static void uninstallPlugin(ContextPluginInformation plug, Callback uninstallCallback) {
        uninstallPlugin(getInstalledContextPlugin(plug), null, uninstallCallback);
    }

    public static void uninstallPlugin(ContextPluginInformation plug, Callback stopCallback, Callback uninstallCallback) {
        uninstallPlugin(getInstalledContextPlugin(plug), stopCallback, uninstallCallback);
    }

    /**
     * Uninstalls the specified ContextPlugin, removing its Dynamix settings and underlying OSGi Bundle. If Dynamix is
     * running, the ContextPlugin is immediately stopped before the uninstall continues. Other ContextPlugins continue
     * to operate normally during this process.
     *
     * @param plug The plug-in to uninstall.
     */
    static void uninstallPlugin(ContextPlugin plug) {
        uninstallPlugin(plug, null, null);
    }

    /**
     * Uninstalls the specified ContextPlugin, removing its Dynamix settings and underlying OSGi Bundle. If Dynamix is
     * running, the ContextPlugin is immediately stopped before the uninstall continues. Other ContextPlugins continue
     * to operate normally during this process.
     *
     * @param plug              The plug-in to uninstall.
     * @param stopCallback      An optional callback to notify when the plug-in stops.
     * @param uninstallCallback An optional callback to notify when the plug-in is uninstalled.
     * @return
     */
    static void uninstallPlugin(ContextPlugin plug, ICallback stopCallback, ICallback uninstallCallback) {
        Log.i(TAG, "Uninstalling " + plug);
		/*
		 * Remove the plugin from the Context Manager, which remove its context support registrations and destroys the
		 * plugin. This method calls 'removeUninstalledContextPlugin' when complete.
		 */
        ContextMgr.removeContextPlugin(plug, stopCallback, uninstallCallback);
    }

    /**
     * Removes the plug-ins OSGi Bundle and removes it from the list of installed context plug-ins in the settings
     * manager.
     */
    static void removeUninstalledContextPlugin(ContextPlugin plug, ICallback callback) {
        List<ICallback> callbacks = new ArrayList<ICallback>();
        callbacks.add(callback);
        removeUninstalledContextPlugin(plug, callbacks);
    }

    /**
     * Removes the plug-ins OSGi Bundle and removes it from the list of installed context plug-ins in the settings
     * manager.
     */
    static synchronized void removeUninstalledContextPlugin(ContextPlugin plug, List<ICallback> callbacks) {
        Log.d(TAG, "Remove " + plug + ", which is at state " + plug.getInstallStatus());
        // Remove the plug-in from the settings manager
        SettingsManager.removeInstalledContextPlugin(plug);
        // Grab the plug-in's wrapper and set DESTROYED state
        ContextPluginRuntimeWrapper wrapper = ContextMgr.getContextPluginRuntimeWrapper(plug);
        if (wrapper != null && wrapper.getState() != PluginState.DESTROYED)
            Log.w(TAG, "Uninstalling NON-DESTROYED plug-in with state " + wrapper.getState() + "  " + plug);
        // Remove the plug-in's OSGi Bundle
        OsgiMgr.uninstallBundle(plug);
        // Disable the plug-in
        plug.setEnabled(false);
        // If the plug-in is also an app, remove it (if requested).
        DynamixApplication pluginApp = DynamixService.getDynamixApplication(Utils.makeAppIdForPlugin(plug));
        if (pluginApp != null && DynamixPreferences.autoAppUninstall()) {
            DynamixService.removeApplication(pluginApp);
        }
        // Set not installed status
        plug.setInstallStatus(PluginInstallStatus.NOT_INSTALLED);
        // Notify any callbacks
        if (callbacks != null)
            for (ICallback cb : callbacks) {
                SessionManager.sendCallbackSuccess(cb);
            }
        // Also inform all listeners that the plugin was uninstalled
        SessionManager.notifyAllContextPluginUninstalled(plug);
    }

    /**
     * Updates the specified ContextPlugin with the incoming ContextPlugin. If handleStateChanges is true, this method
     * automatically starts or stops the plug-in; if not, this method simply updates the SettingsManager only and will
     * not start or stop the plug-in. Returns true if the ContextPlugin was updated; false otherwise.
     */
    static boolean updateContextPluginValues(ContextPlugin plug, boolean handleStateChanges) {
        if (SettingsManager.updateContextPlugin(plug)) {
            // If we're handling state changes, and the plug-in is enabled, start it; otherwise stop it
            if (handleStateChanges) {
                if (plug.isEnabled()) {
                    ContextMgr.startPlugin(plug, false);
                } else {
                    ContextMgr.stopPlugin(plug, true, true, false);
                }
            }
            // Notify listeners about the state change.
            SessionManager.notifyAllContextPluginEnabledStateChanged(plug);
            return true;
        } else
            Log.d(TAG, "Could not update values for " + plug);
        return false;
    }

    /**
     * Refreshes the list of plug-ins known by Dynamix using the configured repositories. Used internally by Dynamix for
     * automatic plug-in discovery. Can also be called by clients that embed Dynamix to trigger context plug-in
     * discovery.
     */
    static public boolean checkForUpdates(boolean runAsynchronously) {
        if (isFrameworkInitialized()) {
            // Do Dynamix Framework Update - this currently triggers a plug-in update when finished
            UpdateManager.refreshDynamixFrameworkUpdates(getAndroidContext(), DynamixService.getConfig()
                    .getPrimaryDynamixServer().getUrl(), new DynamixUpdatesCallbackHandler(), true);
            return true;
        } else
            Log.w(TAG, "Can't check for context plug-in updates, since Dynamix has not booted");
        return false;
    }
    /**
     * Asynchronously checks for context plugin UPDATES using the UpdateManager, notifying the specified
     * IUpdateStatusListener with results (or errors). Typically used by Dynamix UI for displaying results.
     *
     * @param handler
     *            The IUpdateStatusListener to notify with results (or errors).
     */
    // static void checkForUpdates(IContextPluginUpdateListener callback, boolean runAsynchronously) {
    // if (isFrameworkInitialized()) {
    // // Do Dynamix Framework Update
    // UpdateManager.refreshDynamixFrameworkUpdates(getAndroidContext(), DynamixService.getConfig()
    // .getPrimaryDynamixServer().getUrl(), new DynamixUpdatesCallbackHandler(), true);
    // // Refresh plug-ins from repos
    // UpdateManager.refreshContextPluginsFromRepos(getAndroidContext(), getContextPluginRepos(),
    // PluginConstants.PLATFORM.ANDROID, Utils.getAndroidVersionInfo(), getDynamixFrameworkVersion(),
    // new ContextPluginCallbackHandler(callback), androidContext.getPackageManager()
    // .getSystemAvailableFeatures(), runAsynchronously);
    // }
    // }

    /**
     * Returns the last time the repo was modified.
     */
    static Date getRepoLastModified(String repoId) {
        return SettingsManager.getRepoLastModified(repoId);
    }

    /**
     * Sets the last time the repo was modified.
     */
    static void setRepoLastModified(String repoId, Date lastModified) {
        SettingsManager.setRepoLastModified(repoId, lastModified);
    }

    /**
     * Returns the ContextPlugin associated with the incoming ContextPluginInformation using the SettingsManager.
     */
    static ContextPlugin getInstalledContextPlugin(ContextPluginInformation plugInfo) {
        List<ContextPlugin> plugs = getInstalledContextPlugins();
        for (ContextPlugin plug : plugs)
            if (plug.getContextPluginInformation().equals(plugInfo))
                return plug;
        return null;
    }

    /**
     * Returns the list of plug-ins currently managed by the ContextManager. Note that these plug-ins
     * may be in states other than installed (e.g., installing).
     */
    static List<ContextPlugin> getManagedContextPlugins() {
        return ContextMgr.getAllContextPlugins();
    }

    /**
     * Returns a List of all installed plug-ins as a list of ContextPlugin.
     */
    static List<ContextPlugin> getInstalledContextPlugins() {
        List<ContextPlugin> plugList = new ArrayList<ContextPlugin>();
//        // Access the ContextManager plug-ins, which have current install state (e.g., installing)
//        for (ContextPlugin plug : ContextMgr.getAllContextPlugins()) {
//            plugList.add(plug);
//        }
        // Add any other plug-ins known to the SettingsManager, deferring to existing state
//        for (ContextPlugin settingsPlug : getInstalledContextPluginsFromDatabase()) {
//            if (!plugList.contains(settingsPlug))
//                plugList.add(settingsPlug);
//        }
//        return plugList;
        // Defer to plug-ins from the database
        return getInstalledContextPluginsFromDatabase();
    }

    /**
     * Returns a List of all managed plug-ins as a list of ContextPluginInformation.
     */
    public static List<ContextPluginInformation> getInstalledContextPluginInformation() {
        List<ContextPluginInformation> plugList = new ArrayList<ContextPluginInformation>();
        for (ContextPlugin plug : getInstalledContextPlugins()) {
            plugList.add(plug.getContextPluginInformation());
        }
        return plugList;
    }

    /**
     * Returns a List of all pending ContextPlugins (i.e., available but not yet installed plug-ins).
     */
    public static List<ContextPluginInformation> getPendingContextPluginInfo() {
        List<ContextPluginInformation> plugInfoList = new ArrayList<ContextPluginInformation>();
        List<PendingContextPlugin> pendingPlugins = UpdateManager.getPendingContextPlugins();
        for (PendingContextPlugin pending : pendingPlugins)
            plugInfoList.add(pending.getPendingContextPlugin().getContextPluginInformation());
        return plugInfoList;
    }

    /**
     * Returns a List of all pending ContextPlugins (i.e., available but not yet installed plug-ins).
     */
    static List<ContextPlugin> getPendingContextPluginList() {
        List<ContextPlugin> plugList = new ArrayList<ContextPlugin>();
        List<PendingContextPlugin> discoveredPendingPlugs = UpdateManager.getPendingContextPlugins();
        for (PendingContextPlugin discovered : discoveredPendingPlugs)
            plugList.add(discovered.getPendingContextPlugin());
        return plugList;
    }

    /**
     * Returns the latest version of ContextPluginInformation for the specified pluginId String, or null if the
     * information is not found. This method searches ALL known plug-ins whether installed or not.
     */
    public static ContextPluginInformation getContextPluginInfo(String pluginId) {
        ContextPluginInformation latest = null;
        for (ContextPluginInformation info : getAllContextPluginInfo()) {
            if (info.getPluginId().equalsIgnoreCase(pluginId)) {
                if (latest != null) {
                    if (info.getVersion().compareTo(latest.getVersion()) > 1)
                        latest = info;
                } else {
                    latest = info;
                }
                return info;
            }
        }
        return latest;
    }

    /**
     * Returns the ContextPluginInformation for the specified pluginId and version, or null if the information is not
     * found. This method searches ALL known plug-ins whether installed or not.
     */
    public static ContextPluginInformation getContextPluginInfo(String pluginId, VersionInfo version) {
        if (version != null) {
            for (ContextPluginInformation info : getAllContextPluginInfo()) {
                if (info.getPluginId().equalsIgnoreCase(pluginId) && info.getVersion().equals(version))
                    return info;
            }
        } else {
            Log.d(TAG, "getContextPluginInfo was missing a version for " + pluginId);
            return getContextPluginInfo(pluginId);
        }
        return null;
    }

    /**
     * Returns the latest version of the ContextPlugin for the specified pluginId String, or null if the information is
     * not found. This method searches ALL known plug-ins whether installed or not.
     */
    static ContextPlugin getContextPlugin(String pluginId) {
        ContextPlugin latest = null;
        for (ContextPlugin plug : getAllContextPlugins()) {
            if (plug.getId().equalsIgnoreCase(pluginId)) {
                if (latest != null) {
                    if (plug.getVersion().compareTo(latest.getVersion()) > 1)
                        latest = plug;
                } else {
                    latest = plug;
                }
            }
        }
        return latest;
    }

    /**
     * Returns the ContextPlugin for the specified pluginId String and version, or null if the information is not found.
     * This method searches ALL known plug-ins whether installed or not.
     */
    static ContextPlugin getContextPlugin(String pluginId, VersionInfo version) {
        if (version != null)
            for (ContextPlugin plug : getAllContextPlugins()) {
                if (plug.getId().equalsIgnoreCase(pluginId) && plug.getVersion().equals(version))
                    return plug;
            }
        else
            return getContextPlugin(pluginId);
        return null;
    }

    /**
     * Returns the ContextPlugin for the specified pluginId and version String, or null if the information is not found.
     * This method searches ALL known plug-ins whether installed or not.
     */
    static ContextPlugin getContextPlugin(String pluginId, String versionString) {
        return getContextPlugin(pluginId, VersionInfo.createVersionInfo(versionString));
    }

    /**
     * Returns the ContextPlugin for the specified plugInfo, or null if the information is not found. This method
     * searches ALL known plug-ins whether installed or not.
     */
    static ContextPlugin getContextPlugin(ContextPluginInformation plugInfo) {
        for (ContextPlugin plug : getAllContextPlugins()) {
            if (plug.getContextPluginInformation().equals(plugInfo))
                return plug;
        }
        return null;
    }

    /**
     * Returns a List of both installed and pending ContextPlugins (as as List of ContextPluginInformation).
     */
    static List<ContextPluginInformation> getAllContextPluginInfo() {
        List<ContextPluginInformation> plugInfoList = new ArrayList<ContextPluginInformation>();
        // Add all installed plug-ins known by the SettingsManager
        plugInfoList.addAll(getInstalledContextPluginInformation());
        // Access the ContextManager plug-ins, which have current install state (e.g., installing)
        for (ContextPlugin plug : ContextMgr.getAllContextPlugins()) {
            if (plug.getInstallStatus() != PluginInstallStatus.INSTALLED) {
                plugInfoList.add(plug.getContextPluginInformation());
            }
        }
		/*
		 * Finally, access any stragglers from the settings manager, deferring to the ContextManager's version so that
		 * proper state is maintained.
		 */
        for (ContextPluginInformation info : getPendingContextPluginInfo()) {
            // Only add if we *haven't* found this plug-in using the ContextManager
            if (!plugInfoList.contains(info))
                plugInfoList.add(info);
        }
        return plugInfoList;
    }

    /**
     * Returns a List of both installed and pending ContextPlugins.
     */
    public static List<ContextPlugin> getAllContextPlugins() {
        List<ContextPlugin> plugList = new ArrayList<ContextPlugin>();
        // Access the ContextManager plug-ins, which have current install state (e.g., installing)
        for (ContextPlugin plug : ContextMgr.getAllContextPlugins()) {
            plugList.add(plug);
        }
        // Add any other plug-ins known to the SettingsManager, deferring to existing state
        for (ContextPlugin settingsPlug : getInstalledContextPluginsFromDatabase()) {
            if (!plugList.contains(settingsPlug))
                plugList.add(settingsPlug);
        }
		/*
		 * Finally, access any stragglers from the settings manager, deferring to the ContextManager's version so that
		 * proper state is maintained.
		 */
        for (ContextPlugin info : getPendingContextPluginList()) {
            // Only add if we *haven't* found this plug-in using the ContextManager
            if (!plugList.contains(info))
                plugList.add(info);
        }
        return plugList;
    }

    /**
     * Returns true if the plug-in is installed; false otherwise.
     */
    static boolean isPluginInstalled(ContextPlugin plug) {
        return ContextMgr.isPluginInstalled(plug);
    }

    /**
     * Returns the Dynamix base Android Activity, or null if none is set (e.g., if we started from auto-boot).
     */
    static Activity getBaseActivity() {
        return baseActivity;
    }

    /**
     * Returns a FrameworkConfiguration based on a text-based Dynamix Framework configuration file who's path is derived
     * from the incoming Android Context. The configuration file must adhere to the Dynamix Framework configuration
     * specification outlined in the Dynamix developer documentation. Note that if the config file does not exist, the
     * default config file will be copied from Dynamix's res/raw directory.
     *
     * @param context The Android Context of the Dynamix Framework
     * @return A DynamixConfiguration
     * @throws Exception Detailed information about configuration file errors
     */
    static FrameworkConfiguration createFrameworkConfigurationFromPropsFile(Context context) throws Exception {
        Log.i(TAG, "Creating FrameworkConfiguration...");
        // Get the Dynamix data path
        String dataPath = Utils.getDataDirectoryPath();
        // Ensure the 'conf' directory exists in the dataPath
        File dstPath = new File(dataPath + "conf");
        dstPath.mkdirs();
        // Create the configFile using the dstPath
        File configFile = new File(dstPath, "dynamix.conf");
        // Copy the default config file from Dynamix's packaged resources,
        // if the file does not already exist
        if (!configFile.exists()) {
            InputStream in = context.getResources().openRawResource(R.raw.dynamix);
            OutputStream out = new FileOutputStream(configFile);
            // Transfer bytes from in to out
            byte[] buf = new byte[8192];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
        return FrameworkConfiguration.createFromPropsFile(configFile.getCanonicalPath());
    }

    /**
     * Extracts the Dynamix KeyFile to disk.
     *
     * @param context
     * @throws Exception
     */
    static void initKeyStore(Context context) throws Exception {
        Log.i(TAG, "Initializing KeyStore using Android Context: " + context);
        // Get the Dynamix data path
        String dataPath = Utils.getDataDirectoryPath();
        // Ensure the 'conf' directory exists in the dataPath
        File dstPath = new File(dataPath + "conf");
        dstPath.mkdirs();
        // Check if the keystore exists
        // Create the configFile using the dstPath
        File keystoreFile = new File(dstPath, "trusted_webconnector_certs.bks");
        keyStorePath = keystoreFile.getAbsolutePath();
        // Copy the default config file from Dynamix's packaged resources,
        // if the file does not already exist
        if (!keystoreFile.exists()) {
            InputStream in = context.getResources().openRawResource(R.raw.trusted_webconnector_certs);
            OutputStream out = new FileOutputStream(keystoreFile);
            // Transfer bytes from in to out
            byte[] buf = new byte[8192];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }

    /**
     * Returns the ClassLoader for the specified ContextPlugin.
     */
    public static ClassLoader getContextPluginClassLoader(ContextPlugin plug) {
        return OsgiMgr.getContextPluginClassLoader(plug);
    }

    /**
     * Restarts the plug-ins bundle using the OSGIManager.
     *
     * @param plug The plug-in to restart.
     * @return True if the plug-in was restarted; false otherwise.
     */
    public static boolean restartPluginBundle(ContextPlugin plug) {
        return OsgiMgr.restartPluginBundle(plug);
    }

    /**
     * Returns the Dynamix Framework's current session id.
     */
    static UUID getFrameworkSessionId() {
        return frameworkSessionId;
    }

    /**
     * Returns the ContextPlugin associated with the incoming pluginId, or null if the ContextPlugin is not found. Uses
     * the ContextManager, so Dynamix must be running in order to call this method and get results.
     */
    static ContextPluginInformation getInstalledContextPluginInfo(String pluginId) {
        return ContextMgr.getContextPluginInfo(pluginId);
    }

    /**
     * Callback for bundle installed events from the OSGIManager. This method handles dynamically installed plugins.
     *
     * @param plug The ContextPlugin whoes bundle was installed
     */
    static synchronized void handleBundleInstalled(final ContextPlugin plug, final IPluginInstallCallback callback) {
        Log.d(TAG, "handleBundleInstalled for " + plug + " with installed " + plug.isInstalled());
        // Make sure the plugin is valid and installed
        if (plug != null && plug.getInstallStatus() == PluginInstallStatus.INSTALLING) {
            // Set plug-in enabled initially (user can change this later)
            plug.setEnabled(true);
            // Set installed state
            plug.setInstallStatus(PluginInstallStatus.INSTALLED);
            // Try to access settings for the plug-in
            ContextPluginSettings settings = SettingsManager.getContextPluginSettings(plug);
            ContextMgr.initializeContextPlugin(plug, OsgiMgr.getContextPluginRuntimeFactory(plug), settings,
                    new Callback() {
                        @Override
                        public void onSuccess() {
                            // Update our SettingsManager
                            if (SettingsManager.addContextPlugin(plug)) {
								/*
								 * Update the apps with the version from the database.
								 */
                                SessionManager.refreshApps();
                                if (DynamixService.isFrameworkStarted()) {
									/*
									 * If a plug-in is a background service, we start it here. Otherwise, the plug-in
									 * will be started in conjunction with the adding of context support.
									 */
                                    if (plug.isBackgroundService()) {
                                        ContextMgr.startPlugin(plug, false, new Callback() {
                                            public void onSuccess() throws RemoteException {
                                                // Notify all listeners that the install is complete
                                                SessionManager.sendPluginInstallComplete(callback,
                                                        plug.getContextPluginInformation());
                                                // Update all listeners that a new plugin is available
                                                SessionManager.notifyAllNewContextPluginInstalled(plug);
                                            }

                                            public void onFailure(String message, int errorCode) throws RemoteException {
                                                Log.w(TAG, "Failed to start: " + plug);
                                                SessionManager.sendPluginInstallFailed(callback,
                                                        plug.getContextPluginInformation(),
                                                        "DYNAMIX_FRAMEWORK_ERROR could not start " + plug,
                                                        ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                                uninstallPlugin(plug);
                                            }
                                        });
                                    } else {
                                        // Notify all listeners that the install is complete
                                        SessionManager.sendPluginInstallComplete(callback,
                                                plug.getContextPluginInformation());
                                        // Update all listeners that a new plugin is available
                                        SessionManager.notifyAllNewContextPluginInstalled(plug);
                                    }
                                } else {
                                    // Notify all listeners that the install is complete
                                    SessionManager.sendPluginInstallComplete(callback,
                                            plug.getContextPluginInformation());
                                    // Update all listeners that a new plugin is available
                                    SessionManager.notifyAllNewContextPluginInstalled(plug);
                                }
                            } else {
                                String message = "DYNAMIX_FRAMEWORK_ERROR could not add plug-in to database " + plug;
                                Log.w(TAG, message);
                                SessionManager.sendPluginInstallFailed(callback, plug.getContextPluginInformation(),
                                        message, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                                uninstallPlugin(plug);
                            }
                        }

                        @Override
                        public void onFailure(String message, int errorCode) throws RemoteException {
                            SessionManager.sendPluginInstallFailed(callback, plug.getContextPluginInformation(),
                                    "Plug-in failed to initialize: " + message, errorCode);
                            uninstallPlugin(plug);
                        }
                    });
        } else
            Log.e(TAG, "handleBundleInstalled received null or uninstalled plug-in: " + plug);
    }

    /**
     * Callback for bundle installed failed events from the OSGIManager.
     *
     * @param plug The ContextPlugin that failed to be installed.
     */
    static void handleBundleInstallError(ContextPlugin plug) {
        Log.d(TAG, "handleBundleInstallError for " + plug);
        uninstallPlugin(plug, null, null);
    }

    /**
     * Callback for OSGi framework errors.
     */
    static void onOSGiFrameworkError() {
        Log.w(TAG, "onOSGiFrameworkError!!");
        onDynamixInitializingError("OSGi Error");
        // TODO: Handle this case (need to check when these errors are thrown,
        // i.e. is it always fatal?
    }

    /**
     * Callback for the OSGi started event. This is called once the OSGi Framework has completely initialized and
     * started. This method completes the Dynamix boot sequence, if necessary.
     */
    static void onOSGiFrameworkStarted() {
        Log.d(TAG, "onOSGiFrameworkStarted with bootState: " + bootState);
        synchronized (bootState) {
            if (bootState == BootState.BOOTING) {
                completeBoot();
            } else
                throw new RuntimeException("Received onOSGiFrameworkStarted when not BOOTING");
        }
    }

    /**
     * Callback for the OSGi stopped event.
     */
    static void onOSGiFrameworkStopped() {
        Log.d(TAG, "onOSGiFrameworkStopped");
    }

    /**
     * Registers the specified Activity as belonging to the ContextPluginRuntime. Used to programmatically close the
     * Activity later.
     *
     * @param runtime  The ContextPluginRuntime.
     * @param activity The associated Activity.
     */
    static void registerConfigurationActivity(ContextPluginRuntime runtime, Activity activity) {
        if (ContextMgr != null) {
            ContextMgr.registerConfigurationActivity(runtime, activity);
        }
    }

    /**
     * Registers the specified Activity as belonging to the ContextPluginRuntime. Used to programmatically close the
     * Activity later.
     *
     * @param runtime  The ContextPluginRuntime.
     * @param activity The associated Activity.
     */
    static void registerContextAcquisitionActivity(ContextPluginRuntime runtime, Activity activity) {
        if (ContextMgr != null) {
            ContextMgr.registerContextAcquisitionActivity(runtime, activity);
        }
    }

    /**
     * Removes all events for a given listener and contextType, regardless of expiration time.
     */
    static void removeCachedContextEvents(IContextListener listener, String contextType) {
        if (ContextMgr != null) {
            ContextMgr.removeCachedContextEvents(listener, contextType);
        }
    }

    /**
     * Removes all events for a given handler and contextType, regardless of expiration time.
     */
    static void removeCachedContextEvents(IContextHandler handler, String contextType) {
        if (ContextMgr != null) {
            ContextMgr.removeCachedContextEvents(handler, contextType);
        }
    }

    static ContextPluginRuntimeWrapper getContextPluginRuntime(String pluginId, String pluginVersion) {
        if (ContextMgr != null)
            return ContextMgr.getContextPluginRuntime(pluginId, pluginVersion);
        else
            return null;
    }

    static ContextPluginRuntimeWrapper getContextPluginRuntime(String pluginId, VersionInfo version) {
        if (ContextMgr != null)
            return ContextMgr.getContextPluginRuntime(pluginId, version);
        else
            return null;
    }

    /**
     * Requests a context interaction for a particular listener using a particular ContextPlugin and contextDataType.
     *
     * @param app           The app requesting the request.
     * @param handler       The handler to send results to.
     * @param plugin        The plugin to handle the request.
     * @param contextType   The type of context to scan for.
     * @param requestConfig An optional configuration Bundle.
     * @return An IdResult indicating if Dynamix accepted the request.
     */
    static IdResult handleContextRequest(DynamixApplication app, DynamixSession session, IContextHandler handler,
                                         VersionedPlugin plugin, String contextType, Bundle requestConfig) {
        // Make sure Dynamix is started
        if (isFrameworkStarted()) {
            // Try to access the plug-in
            ContextPlugin plug = ContextMgr.getContextPlugin(plugin.getPluginId(), plugin.getPluginVersionString());
            if (plug != null) {
                // Make sure the handler/plug-in combo has context support for the contextType
                List<ContextSupport> support = session.getAllContextSupport(handler, plug, contextType);
                if (support != null && !support.isEmpty()) {
                    // Make sure the app has firewall access
                    if (app.isAdmin() || app.isAccessGranted(contextType, plug.getContextPluginInformation())) {
                        // Make sure the plug-in is enabled
                        if (plug.isEnabled()) {
                            // Get the plug-in's runtime
                            ContextPluginRuntime runtime = ContextMgr.getContextPluginRuntime(plug);
                            if (runtime != null) {
                                // Ensure the app has a context support registration for the type
                                if (session.hasContextSupportForType(handler, contextType)) {
                                    // Make sure the plugin is configured
                                    if (!plug.isConfigured()) {
                                        Log.w(TAG, "Plugin Not Configured: " + plugin);
                                        return new IdResult("Plug-in not configured " + plug.getId(),
                                                ErrorCodes.PLUG_IN_NOT_CONFIGURED);
                                    } else {
                                        if (DynamixPreferences.isDetailedLoggingEnabled())
                                            Log.i(TAG, "Application: " + app + " is launching a context request");
                                        /*
                                         * Note: 2016.03.22: Max discovered that the 'containsKey' call below actually unparcels the requestConfig, which
                                         * throws class not found errors when embedded IContextInfo is present within the bundle.
                                         */
                                        // Update WEB_REQUEST status, if not set
//                                        if (requestConfig != null) {
//                                            if (!requestConfig.containsKey(PluginConstants.WEB_REQUEST))
//                                                requestConfig.putBoolean(PluginConstants.WEB_REQUEST, false);
//                                        }
                                        // Register a request channel
                                        UUID requestId = ContextMgr.registerContextRequestChannel(app, handler,
                                                plug, contextType);
											/*
											 * Launch the context request. Note that the 'HandleContextRequest' class
											 * determines whether to use a general or configured context scan based on
											 * whether the requestConfig is null. The HandleContextRequest class can
											 * also launch user interfaces, if the plug-in provides one.
											 */
                                        ContextPluginRuntimeMethodRunners.launchThread(
                                                new HandleContextRequest(app, session.getSessionId().toString(),
                                                        session.getContextHandlerId(handler), plug
                                                        .getContextPluginInformation(), runtime, requestId,
                                                        contextType, requestConfig), ContextManager
                                                        .getThreadPriorityForPowerScheme());
                                        return new IdResult(requestId.toString());
                                    }
                                } else
                                    return new IdResult("No context support for " + contextType,
                                            ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
                            } else {
                                if (plug.getInstallStatus() == PluginInstallStatus.INSTALLING) {
                                    return new IdResult("Plug-in installing " + plug.getId(),
                                            ErrorCodes.RESOURCE_BUSY);
                                } else
                                    return new IdResult("Could not find runtime for " + plug.getId(),
                                            ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                            }
                        } else {
                            if (plug.getInstallStatus() == PluginInstallStatus.INSTALLING) {
                                return new IdResult("Plug-in installing " + plug.getId(), ErrorCodes.RESOURCE_BUSY);
                            } else
                                return new IdResult("Plug-in disabled " + plug.getId(), ErrorCodes.PLUG_IN_DISABLED);
                        }
                    } else {
                        return new IdResult("NOT_AUTHORIZED: No Firewall Access for plug-in " + plug.getId()
                                + " and context type " + contextType, ErrorCodes.NOT_AUTHORIZED);
                    }
                } else {
                    Log.w(TAG, app + " has no context support for " + contextType);
                    return new IdResult(app + " has no context support for " + contextType,
                            ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
                }
            } else {
                Log.w(TAG, "Requested plug-in not found: " + plugin);
                return new IdResult("Requested plug-in not found: " + plugin, ErrorCodes.PLUG_IN_NOT_FOUND);
            }
        } else
            return new IdResult("Dynamix Not Started", ErrorCodes.NOT_READY);
    }

    /**
     * Opens the specified plug-in's configuration view (if it has one) on behalf of a DynamixApplication.
     *
     * @param app      The app requesting the configuation view.
     * @param pluginId The plug-in to configure.
     * @return Result indicating success or failure.
     */
    public static Result openContextPluginConfigurationForApp(DynamixApplication app, String pluginId,
                                                              String pluginVersion, Bundle viewConfig, boolean frameworkCall) {
        return doContextPluginConfigurationView(app, pluginId, pluginVersion, viewConfig, frameworkCall);
    }

    /**
     * Opens the specified plug-in's configuration view (if it has one) on behalf of the Dynamix Framework.
     *
     * @param pluginId The plug-in to configure.
     * @return Result indicating success or failure.
     */
    public static Result openContextPluginConfigurationForFramework(String pluginId, String pluginVersion) {
        return doContextPluginConfigurationView(null, pluginId, pluginVersion, null, true);
    }

    /**
     * Opens the specified plug-in's configuration view (if it has one). Handles both app and framework calls.
     *
     * @param app           The app requesting the configuation view (may be null if frameworkCall == true).
     * @param pluginId      The plug-in to configure.
     * @param frameworkCall True if this call originates from Dynamix; false otherwise.
     * @return
     */
    private static Result doContextPluginConfigurationView(DynamixApplication app, String pluginId,
                                                           String pluginVersion, Bundle viewConfig, boolean frameworkCall) {
        if (DynamixService.isFrameworkStarted()) {
            if (frameworkCall || app != null) {
                ContextPlugin plug = ContextMgr.getContextPlugin(pluginId, pluginVersion);
                if (plug != null) {
                    // Make sure the interface is launchable
                    ContextPluginRuntime runtime = ContextMgr.getContextPluginRuntime(plug);
                    if (runtime != null) {
                        IPluginView view = null;
                        if (viewConfig == null)
                            view = runtime.getDefaultConfigurationView();
                        else
                            view = runtime.getConfigurationView(viewConfig);
                        if (view != null) {
                            if (true) {
                                // Check if there is a configuration interface for
                                if (!HostActivity.isBound() && HostActivity.bindPlugin(plug, view)) {
                                    Intent intent = new Intent(DynamixService.getAndroidContext(), HostActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    DynamixService.getAndroidContext().startActivity(intent);
                                    return new Result();
                                } else {
                                    return new Result("UI is busy... try again later!", ErrorCodes.RESOURCE_BUSY);
                                }
                            } else {
                                return new Result("NOT_AUTHORIZED to launch UI", ErrorCodes.NOT_AUTHORIZED);
                            }
                        } else {
                            return new Result("No configuration available for " + plug.getName(),
                                    ErrorCodes.CONFIGURATION_ERROR);
                        }
                    } else {
                        return new Result("No runtime available for " + plug.getName(), ErrorCodes.STATE_ERROR);
                    }
                } else
                    return new Result("Plug-in not found " + pluginId, ErrorCodes.PLUG_IN_NOT_FOUND);
            } else
                return new Result("App not found " + app, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
        } else
            return new Result("Dynamix Not Started", ErrorCodes.NOT_READY);
    }

    /**
     * Utility method for setting the BaseActivity.
     */
    static void setBaseActivity(Activity activity) {
        baseActivity = activity;
        Log.i(TAG, "setBaseActivity with " + activity);
    }

    /**
     * Sets up the Dynamix Framework database using the incoming Context. If an existing database is already open, this
     * method closes it first. In addition to DynamixService.boot, this method is also called by the BootUpReceiver,
     * which is used to start the DynamixFramework in the background when the device firsts starts up.
     */
    static void setupDatabase(FrameworkConfiguration config, Context context, final Callback callback) {
        // If a SettingsManager already exists, close it before creating a new one
        if (SettingsManager != null) {
            SettingsManager.closeDatabase();
        }
        // Grab the Dynamix's data path
        String dataPath = Utils.getDataDirectoryPath();
        // Create the database path using dbPath and the incoming FrameworkConfiguration
        final String dbPath = dataPath + config.getDatabaseFilePath();
        // Create a SettingsManager and open the database
        SettingsManager = new DB4oSettingsManager();
        Utils.dispatch(true, new Runnable() {
            @Override
            public void run() {
                try {
                    SettingsManager.openDatabase(dbPath);
                    SessionManager.sendCallbackSuccess(callback);
                } catch (Exception e) {
                    SessionManager.sendCallbackFailure(callback, e.toString(), ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
                }
            }
        });
    }

    /**
     * Starts the context plug-in update timer using the update interval set in the Dynamix preferences.
     */
    static void startContextPluginUpdateTimer() {
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                if (DynamixPreferences.autoContextPluginUpdateCheck()) {
                    updateContextPluginUpdateTimer(DynamixPreferences.getContextPluginUpdateInterval());
                }
            }
        });
    }

    /**
     * Stops the context plug-in update timer.
     */
    static void stopContextPluginUpdateTimer() {
        if (contextPlugUpdateTimer != null)
            contextPlugUpdateTimer.cancel();
        contextPlugUpdateTimer = null;
    }

    /**
     * Unregisteres a previoully registered plug-in configuration activity.
     *
     * @param runtime The ContextPluginRuntime wishing to unregister its configuration activity.
     */
    static void unRegisterConfigurationActivity(ContextPluginRuntime runtime) {
        if (ContextMgr != null) {
            ContextMgr.unRegisterConfigurationActivity(runtime);
        }
    }

    /**
     * Unregisteres a previoully registered context acquisition activity.
     *
     * @param runtime The ContextPluginRuntime wishing to unregister its context acquisition activity.
     */
    static void unregisterContextAcquisitionActivity(ContextPluginRuntime runtime) {
        if (ContextMgr != null) {
            ContextMgr.unregisterContextAcquisitionActivity(runtime);
        }
    }

    /**
     * Replaces the authorized application with the incoming application in both the SettingsManager and SessionManager
     * (if Dynamix is running).
     */
    public static boolean updateApplication(DynamixApplication app) {
        if (SettingsManager.updateDynamixApplication(app)) {
            if (SessionManager.updateSessionApplication(app)) {
                SessionManager.refreshApps();
                return true;
            } else {
                Log.w(TAG, "Could not update session for app");
                return false;
            }
        } else {
            Log.w(TAG, "SettingsManager could not update: " + app);
            return false;
        }
    }

    /**
     * Updates the context plug-in update timer with a new check interval.
     *
     * @param interval How often to check for updates (in milliseconds).
     */
    static void updateContextPluginUpdateTimer(long interval) {
        stopContextPluginUpdateTimer();
        Log.i(TAG, "Setting timer for update check every: " + interval + " milliseconds");
        contextPlugUpdateTimer = new CountDownTimer(interval, interval) {
            @Override
            public void onFinish() {
                Log.i(TAG, "Auto Context Plug-in Update!");
                // Use the update manager to check for updates
                checkForUpdates(true);
                // Restart another update check, if necessary
                if (DynamixPreferences.autoContextPluginUpdateCheck())
                    updateContextPluginUpdateTimer(DynamixPreferences.getContextPluginUpdateInterval());
            }

            @Override
            public void onTick(long millisUntilFinished) {
                // Log.i(TAG, "Time until update: " + millisUntilFinished);
            }
        }.start();
    }

    /**
     * Returns the current Android context.
     */
    public static Context getAndroidContext() {
        return androidContext;
    }

    /**
     * Completes the Dynamix boot sequence after the OSGi Framework has started and the Dynamix Service has started.
     */
    private static void completeBoot() {
        // Only completeBoot if we're BOOTING
        synchronized (bootState) {
            if (bootState == BootState.BOOTING) {
                Log.i(TAG, "Completing the Dynamix Service boot sequence...");
				/*
				 * CAUTION: If we call Looper.prepare() here, the Dynamix UI becomes locked up during long-taps and menu
				 * taps. Not sure why.
				 */
                // Set booted state
                bootState = BootState.BOOTED;
				/*
				 * Setup our internal Web server to handle web clients. Note that we can only start the WebConnector
				 * after booting, since it needs the webFacade to be properly initialized.
				 */
                DynamixService.startWebConnectorUsingConfigData();
                // Send initialized event
                onDynamixInitialized(service);
                // Update HomeActivity UI
                uiHandle.post(new Runnable() {
                    @Override
                    public void run() {
						/*
						 * Handle Dynamix updates
						 */
                        if (DynamixPreferences.isDynamixUpdateAvailable()) {
                            if (DynamixPreferences.isDynamixUpdateRequired()) {
								/*
								 * Force an update check to reset improper update settings.
								 */
                                checkForUpdates(true);
                                if (HomeActivity.activity != null) {
                                    Utils.showUpdateAlert(HomeActivity.activity, true,
                                            DynamixPreferences.getDynamixUpdateMessage(),
                                            DynamixPreferences.getDynamixUpdateUrl());
                                    return;
                                } else {
                                    System.exit(0);
                                    return;
                                }
                            } else {
                                if (HomeActivity.activity != null) {
                                    Utils.showUpdateAlert(HomeActivity.activity, false,
                                            DynamixPreferences.getDynamixUpdateMessage(),
                                            DynamixPreferences.getDynamixUpdateUrl());
                                    return;
                                }
                            }
                        }
                        // No updates, so continue...
                        Log.i(TAG, "Dynamix Enabled: " + DynamixPreferences.isDynamixEnabled());
                        Log.i(TAG, "Dynamix Auto-start: " + DynamixPreferences.autoStartDynamix());
                        Log.i(TAG, "Dynamix startRequested: " + startRequested);
                        Log.i(TAG, "Dynamix clean exit: " + SettingsManager.hadCleanExit());
                        // Set recovery mode state if we didn't have a clean exit
                        /**
                         * TODO: Finish recovery mode. Currently it cannot distinguish between crashes
                         * and restarts due to debugging, etc.
                         */
                        // recoveryMode = !SettingsManager.hadCleanExit();
						/*
						 * Deactivate framework if we didn't exit cleanly
						 */
                        if (!recoveryMode) {
                            boolean shouldStart = (DynamixPreferences.isDynamixEnabled() && DynamixPreferences
                                    .autoStartDynamix()) || startRequested;
                            Log.i(TAG, "Dynamix starting: " + shouldStart);
                            // Process startFramework, if necessary
                            if (shouldStart || startRequested)
                                startFramework();
                        } else {
                            // SettingsManager.setCleanExit(true);
                            Log.w(TAG, "Dynamix did not exit cleanly... not starting framework.");
                            onDynamixInitializingError("Dynamix did not exit cleanly... not starting framework.");
                        }
                    }
                });
                Log.i(TAG, "Dynamix has finished booting!");
            } else
                Log.w(TAG, "completeBoot called when not booting");
        }
    }

    /**
     * Default implementation that returns null.
     */
    public IBinder asBinder() {
        // return facadeBinder;
        return null;
    }

    /**
     * Returns the Dynamix facade to requesting clients.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "onBind is returning facadeBinder: " + facadeBinder);
        return facadeBinder;
    }

    /**
     * Sets up initial Dynamix service state.
     */
    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate: Initializing Dynamix from state: " + bootState);
        super.onCreate();
        // Notify initializing
        onDynamixInitializing();
		/*
		 * !!! IMPORTANT !!! A call to the SessionManager MUST be made by our thread before any others so that the
		 * SessionManager's Handler is bound to our main thread. DO NOT REMOVE THIS CALL FROM THIS LOCATION.
		 */
        SessionManager.getAllSessions();
		/*
		 * Prepare our looper, if needed.
		 */
        if (Looper.myLooper() == null)
            Looper.prepare();
        // Check boot state
        synchronized (bootState) {
			/*
			 * We will be NOT_BOOTED if Dynamix crashed and was restarted by Android. In this case, boot will not have
			 * been called..
			 */
            if (bootState == BootState.NOT_BOOTED) {
                bootState = BootState.BOOTING;
                Log.w(TAG, "onCreate NOT_BOOTED... we probably crashed!");
                // Set the Android context to 'this', since we'll have been restarted by the system
                androidContext = this;
                // Also, make sure to set the Util context
                Utils.setContext(this);
                // Call startFramework to cache a start request
                startFramework();
            }
            if (bootState == BootState.BOOTING) {
                // Set service is running
                androidServiceRunning = true;
                // Create a framework session id for secure internal communications
                frameworkSessionId = UUID.randomUUID();
                // Ensure the keystore is created
                try {
                    if (this instanceof Context)
                        initKeyStore(this);
                    else if (getAndroidContext() != null)
                        initKeyStore(getAndroidContext());
                    else
                        throw new Exception("Missing Android Context for initKeyStore!");
                } catch (Exception e2) {
                    Log.w(TAG, "Problem initializing keystore: " + e2);
                }
                // Setup state for embedded and service modes
                if (!embeddedMode) {
                    // Set context on our preferences
                    DynamixPreferences.setContext(this);
					/*
					 * Remember our service. If we're running in embeddedMode, the service will have already been
					 * created by 'bootEmbedded'.
					 */
                    service = this;
					/*
					 * In service mode, the androidContext is the service itself. If we're running in embeddedMode, the
					 * androidContext will have already been set by 'bootEmbedded'.
					 */
                    DynamixService.androidContext = this;
					/*
					 * Handle first run
					 */
                    if (DynamixPreferences.isFirstRun()) {
                        Log.i(TAG, "Handling Dynamix first run");
                        // Set first run to false
                        DynamixPreferences.setFirstRun(false);
                        // Generate our instance id
                        DynamixPreferences.setDynamixInstanceId(UUID.randomUUID().toString());
                        Log.i(TAG, "Dynamix Instance ID: " + DynamixPreferences.getDynamixInstanceId());
                        // Generate our RSA keys
                    }
					/*
					 * In service mode, the config needs to be created. If we're running in embeddedMode, the config
					 * will have already been set by 'bootEmbedded'.
					 */
                    try {
                        if (this instanceof Context)
                            config = createFrameworkConfigurationFromPropsFile(this);
                        else if (getAndroidContext() != null)
                            config = createFrameworkConfigurationFromPropsFile(getAndroidContext());
                        else
                            throw new Exception("Missing Android Context for createFrameworkConfigurationFromPropsFile!");
                    } catch (Exception e1) {
                        Log.e(TAG, "Framework Configuration Exception: " + e1);
                        onDynamixInitializingError(e1.toString());
                        return;
                    }
                    // Setup foreground service handling so that Android tries desperately to keep us alive.
                    try {
                        foregroundHandler = new AndroidForeground(
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE), getClass().getMethod(
                                "startForeground", AndroidForeground.mStartForegroundSignature), getClass()
                                .getMethod("stopForeground", AndroidForeground.mStopForegroundSignature));
                    } catch (NoSuchMethodException e) {
                        // Running on an older platform.
                        foregroundHandler = new AndroidForeground(
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE), null, null);
                    }
                    foregroundHandler.startForegroundCompat(1, new Notification(), this);
                }
                // Setup the database
                setupDatabase(DynamixService.getConfig(), service, new Callback() {
                    @Override
                    public void onSuccess() throws RemoteException {
						/*
						 * Setup the primary repositories specified in the config
						 */
                        Repository primaryRepo = getConfig().getPrimaryContextPluginRepo();
                        Repository backupRepo = getConfig().getBackupContextPluginRepo();
                        Repository localRepo = getConfig().getLocalPluginRepo();
                        if (primaryRepo != null && !SettingsManager.getPluginRepositories().contains(primaryRepo))
                            SettingsManager.addPluginRepository(primaryRepo);
                        if (backupRepo != null && !SettingsManager.getPluginRepositories().contains(backupRepo))
                            SettingsManager.addPluginRepository(backupRepo);
                        if (localRepo != null && !SettingsManager.getPluginRepositories().contains(localRepo))
                            SettingsManager.addPluginRepository(localRepo);
                        // Setup the ContextManager
                        if (ContextMgr != null) {
                            // If the ContextManager exists, pause it... it will be started again when Dynamix starts.
                            if (ContextMgr.isStarted())
                                ContextMgr.pauseContextHandling();
                        } else {
                            // Create the Context Manager
                            ContextMgr = new ContextManager(DynamixService.androidContext, SettingsManager
                                    .getPowerScheme(), DynamixService.getConfig().getContextCacheMaxEvents(),
                                    DynamixService.getConfig().getContextCacheMaxDurationMills(), DynamixService
                                    .getConfig().getContextCacheCullIntervalMills());
                            Log.i(TAG, "Created the Dynamix ContextManager");
                        }
						/*
						 * Create our binders, which allow clients to connect the Dynamix.
						 */
                        facadeBinder = new AppFacadeBinder(DynamixService.this, ContextMgr, embeddedMode, true);
                        // webFacade = new WebFacadeBinder(this, ContextMgr, embeddedMode);
                        // Setup support for self-signed certs, if requested
                        if (config.allowSelfSignedCertsDefault())
                            Utils.acceptAllSelfSignedSSLcertificates();
                        else
                            Utils.denyAllSelfSignedSSLcertificates();
                        // Create our restart PendingIntent (for use in Dynamix restart requests)
                        Intent broadcastReceiverIntent = new Intent(androidContext, BootUpReceiver.class);
                        broadcastReceiverIntent.putExtra("restart", true);
                        RESTART_INTENT = PendingIntent.getBroadcast(androidContext, 0, broadcastReceiverIntent, 0);
						/*
						 * Launch the OSGi framework. Note that osgiMgr.init() runs on its own thread and calls us back
						 * using onOSGiFrameworkStarted, which continues the boot process.
						 */
                        if (OsgiMgr != null) {
                            // If the OsgiMgr exists, stop it before continuing.
                            OsgiMgr.stopFramework();
                            Log.w(TAG, "OSGi Manager was not null during onCreate");
                        }
                        OsgiMgr = new OSGIManager(DynamixService.this);
                        OsgiMgr.init();
                        Log.i(TAG, "Initializing the Dynamix OSGi Manager...");
                    }

                    @Override
                    public void onFailure(String message, int errorCode) throws RemoteException {
                        Log.e(TAG, "Open database error: " + message);
                        DynamixService.onDynamixInitializingError("Open database error: " + message);
                    }
                });
            } else {
                Log.e(TAG, "Init was called in state " + bootState);
				/*
				 * Don't throw exceptions here, since there have been reports of onCreate() being called when we're in
				 * state Booted.
				 */
                // throw new RuntimeException("Init was called in state " + bootState);
            }
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        if (isFrameworkInitialized()) {
            // Set cleanexit to true, since Android is probably removing the service
            SettingsManager.setCleanExit(true);
            destroyFramework(true, false);
        }
        super.onDestroy();
    }

    @Override
    public void onRebind(Intent intent) {
        // TODO: Handle rebind?
    }

    /**
     * Handles the Android onStartCommand life-cycle call. This is called after onCreate() when running as a service.
     * Note that there is a bug in Android 2.3/3.0 where onStartCommand will *not* be called in the case of a crash and
     * then subsequest restart. https://groups.google.com/forum/?hl=en&fromgroups#!topic/android-developers/2H-zkME9FB0
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand with bootState: " + bootState);
        // Return START_STICKY so that Android tries to keep us alive (or restarts us)
        return Service.START_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "App onUnbind for UID: " + Binder.getCallingUid());
        super.onUnbind(intent);
		/*
		 * Using onUnbind won't work reliably for discovering disconnected remote clients. This is because the
		 * IDynamixFacade is created once and the binder is passed to all clients. The Context Manager listens for
		 * onCallbackDied events from connected applications, and calls DynamixService.removeContextListener(listener,
		 * true) whether Dynamix is active or not. This means that we don't need to do any cleanup in this method.
		 */
        // Return false so that Android does not call rebind when the client tries to connect again.
        return false;
    }

    /**
     * Experimental: This method is called as services are registered in the OSGi framework. Security -
     * ConditionalPermissionAdmin
     */
    protected synchronized void handleServiceEvent(ServiceEvent event) {
        Log.d(TAG, "handleServiceEvent of type: " + event.getType());
		/*
		 * Event type int values: Type == 1 (Registered?) Type == 4 (Unregistered?)
		 */
        if (event.getType() == ServiceEvent.REGISTERED) {
            // TODO
        }
    }

    /**
     * Starts the Dynamix Framework.
     */
    private synchronized void doStartFramework() {
        Log.d(TAG, "doStartFramework with bootState: " + bootState);
        if (isFrameworkInitialized()) {
            if (startState == StartState.STOPPED) {
                synchronized (startState) {
                    startState = StartState.STARTING;
                }
				/*
				 * Set clean exit to false to catch runtime problems. After booting Dynamix, we only start the framework
				 * if the exit was clean. This way, problem plug-ins can be removed without needing to re-install
				 * Dynamix.
				 */
                SettingsManager.setCleanExit(false);
                // Notify starting
                onDynamixStarting();
                // Force an update check
                checkForUpdates(true);
                // Start the update check timer
                startContextPluginUpdateTimer();
                // Set a BroadcastReceiver to startContextDetection on ACTION_SCREEN_ON events
                wakeReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        // Handle device wake!
                        Log.v(TAG, "wakeReceiver onReceive called");
                        if (isFrameworkStarted()) {
                            if (!DynamixPreferences.backgroundModeEnabled()) {
                                Log.i(TAG, "Resuming Dynamix");
                                ContextMgr.startContextManager();
                                // WebConnector.resumeTimeoutChecking();
                                // Notify apps that we're active
                                SessionManager.notifyAllDynamixFrameworkActive();
                            }
                        }
                    }
                };
                registerReceiver(wakeReceiver, new IntentFilter(Intent.ACTION_SCREEN_ON));
                // Set a BroadcastReceiver to stopContextDetection on ACTION_SCREEN_OFF events
                sleepReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        // Handle device sleep!
                        Log.v(TAG, "sleepReceiver onReceive called");
                        if (isFrameworkStarted()) {
                            if (!DynamixPreferences.backgroundModeEnabled()) {
                                Log.i(TAG, "Pausing Dynamix");
                                ContextMgr.pauseContextHandling();
                                // WebConnector.pauseTimeoutChecking();
                                // Notify apps that we're inactive
                                SessionManager.notifyAllDynamixFrameworkInactive();
                            } else
                                Log.i(TAG, "Continuing context support while screen is off");
                        }
                    }
                };
                registerReceiver(sleepReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
				/*
				 * Complete start on a thread...
				 */
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "doStartFramework continuing to start with state: " + startState);
                        // State bookkeeping
                        startRequested = false;
                        // Tell the OSGiManager about Dynamix
                        OsgiMgr.setService(DynamixService.this);
                        // Start the ContextManager (re-starts initialized plug-ins)
                        Log.d(TAG, "doStartFramework is trying to start context manager");
                        ContextMgr.startContextManager();
                        while (!ContextMgr.isStarted()) {
                            Log.d(TAG, "Waiting for ContextManager to start...");
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                            }
                        }
                        Log.d(TAG, "ContextManager has started!");
                        // Check for plug-in inconsistency (e.g, plug-ins in installed and available)
                        cleanDatabaseState();
                        // Init our installed plugins
                        Log.d(TAG, "doStartFramework is initializing plug-ins");
                        initializeInstalledPlugins();
                        // We are initialized (set our static reference)
                        DynamixService.service = DynamixService.this;
                        // Clear temp apps
                        List<DynamixApplication> remove = new ArrayList<DynamixApplication>();
                        for (DynamixApplication app : getAllDynamixApplications()) {
                            if (app.isRemotelyPaired() && app.getRemotePairing().isTemporary())
                                remove.add(app);
                        }
                        for (DynamixApplication app : remove) {
                            removeApplicationFromSettings(app);
                        }
                        // Set started state
                        DynamixPreferences.setDynamixEnabledState(true);
                        synchronized (startState) {
                            startState = StartState.STARTED;
                        }
                        Log.i(TAG, "Dynamix Service Started!");
						/*
						 * Since we started, reset the recoveryMode flag
						 */
                        recoveryMode = false;
						/*
						 * Register our firewall receiver. Must be done AFTER setting startState to STARTED!
						 */
                        registerFirewallReceiver();
                        // Start our restart alarm (runs every 10 minutes: 600000 millis)
                        startRestartAlarm(600000);
                        // Send started notifications
                        onDynamixStarted();
                        SessionManager.notifyAllDynamixFrameworkActive();
                    }
                }).start();
            } else
                Log.w(TAG, "Cannot start from state: " + startState);
        } else
            Log.w(TAG, "Cannot start Dynamix because it's not initialized");
    }

    /**
     * Cleans up database state. For example, removes installed plug-ins from the pending plug-in
     * list. State problems like this may occur of Dynamix crashes during plug-in installation.
     */
    private void cleanDatabaseState() {
        // Grab all pending plug-ins
        List<ContextPlugin> pendingSnapshot = getPendingContextPluginList();
        // Grab all installed plug-ins
        List<ContextPlugin> installedSnapshot = getInstalledContextPlugins();
        // Remove each installed plug-in from the pending list
        for (ContextPlugin plug : installedSnapshot) {
            if (pendingSnapshot.contains(plug)) {
                Log.w(TAG, "Removing installed plug-in found in pending plug-in list: " + plug);
                uninstallPlugin(plug);
            }
        }
    }

    /**
     * Initializes all installed ContextPlugins.
     */
    private synchronized void initializeInstalledPlugins() {
        // First, get the list of ContextPlugins from the SettingsManager
        List<ContextPlugin> plugs = SettingsManager.getInstalledContextPlugins();
        // Note that the ContextManager manages *all* plugins, even those that are not installed yet.
        for (final ContextPlugin plug : plugs) {
            // If the plug is installed, start its bundle and then add it to the ContextManager for management.
            if (plug.isInstalled()) {
                // Try to start the plugin's OSGi Bundle
                if (OsgiMgr.startPluginBundle(plug)) {
					/*
					 * Add the ContextPlugin to the ContextMgr, getting the plugin's ContextPluginRuntimeFactory from
					 * the OsgiMgr.
					 */
                    ContextPluginSettings settings = SettingsManager.getContextPluginSettings(plug);
                    ContextMgr.initializeContextPlugin(plug, OsgiMgr.getContextPluginRuntimeFactory(plug), settings,
                            new Callback() {
                                @Override
                                public void onSuccess() throws RemoteException {
                                    // Start background service plug-ins
                                    if (plug.isBackgroundService()) {
                                        ContextMgr.startPlugin(plug, false);
                                    }
                                }

                                @Override
                                public void onFailure(String message, int errorCode) throws RemoteException {
                                    // Plug-in failed to init, don't call start
                                }
                            });
                } else {
                    Log.w(TAG, "initializeInstalledPlugins could not start bundle for: " + plug);
                }
            } else {
                Log.w(TAG, "initializeInstalledPlugins found that a plug-in was not installed: " + plug);
            }
        }
    }

    /**
     * Stops the Dynamix Framework, releasing its acquired resources and killing the service if requested.
     *
     * @param destroy        True if Dynamix should be destroyed; false otherwise.
     * @param killProcess    True if Dynamix process should be killed; false otherwise.
     * @param restartProcess True if Dynamix process should be restarted after being killed; false otherwise.
     */
    private synchronized void doStopFramework(final boolean destroy, final boolean killProcess,
                                              final boolean restartProcess) {
        // Stop our restart alarm
        stopRestartAlarm();
        // Handle states
        if (startState == StartState.STOPPED) {
            Log.i(TAG, "Already stopped.");
            if (destroy)
                doDestroyFramework(killProcess, restartProcess);
        } else if (startState == StartState.STOPPING)
            Log.i(TAG, "Already stopping... please wait");
        else {
            Log.i(TAG, "Stopping the Dynamix Framework");
            // Set stopping state
            synchronized (startState) {
                startState = StartState.STOPPING;
            }
            // Notify stopping
            onDynamixStopping();
            // Stop update checking
            stopContextPluginUpdateTimer();
            // Unregister our wakeReceiver
            if (wakeReceiver != null) {
                unregisterReceiver(wakeReceiver);
                wakeReceiver = null;
            }
            // Unregister our sleepReceiver
            if (sleepReceiver != null) {
                unregisterReceiver(sleepReceiver);
                sleepReceiver = null;
            }
			/*
			 * Complete stop on a thread...
			 */
            Utils.dispatch(true, new Runnable() {
                @Override
                public void run() {
                    // Clear any session opened callbacks
                    SessionManager.clearSessionOpenedCallbacks();
                    if (!destroy) {
                        // Pause context detection
                        ContextMgr.pauseContextHandling();
                        // Wait for the ContextManager to stop
                        while (!ContextMgr.isPaused()) {
                            try {
                                Log.d(TAG, "Waiting for ContextManager to pause...");
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                            }
                        }
						/*
						 * Stop each OSGi bundle associated to ContextPlugins managed by the ContextManager. TODO: For
						 * now, we leave the bundles running when stopped.
						 */
                        // for (ContextPlugin plug : ContextMgr.getAllContextPlugins()) {
                        // OsgiMgr.stopPluginBundle(plug);
                        // }
                    }
                    // Set stopped state
                    synchronized (startState) {
                        startState = StartState.STOPPED;
                    }
                    // Handle notifications
                    onDynamixStopped();
                    SessionManager.notifyAllDynamixFrameworkInactive();
                    // Finish destroy, if requested
                    if (destroy)
                        doDestroyFramework(killProcess, restartProcess);
                }
            });
        }
    }

    /*
	 * Utility that starts our restart alarm.
	 */
    private void startRestartAlarm(long period) {
        Intent intent = new Intent(getAndroidContext(), BootUpReceiver.class);
        restartIntent = PendingIntent.getBroadcast(getAndroidContext(), 0, intent, 0);
        AlarmManager alarmMgr = (AlarmManager) getAndroidContext().getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 0, period, restartIntent);
    }

    /*
	 * Utility that stops our restart alarm.
	 */
    private void stopRestartAlarm() {
        if (restartIntent != null) {
            AlarmManager mgr = (AlarmManager) getAndroidContext().getSystemService(Context.ALARM_SERVICE);
            mgr.cancel(restartIntent);
        }
    }

    /**
     * Completes the framework destroy process
     *
     * @param killProcess
     * @param restartProcess
     */
    private synchronized void doDestroyFramework(final boolean killProcess, final boolean restartProcess) {
        // Only destroy if we're initialized and not started
        Log.i(TAG, "doDestroyFramework with framework initialized " + isFrameworkInitialized() + " and started "
                + isFrameworkStarted());
        if (isFrameworkInitialized() && !isFrameworkStarted()) {
            Utils.dispatch(true, new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "Destroying Dynamix Framework...");
                    // Remove our foreground handler
                    if (foregroundHandler != null)
                        foregroundHandler.stopForegroundCompat(1, DynamixService.this);
                    // Stop and plug-in discovery operations
                    UpdateManager.cancelContextPluginRepoUpdate();
                    // Close all sessions
                    SessionManager.closeAllSessions(true, new Callback() {
                        @Override
                        public void onSuccess() throws RemoteException {
                            completeDestroy();
                        }

                        @Override
                        public void onFailure(String message, int errorCode) throws RemoteException {
                            completeDestroy();
                        }

                        private void completeDestroy() {
                            Log.i(TAG, "Completing Dynamix Destroy");
                            // Stop context detection
                            ContextMgr.stopContextHandling();
                            // Wait for the ContextManager to stop
                            while (!ContextMgr.isStopped()) {
                                try {
                                    Log.d(TAG, "Waiting for ContextManager to stop...");
                                    Thread.sleep(500);
                                } catch (InterruptedException e) {
                                }
                            }
                            Log.i(TAG, "Context Manager Stopped");
							/*
							 * Stop each OSGi bundle associated to ContextPlugins managed by the ContextManager.
							 */
                            Log.i(TAG, "Stopping OSGi Bundles.");
                            for (ContextPlugin plug : ContextMgr.getAllContextPlugins()) {
                                OsgiMgr.stopPluginBundle(plug);
                            }
                            Log.i(TAG, "Stopping OSGi Framework");
                            // Stop the OSGi manager
                            OsgiMgr.stopFramework();
                            // Handle embedded mode stop
                            if (!embeddedMode) {
								/*
								 * TODO: If clients are still bound, stopSelf will *not* stop the service, even though
								 * we use startService to start Dynamix. This is a problem, since we are unable to stop
								 * properly if we still have bound clients. I'm currently using System.exit below to
								 * completely kill the service; however, Android will restart us, which we don't want.
								 */
                                service.stopSelf();
                            }
                            Log.i(TAG, "Stopping Web Connector");
                            // Stop the WebConnector
                            stopWebConnector();
                            // Wait a bit so that final events can be sent out to clients
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                            }
							/*
							 * Clear the ContextManager's InternalFacadeBinders. Note that the InternalFacadeBinders
							 * *must* be maintained until this point, otherwise they may not hear the session closed
							 * notifications.
							 */
                            Log.i(TAG, "Finalizing Dynamix State");
                            ContextMgr.clearInternalFacadeBinders();
                            // Kill the remote listeners
                            SessionManager.killRemoteListeners();
                            // Set clean exit
                            if (!recoveryMode)
                                SettingsManager.setCleanExit(true);
                            // Close the database
                            SettingsManager.closeDatabase();
                            // Set not booted
                            synchronized (bootState) {
                                bootState = BootState.NOT_BOOTED;
                            }
                            Log.i(TAG, "Destroyed the Dynamix Framework");
                            // Handle kill process request
                            if (killProcess) {
                                killDynamixProcess(restartProcess);
                            }
                        }
                    });
                }
            });
        } else
            Log.w(TAG, "Cannot destroy framework while in state " + startState);
    }

    /**
     * Kills the Dynamix process, restarting it if requested.
     */
    protected static void killDynamixProcess(boolean restart) {
        // Setup a restart, if requested
        if (restart) {
            AlarmManager mgr2 = (AlarmManager) androidContext
                    .getSystemService(Context.ALARM_SERVICE);
            mgr2.set(AlarmManager.RTC, System.currentTimeMillis() + 3000, PendingIntent
                    .getActivity(androidContext, 0, new Intent(androidContext,
                            HomeActivity.class), 0));
        }
        // Exit our process
        System.exit(0);
    }

    /**
     * Refreshing the list of Dynamix plug-ins using the available repos.
     */
    public static void refreshPluginsFromRepos() {
        checkForUpdates(true);
    }

    /**
     * Adds the plug-in repository to the set of repositories, replacing an existing repo if it was already persisted.
     */
    public static boolean addContextPluginRepo(Repository repo) {
        return SettingsManager.addPluginRepository(repo);
    }

    /**
     * Removes the plug-in repository from the set of repositories.
     */
    public static boolean removeContextPluginRepo(Repository repo) {
        return SettingsManager.removePluginRepository(repo);
    }

    /**
     * Updates the plug-in repository in the database with the incoming values. Note that the repo must have the id of
     * the repo in the database to update.
     */
    public static boolean updateContextPluginRepo(Repository repo) {
        return SettingsManager.updatePluginRepository(repo);
    }

    /**
     * Returns the repository specified by the repo url, or null if no mapping could be found.
     */
    public static Repository getPluginRepository(String url) {
        return SettingsManager.getPluginRepository(url);
    }

    /**
     * Returns the list of available RepositoryInf(s), which are used for discovering new plug-ins and plug-in updates.
     */
    public static List<RepositoryInfo> getRepositoryInfo() {
        List<RepositoryInfo> returnList = new ArrayList<RepositoryInfo>();
        for (Repository repo : getContextPluginRepos())
            returnList.add(repo.getRepositoryInfo());
        return returnList;
    }

    /**
     * Returns the list of available Repository(s), which are used for discovering new plug-ins and plug-in updates.
     */
    public static List<Repository> getContextPluginRepos() {
        return SettingsManager.getPluginRepositories();
        // TODO: Dynamix Nexus Repo test (remove)
        // RepositoryInfo repo = new RepositoryInfo(
        // "Dynamix Core Context Plug-in Repo",
        // "http://repo1.ambientdynamix.org:8081/nexus/service/local/data_index?g=org.ambientdynamix.contextplugins&repositoryId=dynamix-core-contextplugins",
        // ContextPluginConnectorFactory.NEXUS_INDEX_SOURCE);
        // RepositoryInfo repo = new RepositoryInfo(
        // "Dynamix Core Context Plug-in Repo",
        // "http://repo1.ambientdynamix.org:8081/nexus/service/local/lucene/search?g=org.ambientdynamix.contextplugins&repositoryId=dynamix-core-contextplugins",
        // ContextPluginConnectorFactory.NEXUS_LUCENE_SOURCE);
        // try {
        // sources.add(ContextPluginConnectorFactory.makeContextPluginConnector(repo));
        // } catch (Exception e) {
        // Log.e(TAG, "Could not make repository using: " + repo);
        // }
        // return sources;
    }

    /*
	 * Boot state constants
	 */
    private static enum BootState {
        NOT_BOOTED, BOOTING, BOOTED
    }

    /*
	 * Private internal class for handling checkForContextPluginUpdates()
	 */
    private static class DynamixUpdatesCallbackHandler implements IDynamixUpdateListener {
        @Override
        public void onUpdateStarted() {
            // TODO Auto-generated method stub
        }

        @Override
        public void onUpdateCancelled() {
            // TODO Auto-generated method stub
        }

        @Override
        public void onUpdateError(String message) {
			/*
			 * There was an error during the update, but make sure to refresh the plug-ins anyway.
			 */
            refreshPlugins();
        }

        @Override
        public void onUpdateComplete(DynamixUpdates updates) {
            Log.i(TAG, "Received Dynamix Updates....");
            // Check for Dynamix updates
            DynamixPreferences.setDynamixUpdateAvailable(updates);
            if (DynamixPreferences.isDynamixUpdateAvailable()) {
                if (DynamixPreferences.isDynamixUpdateRequired()) {
                    if (HomeActivity.activity != null) {
						/*
						 * Handle required Dynamix updates
						 */
                        uiHandle.post(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                Utils.showUpdateAlert(HomeActivity.activity, true,
                                        DynamixPreferences.getDynamixUpdateMessage(),
                                        DynamixPreferences.getDynamixUpdateUrl());
                            }
                        });
                        return;
                    } else {
                        System.exit(0);
                        return;
                    }
                }
            }
            // Check if our trusted certs are up to date
            List<TrustedCert> currentCerts = getAuthorizedCertsFromKeyStore();
            List<TrustedCert> removeCerts = new ArrayList<TrustedCert>();
            List<TrustedCertBinder> addCerts = new ArrayList<TrustedCertBinder>();
            // Create our lists of new certs to add
            if (updates != null) {
                for (TrustedCertBinder cert : updates.getTrustedWebConnectorCerts()) {
                    TrustedCert foundCert = null;
                    for (TrustedCert currentCert : currentCerts) {
                        if (cert.getFingerprint().equalsIgnoreCase(currentCert.getFingerprint())) {
                            foundCert = currentCert;
                            break;
                        }
                    }
                    if (foundCert != null) {
                        if (cert.remove()) {
                            Log.i(TAG, "Found cert to remove: " + foundCert.getAlias());
                            removeCerts.add(foundCert);
                        } else {
                            // Do nothing, since we've already stored this cert
                        }
                    } else {
                        if (!cert.remove()) {
                            Log.i(TAG, "Found new cert to add: " + cert.getAlias());
                            addCerts.add(cert);
                        } else {
                            // Do nothing, since we haven't stored this bad cert
                        }
                    }
                }
                // Remove certs
                for (TrustedCert remove : removeCerts) {
                    try {
                        removeAuthorizedCert(remove.getAlias());
                    } catch (Exception e) {
                        Log.w(TAG, "Could not remove " + remove.getAlias() + ": " + e);
                    }
                }
                // Add new certs
                for (TrustedCertBinder add : addCerts) {
                    try {
                        storeAuthorizedCert(add.getAlias(), Utils.downloadCertificate(add.getUrl()));
                    } catch (Exception e) {
                        Log.w(TAG, "Could not store certificate: " + e);
                    }
                }
                // Handle incoming repos
                for (Repository repo : updates.getTrustedRepos()) {
                    if (repo.forceRemove()) {
                        repo.setRemovable(true);
                        DynamixService.removeContextPluginRepo(repo);
                    } else {
                        DynamixService.addContextPluginRepo(repo);
                    }
                }
            }
            refreshPlugins();
        }

        private void refreshPlugins() {
            // Refresh plug-ins from repos
            UpdateManager.refreshContextPluginsFromRepos(getAndroidContext(), getContextPluginRepos(),
                    PluginConstants.PLATFORM.ANDROID, Utils.getAndroidVersionInfo(), getDynamixFrameworkVersion(),
                    new ContextPluginCallbackHandler(null), androidContext.getPackageManager()
                            .getSystemAvailableFeatures(), true);
        }
    }

    /*
	 * Private internal class for handling checkForContextPluginUpdates()
	 */
    private static class ContextPluginCallbackHandler implements IContextPluginUpdateListener {
        IContextPluginUpdateListener callback;

        public ContextPluginCallbackHandler(IContextPluginUpdateListener callback) {
            this.callback = callback;
        }

        @Override
        public void onUpdateCancelled() {
            if (callback != null)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        callback.onUpdateCancelled();
                    }
                });
            // Notify
            onPluginDiscoveryCompleted();
        }

        @Override
        public void onUpdateComplete(List<PendingContextPlugin> incomingUpdates, final Map<Repository, String> errors) {
            List<PendingContextPlugin> existingUpdates = SettingsManager.getPendingContextPlugins();
            final List<PendingContextPlugin> finalUpdates = new ArrayList<PendingContextPlugin>();
            // Maintain existing updates for PluginSource's with errors
            for (PendingContextPlugin existingUpdate : existingUpdates) {
                for (Repository errorSource : errors.keySet()) {
                    if (errorSource.equals(existingUpdate.getPendingContextPlugin().getRepoSource()))
                        finalUpdates.add(existingUpdate);
                    break;
                }
            }
            List<ContextPluginInformation> discoveredPluginsForClients = new ArrayList<ContextPluginInformation>();
            for (PendingContextPlugin result : incomingUpdates) {
                finalUpdates.add(result);
                discoveredPluginsForClients.add(result.getPendingContextPlugin().getContextPluginInformation());
            }
            // Update the SettingsManager
            SettingsManager.setPendingContextPlugins(finalUpdates);
            Log.d(TAG, "checkForContextPluginUpdates is completed with total updates: " + finalUpdates.size());
            // Notify callback
            if (callback != null)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        callback.onUpdateComplete(finalUpdates, errors);
                    }
                });
            // Notify framework listeners
            onPluginDiscoveryCompleted();
            // Notify session listeners
            SessionManager.notifyAllContextPluginDiscoveryFinished();
        }

        @Override
        public void onUpdateStarted() {
            if (callback != null)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        callback.onUpdateStarted();
                    }
                });
            onPluginDiscoveryStarted();
        }

        @Override
        public void onUpdateError(final String message) {
            Log.w(TAG, "onUpdateError: " + message);
            if (callback != null)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        onPluginDiscoveryError(message);
                    }
                });
        }
    }

    /**
     * Sends onDynamixInitializing to all registered framework listeners.
     */
    public static void onDynamixInitializing() {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixInitializing();
                    }
                });
        }
    }

    /**
     * Sends onDynamixInitialized to all registered framework listeners.
     */
    public static void onDynamixInitialized(final DynamixService service) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixInitialized(service);
                    }
                });
        }
    }

    /**
     * Sends onDynamixInitializingError to all registered framework listeners.
     */
    public static void onDynamixInitializingError(final String message) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixInitializingError(message);
                    }
                });
        }
    }

    /**
     * Sends onDynamixStarting to all registered framework listeners.
     */
    public static void onDynamixStarting() {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixStarting();
                    }
                });
        }
    }

    /**
     * Sends onDynamixStarted to all registered framework listeners.
     */
    public static void onDynamixStarted() {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixStarted();
                    }
                });
        }
    }

    /**
     * Sends onDynamixStopping to all registered framework listeners.
     */
    public static void onDynamixStopping() {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixStopping();
                    }
                });
        }
    }

    /**
     * Sends onDynamixStopped to all registered framework listeners.
     */
    public static void onDynamixStopped() {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixStopped();
                    }
                });
        }
    }

    /**
     * Sends onDynamixApplicationAdded to all registered framework listeners.
     */
    public static void onDynamixApplicationAdded(final DynamixApplication app) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixApplicationAdded(app);
                    }
                });
        }
    }

    /**
     * Sends onDynamixApplicationRemoved to all registered framework listeners.
     */
    public static void onDynamixApplicationRemoved(final DynamixApplication app) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixApplicationRemoved(app);
                    }
                });
        }
    }

    /**
     * Sends onDynamixApplicationConnected to all registered framework listeners.
     */
    public static void onDynamixApplicationConnected(final DynamixApplication app) {
        synchronized (frameworkListeners) {
        }
        for (final IDynamixFrameworkListener listener : frameworkListeners)
            Utils.dispatch(true, new Runnable() {
                @Override
                public void run() {
                    listener.onDynamixApplicationConnected(app);
                }
            });
    }

    /**
     * Sends onDynamixApplicationDisconnected to all registered framework listeners.
     */
    public static void onDynamixApplicationDisconnected(final DynamixApplication app) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixApplicationDisconnected(app);
                    }
                });
        }
    }

    /**
     * Sends onDynamixError to all registered framework listeners.
     */
    public static void onDynamixError(final String message) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onDynamixError(message);
                    }
                });
        }
    }

    /**
     * Sends onPluginDiscoveryStarted to all registered framework listeners.
     */
    public static void onPluginDiscoveryStarted() {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onPluginDiscoveryStarted();
                    }
                });
        }
    }

    /**
     * Sends onPluginDiscoveryError to all registered framework listeners.
     */
    public static void onPluginDiscoveryError(final String message) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onPluginDiscoveryError(message);
                    }
                });
        }
    }

    /**
     * Sends onPluginDiscoveryCompleted to all registered framework listeners.
     */
    public static void onPluginDiscoveryCompleted() {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onPluginDiscoveryCompleted();
                    }
                });
        }
    }

    /**
     * Sends onPluginStopTimeout to all registered framework listeners.
     */
    public static void notifyOnPluginStopTimeout() {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onPluginStopTimeout();
                    }
                });
        }
    }

    /**
     * Sends onPluginInstalled to all registered framework listeners.
     */
    public static void notifyOnPluginInstalled(final ContextPluginInformation plug) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onPluginInstalled(plug);
                    }
                });
        }
    }

    /**
     * Sends onPluginUninstalled to all registered framework listeners.
     */
    public static void notifyOnPluginUninstalled(final ContextPluginInformation plug) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onPluginUninstalled(plug);
                    }
                });
        }
    }

    /**
     * Sends onPluginUpdated to all registered framework listeners.
     */
    public static void notifyOnPluginUpdated(final ContextPluginInformation plug) {
        synchronized (frameworkListeners) {
            for (final IDynamixFrameworkListener listener : frameworkListeners)
                Utils.dispatch(true, new Runnable() {
                    @Override
                    public void run() {
                        listener.onPluginUpdated(plug);
                    }
                });
        }
    }
}