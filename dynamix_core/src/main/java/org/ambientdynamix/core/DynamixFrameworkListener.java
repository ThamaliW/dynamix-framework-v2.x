/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import org.ambientdynamix.api.application.ContextPluginInformation;

/**
 * Base implementation of IDynamixFrameworkListener with no functionality. Override methods as needed.
 * 
 * @see IDynamixFrameworkListener
 * @author Darren Carlson
 *
 */
public class DynamixFrameworkListener implements IDynamixFrameworkListener {
	@Override
	public void onDynamixInitializing() {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixInitializingError(String message) {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixInitialized(DynamixService dynamix) {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixStarting() {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixStarted() {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixStopping() {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixStopped() {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixApplicationAdded(DynamixApplication app) {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixApplicationRemoved(DynamixApplication app) {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixApplicationConnected(DynamixApplication app) {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixApplicationDisconnected(DynamixApplication app) {
		// No functionality in case implementation
	}

	@Override
	public void onDynamixError(String message) {
		// No functionality in case implementation
	}

	@Override
	public void onPluginDiscoveryStarted() {
		// No functionality in case implementation
	}

	@Override
	public void onPluginDiscoveryCompleted() {
		// No functionality in case implementation
	}

	@Override
	public void onPluginDiscoveryError(String message) {
		// No functionality in case implementation
	}

	@Override
	public void onPluginStopTimeout() {
		// No functionality in case implementation
	}

	@Override
	public void onPluginInstalled(ContextPluginInformation plug) {
		// No functionality in case implementation
	}

	@Override
	public void onPluginUninstalled(ContextPluginInformation plug) {
		// No functionality in case implementation
	}

	@Override
	public void onPluginUpdated(ContextPluginInformation plug) {
		// No functionality in case implementation
	}
}
