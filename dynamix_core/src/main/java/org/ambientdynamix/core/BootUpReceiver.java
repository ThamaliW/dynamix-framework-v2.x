/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * BootUpReceiver provides the logic to auto-start Dynamix at the completion of the Android boot process (if requested).
 * BootUpReceiver is registered in AndroidManifest.xml to receive 'android.intent.action.BOOT_COMPLETED' events.
 * 
 * @author Darren Carlson
 */
public class BootUpReceiver extends BroadcastReceiver {
	private final String TAG = this.getClass().getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		// Set the Context so we can read preferences
		DynamixPreferences.setContext(context);
		// Start if Dynamix is enabled, auto-start is true and the DynamixService is not running
		boolean enabled = DynamixPreferences.isDynamixEnabled();
		boolean autoStart = DynamixPreferences.autoStartDynamix();
		boolean started = DynamixService.isFrameworkStarted();
		if (enabled && autoStart && !started) {
			if (intent.getAction() == Intent.ACTION_BOOT_COMPLETED) {
				Log.i(TAG, "Launching Dynamix after device boot!");
			} else {
				Log.i(TAG, "Launching Dynamix from killed state!");
			}
			// Use the incoming context to start the Dynamix service
			context.startService(new Intent(context, DynamixService.class));
		} else {
			if (intent.getAction() == Intent.ACTION_BOOT_COMPLETED) {
				Log.i(TAG, "BootUpReceiver is NOT launching Dynamix. Enabled " + enabled + " autoStart " + autoStart
						+ " started " + started);
			} else {
				if (started) {
					// Do nothing, since we're already started
				} else {
					// Warn about wrong state
					Log.w(TAG, "The Dynamix restart alarm was running in state: Enabled " + enabled + " autoStart "
							+ autoStart + " started " + started);
				}
			}
		}
	}
}