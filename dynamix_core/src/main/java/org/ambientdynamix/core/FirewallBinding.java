/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Represents a binding between an application and one (or more) pending context support requests with associated
 * firewall rules. This is needed since the context firewall must be able to handle multiple context support requests
 * from a single app arriving at different times. For example, an application adding support for multiple context types
 * when it starts. These requests must be aggregated and presented to the user in a single context firewall popup rather
 * than opening the popup for every single context request, which would get annoying in a hurry.
 * 
 * @author Darren Carlson
 *
 */
public class FirewallBinding {
	private UUID requestId;
	private DynamixApplication app;
	private Map<PendingContextSupportRequest, List<FirewallRule>> firewallRuleMap = new HashMap<PendingContextSupportRequest, List<FirewallRule>>();

	/**
	 * Creates a FirewallBinding.
	 */
	public FirewallBinding(DynamixApplication app, PendingContextSupportRequest pendingSupport,
			List<FirewallRule> requiredRules) {
		this.app = app;
		firewallRuleMap.put(pendingSupport, requiredRules);
	}

	/**
	 * Returns ths app associated with this firewall binding.
	 */
	public DynamixApplication getApp() {
		return app;
	}

	/**
	 * Adds the pending context support request and associated firewall rules.
	 */
	public void addPendingRequest(PendingContextSupportRequest pending, List<FirewallRule> rules) {
		synchronized (firewallRuleMap) {
			firewallRuleMap.put(pending, rules);
		}
	}

	/**
	 * Returns the set of pending context support requests.
	 */
	public Set<PendingContextSupportRequest> getPendingContextSupportRequests() {
		return firewallRuleMap.keySet();
	}

	/**
	 * Returns true if this firewall binding already has the incoming list of firewall rules associated with any
	 * registered pending context support requests; false otherwise. This is useful to know since we only want to
	 * present a particular configuration of context firewall rules once to the user in the popup.
	 */
	public boolean hasFirewallRules(List<FirewallRule> rules) {
		synchronized (firewallRuleMap) {
			for (List<FirewallRule> existing : firewallRuleMap.values()) {
				if (existing.containsAll(rules))
					return true;
			}
			return false;
		}
	}

	/**
	 * Returns the set of all firewall rules from the pending context support requests registered to this binding. This
	 * method returns each unique firewall rule once, regardless if it was registered with multiple pending context
	 * support requests.
	 * 
	 * @return
	 */
	public Set<FirewallRule> createRuleSet() {
		synchronized (firewallRuleMap) {
			Set<FirewallRule> set = new LinkedHashSet<FirewallRule>();
			for (List<FirewallRule> existing : firewallRuleMap.values()) {
				for (FirewallRule rule : existing)
					if (!set.contains(rule))
						set.add(rule);
			}
			return set;
		}
	}

	public void setRequestId(UUID requestId) {
		this.requestId = requestId;
	}

	public UUID getRequestId() {
		return requestId;
	}

	@Override
	public String toString() {
		return "FirewallBinding for " + app;
	}
}