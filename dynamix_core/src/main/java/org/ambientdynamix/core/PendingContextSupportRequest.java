/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.IContextSupportCallback;
import org.ambientdynamix.api.application.VersionedPlugin;

import android.os.Bundle;

/**
 * Represents a pending context support request.
 * 
 * @author Darren Carlson
 *
 */
public class PendingContextSupportRequest {
	private UUID requestId = UUID.randomUUID();
	private IContextListener listener;
	private List<ContextPluginInformation> plugs;
	private DynamixApplication app;
	private DynamixSession session;
	private String contextType;
	private boolean forcePluginInstall;
	private Bundle config;
	private IContextSupportCallback callback;
	private ContextHandlerImpl handler;

	public PendingContextSupportRequest(DynamixApplication app, DynamixSession session, IContextListener listener,
			List<ContextPluginInformation> allRequestedPlugins, String contextType, boolean forcePluginInstall,
			Bundle config, IContextSupportCallback callback, ContextHandlerImpl handler) {
		this.app = app;
		this.session = session;
		this.listener = listener;
		this.plugs = allRequestedPlugins;
		this.contextType = contextType;
		this.forcePluginInstall = forcePluginInstall;
		this.config = config;
		this.callback = callback;
		this.handler = handler;
	}

	public DynamixApplication getApp() {
		return app;
	}

	public UUID getRequestId() {
		return requestId;
	}

	public IContextListener getListener() {
		return listener;
	}

	public List<ContextPluginInformation> getAllRequestedPlugins() {
		return plugs;
	}

	public List<VersionedPlugin> getAllRequestedPluginIds() {
		List<VersionedPlugin> ids = new ArrayList<VersionedPlugin>();
		for (ContextPluginInformation plug : getAllRequestedPlugins()) {	
			// Access the full plug-in from Dynamix (grabs correct version info)
			ContextPluginInformation plugInfo = DynamixService.getContextPluginInfo(plug.getPluginId(), plug.getVersion());
			ids.add(new VersionedPlugin(plugInfo.getPluginId(), plugInfo.getVersion()));
		}
		return ids;
	}

	public String getContextType() {
		return contextType;
	}

	public boolean isForcePluginInstall() {
		return forcePluginInstall;
	}

	public Bundle getConfig() {
		return config;
	}

	public IContextSupportCallback getCallback() {
		return callback;
	}

	public ContextHandlerImpl getHandler() {
		return handler;
	}

	public DynamixSession getSession() {
		return session;
	}
}
