/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.Utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Models a relationship between an app and a requested set of firewall rules. Firewall rules include a context type, a
 * set of associated plug-ins and an access level (e.g., ALLOWED_SESSION). The FirewallRequest is the object is sent to
 * the firewall popup
 * 
 * @author Darren Carlson
 *
 */
public class FirewallRequest implements Parcelable {
	private UUID requestId;
	private DynamixApplication app;
	private List<FirewallRule> firewallRules = new ArrayList<FirewallRule>();
	private boolean cancelled = false;
	/**
	 * Static Parcelable Creator required to reconstruct a the object from an incoming Parcel
	 */
	public static final Parcelable.Creator<FirewallRequest> CREATOR = new Parcelable.Creator<FirewallRequest>() {
		@Override
		public FirewallRequest createFromParcel(Parcel in) {
			return new FirewallRequest(in);
		}

		@Override
		public FirewallRequest[] newArray(int size) {
			return new FirewallRequest[size];
		}
	};

	public FirewallRequest(DynamixApplication app, FirewallRule firewallRule) {
		List<FirewallRule> firewallRules = new ArrayList<FirewallRule>();
		firewallRules.add(firewallRule);
		this.app = app;
		this.firewallRules = firewallRules;
		requestId = UUID.randomUUID();
	}

	public FirewallRequest(DynamixApplication app, List<FirewallRule> firewallRules) {
		this.app = app;
		this.firewallRules = firewallRules;
		requestId = UUID.randomUUID();
	}

	public DynamixApplication getApp() {
		return app;
	}

	public List<FirewallRule> getFirewallRules() {
		return firewallRules;
	}

	public FirewallRule getFirewallRule(String contextType, List<ContextPluginInformation> plugins) {
		synchronized (firewallRules) {
			for (FirewallRule rule : getFirewallRules()) {
				if (rule.getContextTypeId().equalsIgnoreCase(contextType))
					if (rule.getPlugins().containsAll(plugins))
						return rule;
			}
		}
		return null;
	}

	public void addFirewallRule(FirewallRule rule) {
		synchronized (firewallRules) {
			if (!firewallRules.contains(rule))
				firewallRules.add(rule);
		}
	}

	public void addFirewallRules(List<FirewallRule> newRules) {
		synchronized (firewallRules) {
			List<FirewallRule> addRules = new ArrayList<FirewallRule>();
			for (FirewallRule checkRule : newRules) {
				if (!firewallRules.contains(checkRule))
					addRules.add(checkRule);
			}
			firewallRules.addAll(addRules);
		}
	}

	public void setFirewallRules(List<FirewallRule> firewallRules) {
		synchronized (firewallRules) {
			this.firewallRules = firewallRules;
		}
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public boolean isCancelled() {
		return this.cancelled;
	}

	public UUID getRequestId() {
		return this.requestId;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public boolean equals(Object candidate) {
		// First determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Ok, they are the same class... check if their id's are the same
		FirewallRequest other = (FirewallRequest) candidate;
		// return this.app.equals(other.app) && this.contextType.equalsIgnoreCase(other.contextType);
		return this.requestId.equals(other.requestId);
	}

	// HashCode Example: http://www.javafaq.nu/java-example-code-175.html
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.requestId.hashCode();
		return result;
	}

	private FirewallRequest(Parcel in) {
		this.app = in.readParcelable(getClass().getClassLoader());
		this.requestId = (UUID) in.readSerializable();
		in.readList(firewallRules, this.getClass().getClassLoader());
		this.cancelled = Utils.readBoolean(in);
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeParcelable(app, 0);
		out.writeSerializable(requestId);
		out.writeList(firewallRules);
		Utils.writeBoolean(out, cancelled);
	}
}
