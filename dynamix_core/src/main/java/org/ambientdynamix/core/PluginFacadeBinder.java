/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.core.DynamixApplication.APP_TYPE;

import android.content.Context;
import android.util.Log;

/**
 * Provides an implementation of the IDynamixFacade API that can be used by Dynamix plug-ins.
 * 
 * 
 * @see IDynamixFacade
 * @author Darren Carlson
 */
public class PluginFacadeBinder extends AppFacadeBinder {
	private String TAG = this.getClass().getSimpleName();
	private ContextPlugin plug;
	private String appId;

	/**
	 * Creates an InternalFacadeBinder.
	 */
	public PluginFacadeBinder(ContextPlugin plug, Context context, ContextManager conMgr, boolean embeddedMode) {
		super(context, conMgr, embeddedMode, false);
		this.plug = plug;
		this.appId = Utils.makeAppIdForPlugin(plug);
		/*
		 * We need to add the framework listener AFTER the super constructor above, since we need an appId before
		 * calling 'DynamixService.addDynamixFrameworkListener', which uses our 'equals', which requires an appId. Whew!
		 */
		DynamixService.addDynamixFrameworkListener(super.frameworkListener);
		Log.d(TAG, "Created PluginFacadeBinder for: " + appId);
	}

	/**
	 * Returns a generated application id for this binder.
	 */
	public String getAppId() {
		return this.appId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getCallerId() {
		Log.d(TAG, "Returning caller ID: " + appId);
		return appId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DynamixApplication createNewApplicationFromCaller(String id, boolean admin) {
		// Construct a new application for the caller
		DynamixApplication app = new DynamixApplication(APP_TYPE.PLUG_IN, id, plug.getName());
		app.setAppDescription(plug.getDescription());
		app.setAdmin(DynamixPreferences.isAutoAdminPluginAppsEnabled());
		if (plug.getVersion() != null)
			app.addAppDetail(DynamixApplication.VERSION_CODE, plug.getVersion().toString());
		else
			app.addAppDetail(DynamixApplication.VERSION_CODE, new VersionInfo(0, 0, 0).toString());
		return app;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// first determine if they are the same object reference
		if (this == candidate)
			return true;
		// make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		PluginFacadeBinder other = (PluginFacadeBinder) candidate;
		return this.appId.equalsIgnoreCase(other.appId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.appId.hashCode();
		return result;
	}
}