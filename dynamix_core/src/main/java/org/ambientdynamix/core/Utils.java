/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.IContextInfo;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.ContextPluginDependency;
import org.ambientdynamix.core.DynamixApplication.APP_TYPE;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;
import org.apache.commons.validator.routines.UrlValidator;
import org.dynamicjava.api_bridge.ApiBridge;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.net.Authenticator;
import java.net.CookieHandler;
import java.net.ResponseCache;
import java.net.URL;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Static utility methods for the Dynamix Framework.
 *
 * @author Darren Carlson
 */
public class Utils {
    // Private data
    private final static String TAG = Utils.class.getSimpleName();
    private static HostnameVerifier defaultHostnameVerifier;
    private static SSLSocketFactory defaultSSLSocketFactory;
    private static TrustManager[] defaultTrustManagers;
    private static SSLContext sc;
    private static UrlValidator urlValidator = new UrlValidator();
    private static boolean showingUpdateAlert = false;
    private static Context context;

    private static ThreadFactory threadFactory = new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setDaemon(true);
            t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable e) {
                    Log.w(TAG, "Uncaught exception during dispatch: " + e.toString());
                    e.printStackTrace();
                }
            });
            return t;
        }
    };

    private static ThreadPoolExecutor executer = new ThreadPoolExecutor(25, 100, 1, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>(), threadFactory);


    // Singleton constructor
    private Utils() {
    }

    /**
     * Sets the Context.
     */
    public static void setContext(Context c) {
        context = c;
    }

    /**
     * Returns the local ip address.
     */
    public static String getLocalIpAddress() {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connManager != null) {
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWifi.isConnected()) {
                WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInf = wifiMan.getConnectionInfo();
                int ipAddress = wifiInf.getIpAddress();
                String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
                        (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
                // Log.d(getClass().getSimpleName(), "Found ip address : " + ip);
                return ip;
            }
        }
        return "0.0.0.0";
    }

    /**
     * Returns the private data directory for the given plug-in.
     *
     * @param plug The plug-in to check.
     * @return The data directory for the plug-in.
     */
    public static File getPluginPrivateDataDir(ContextPlugin plug) {
        ContextWrapper cWrap = new ContextWrapper(DynamixService.getAndroidContext());
        String privateFilesPath = cWrap.getFilesDir().getAbsolutePath() + "/felix/felix-cache/bundle"
                + plug.getBundleId() + "/data/";
        // Make sure the directory is created
        File dir = new File(privateFilesPath);
        if(!dir.exists())
            dir.mkdir();
        return dir;
    }

    /**
     * Returns true if the uri is valid; false otherwise.
     */
    public static boolean isUriValid(String uri) {
        return urlValidator.isValid(uri);
    }

    /**
     * Returns true if the targetPlugins has dependent plug-ins; false otherwise.
     */
    public static boolean hasDependentPlugins(ContextPlugin targetPlug, boolean checkOnlyInstalled) {
        return getDependentPlugins(targetPlug, checkOnlyInstalled).size() > 0;
    }

    /**
     * Returns true if the targetPlugins has required plug-ins; false otherwise.
     */
    public static boolean hasRequiredPlugins(ContextPlugin targetPlug, boolean checkOnlyInstalled) {
        return getRequiredPlugins(targetPlug, checkOnlyInstalled).size() > 0;
    }

    /**
     * Returns all plug-ins that are dependent on the targetPlugin.
     *
     * @param checkOnlyInstalled If true, this method checks dependencies against only installed plug-ins; false will compare against
     *                           all known plug-ins.
     */
    public static List<ContextPlugin> getDependentPlugins(ContextPlugin targetPlug, boolean checkOnlyInstalled) {
        if (targetPlug != null) {
            if (checkOnlyInstalled)
                return Utils.doGetDependentPlugins(targetPlug, DynamixService.getInstalledContextPluginsFromDatabase());
            else
                return Utils.doGetDependentPlugins(targetPlug, DynamixService.getAllContextPlugins());
        } else {
            Log.w(TAG, "getDependentPlugins called with null target plug-in");
            return new ArrayList<ContextPlugin>();
        }
    }

    /**
     * Returns all plug-ins that are required by the targetPlugin.
     *
     * @param checkOnlyInstalled If true, this method checks requirements against only installed plug-ins; false will compare against
     *                           all known plug-ins.
     */
    public static List<ContextPlugin> getRequiredPlugins(ContextPlugin targetPlug, boolean checkOnlyInstalled) {
        if (checkOnlyInstalled)
            return Utils.doGetRequiredPlugins(targetPlug, DynamixService.getInstalledContextPluginsFromDatabase());
        else
            return Utils.doGetRequiredPlugins(targetPlug, DynamixService.getAllContextPlugins());
    }

    /**
     * Returns all plug-ins from the pluginList that are dependent on the targetPlugin. Recursively scans all dependent
     * plug-ins to return the entire plug-in graph.
     */
    public static List<ContextPlugin> doGetDependentPlugins(ContextPlugin targetPlug, List<ContextPlugin> pluginList) {
        // Log.i(TAG, "Scan: Finding dependencies for " + targetPlug.getName());
        List<ContextPlugin> returnList = new ArrayList<ContextPlugin>();
        List<ContextPlugin> modifiableList = new ArrayList<ContextPlugin>(pluginList);
        // Remove the targetPlug from the pluginList
        modifiableList.remove(targetPlug);
        for (ContextPlugin checkPlug : modifiableList) {
            // Log.i(TAG, "Scan: Checking if " + checkPlug.getName() + " depends on " + targetPlug.getName());
            // Create temporary ContextPluginDependency entities for list comparison
            ContextPluginDependency latest = new ContextPluginDependency(targetPlug.getId(), null);
            ContextPluginDependency specific = new ContextPluginDependency(targetPlug.getId(), targetPlug.getVersion());
            if (checkPlug.getPluginDependencies().contains(latest)
                    || checkPlug.getPluginDependencies().contains(specific)) {
                // Add the managed plug-in to the return list
                if (!returnList.contains(checkPlug)) {
                    // Log.i(TAG, "Scan: " + checkPlug.getName() + " DOES depend on " + targetPlug.getName());
                    returnList.add(checkPlug);
                } else {
                    // Log.i(TAG,
                    // "Scan: Already discovered that " + checkPlug.getName() + " DOES depend on "
                    // + targetPlug.getName());
                }
                // Recurse into the managed plug's dependencies, too
                for (ContextPlugin subPlug : doGetDependentPlugins(checkPlug, modifiableList)) {
                    if (!returnList.contains(subPlug)) {
                        // Log.i(TAG, "Scan: " + subPlug.getName() + " DOES depend on " + targetPlug.getName());
                        returnList.add(subPlug);
                    } else {
                        // Log.i(TAG, "Scan: Already discovered that " + subPlug.getName() + " DOES depend on "
                        // + targetPlug.getName());
                    }
                }
            }
        }
        return returnList;
    }

    /**
     * Returns all plug-ins that are required by the targetPlugin, based on the incoming list of plug-ins
     */
    public static List<ContextPlugin> doGetRequiredPlugins(ContextPlugin targetPlug, List<ContextPlugin> pluginList) {
        // Log.i(TAG, "Scan: Finding requirements for " + targetPlug.getName());
        List<ContextPlugin> returnList = new ArrayList<ContextPlugin>();
        List<ContextPlugin> modifiableList = new ArrayList<ContextPlugin>(pluginList);
        modifiableList.remove(targetPlug);
        for (ContextPlugin checkPlug : modifiableList) {
            // Log.i(TAG, "Scan: Checking if " + targetPlug.getName() + " requires " + checkPlug.getName());
            // Create a temporary ContextPluginDependency for list comparison
            ContextPluginDependency tmp = new ContextPluginDependency(checkPlug.getId(), checkPlug.getVersion());
            if (targetPlug.getPluginDependencies().contains(tmp)) {
                // Add the managed plug-in to the return list
                if (!returnList.contains(checkPlug)) {
                    // Log.i(TAG, "Scan: " + targetPlug.getName() + " DOES require " + checkPlug.getName());
                    returnList.add(checkPlug);
                } else {
                    // Log.i(TAG,
                    // "Scan: Already discovered that " + checkPlug.getName() + " DOES depend on "
                    // + targetPlug.getName());
                }
                // Recurse into the managed plug's dependencies, too
                for (ContextPlugin subPlug : doGetRequiredPlugins(checkPlug, modifiableList)) {
                    if (!returnList.contains(subPlug)) {
                        // Log.i(TAG, "Scan: " + subPlug.getName() + " DOES depend on " + targetPlug.getName());
                        returnList.add(subPlug);
                    } else {
                        // Log.i(TAG, "Scan: Already discovered that " + subPlug.getName() + " DOES depend on "
                        // + targetPlug.getName());
                    }
                }
            }
        }
        return returnList;
    }

    /**
     * Returns a formatted string that lists all plug-ins that are dependent on the targetPlugin.
     */
    public static String getDependantPluginsString(ContextPlugin targetPlug, boolean checkOnlyInstalled) {
        StringBuilder dependentText = new StringBuilder();
        List<ContextPlugin> dependentPlugs = getDependentPlugins(targetPlug, checkOnlyInstalled);
        if (dependentPlugs.size() > 0) {
            Iterator<ContextPlugin> i = dependentPlugs.iterator();
            while (i.hasNext()) {
                ContextPlugin p = i.next();
                if (i.hasNext())
                    dependentText.append(p.getName() + ", ");
                else
                    dependentText.append(p.getName() + ".");
            }
        }
        return dependentText.toString();
    }

    /**
     * Returns a properly formatted application id for the PLUGIN-based app.
     */
    public static String makeAppIdForPlugin(ContextPlugin plugin) {
        return APP_TYPE.PLUG_IN + "-" + plugin.getId() + "-" + plugin.getVersion();
    }

    /**
     * Returns the SHA1 hash of the input string.
     */
    public static String makeSHA1Hash(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();
        byte[] buffer = input.getBytes();
        md.update(buffer);
        byte[] digest = md.digest();
        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
            hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
        }
        return hexStr;
    }

    /**
     * Returns the calling class as a String.
     */
    public static String getCallerClassName() {
        StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
        String callerClassName = null;
        for (int i = 1; i < stElements.length; i++) {
            StackTraceElement ste = stElements[i];
            if (ste.getClassName().indexOf("java.lang.Thread") != 0) {
                if (callerClassName == null) {
                    callerClassName = ste.getClassName();
                } else if (!callerClassName.equals(ste.getClassName())) {
                    return ste.getClassName();
                }
            }
        }
        return null;
    }

    /**
     * Returns the maximum process ID possible for the hardware platform, as determined by reading the value in
     * '/proc/sys/kernel/pid_max'. If the value cannot be read, a warning is issued and the default max process value of
     * 32768 is returned.
     */
    public static int getMaxProcessId() {
        try {
            // Try to 'cat' the path
            java.lang.Process proc = Runtime.getRuntime().exec(new String[]{"cat", "/proc/sys/kernel/pid_max"});
            // Create a reader for the proc's input stream
            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()), 8192);
            String content = reader.readLine();
            if (content != null && content.length() > 0)
                return Integer.parseInt(content.trim());
            else
                throw new IOException();
        } catch (IOException e) {
            Log.w(TAG, "Could not access /proc/sys/kernel/pid_max. Returning the default value 32768");
            return 32768;
        }
    }

    /**
     * Creates a List of ContextSupportInfo from a List of ContextSupport.
     */
    public static List<ContextSupportInfo> makeContextSupportInfoList(List<ContextSupport> support) {
        List<ContextSupportInfo> list = new ArrayList<ContextSupportInfo>();
        if (support != null)
            for (ContextSupport s : support)
                list.add(s.getContextSupportInfo());
        else
            Log.w(TAG, "Null List<ContextSupport>");
        return list;
    }

    /**
     * Returns true if Dynamix is waiting for WIFI (e.g., downloads over 3G/4G networks is disabled); false otherwise
     * (i.e.,g if Dynamix can access the network).
     */
    public static boolean isWaitingForWifi() {
        if (Looper.myLooper() == null)
            Looper.prepare();
        // If embedded, we're never waiting for wifi
        if (DynamixService.isEmbedded())
            return false;
        else if (DynamixPreferences.useWifiNetworkOnly() && !Utils.isWifiConnected()) {
            Log.w(TAG, "Waiting for WiFi!");
            return true;
        } else
            return false;
    }

    /**
     * Returns the string with leading and trailing white-space removed. If the string is null, null is returned.
     */
    public static String trim(String s) {
        if (s != null)
            return s.trim();
        else
            return null;
    }

    /**
     * Removes newline, carriage return and tab characters from a string.
     *
     * @param toBeEscaped string to escape
     * @return the escaped string
     */
    public static String removeFormattingCharacters(final String toBeEscaped) {
        StringBuffer escapedBuffer = new StringBuffer();
        for (int i = 0; i < toBeEscaped.length(); i++) {
            if ((toBeEscaped.charAt(i) != '\n') && (toBeEscaped.charAt(i) != '\r') && (toBeEscaped.charAt(i) != '\t')) {
                escapedBuffer.append(toBeEscaped.charAt(i));
            }
        }
        String s = escapedBuffer.toString();
        return s;//
        // Strings.replaceSubString(s, "\"", "")
    }

    /**
     * Downloads and creates a X509Certificate from the provided path.
     */
    public static X509Certificate downloadCertificate(String path) throws IOException, CertificateException {
        URL url = new URL(path);
        InputStream is = new BufferedInputStream(url.openStream());
        CertificateFactory certFactory = CertificateFactory.getInstance("X509");
        X509Certificate cert = (X509Certificate) certFactory.generateCertificate(is);
        is.close();
        return cert;
    }

    /**
     * Calculates the SHA-1 fingerprint of the incoming X509Certificate.
     */
    public static String getFingerprint(X509Certificate cert) throws NoSuchAlgorithmException,
            CertificateEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] der = cert.getEncoded();
        md.update(der);
        byte[] digest = md.digest();
        return hexify(digest);
    }

    /**
     * Converts the incoming byte array into a hex String.
     */
    public static String hexify(byte bytes[]) {
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        StringBuffer buf = new StringBuffer(bytes.length * 2);
        for (int i = 0; i < bytes.length; ++i) {
            buf.append(hexDigits[(bytes[i] & 0xf0) >> 4]);
            buf.append(hexDigits[bytes[i] & 0x0f]);
        }
        return buf.toString();
    }

    /**
     * Returns true if the device is connected over WIFI; false otherwise.
     */
    public static boolean isWifiConnected() {
        ConnectivityManager conMan = (ConnectivityManager) DynamixService.getAndroidContext().getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    /**
     * Utility that dispatches the runnable using a daemon thread.
     */
    public static void dispatch(boolean runAsynchronously, final Runnable runnable) {
        dispatch(runAsynchronously, runnable, null);
    }

    /**
     * Utility that dispatches the runnable using a daemon thread. Catches standard and uncaught exceptions.
     */
    public static void dispatch(boolean runAsynchronously, final Runnable runnable, final Runnable exceptionHandler) {
        if (runAsynchronously) {
            if (exceptionHandler != null) {
                Runnable exceptionHandledRunnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            runnable.run();
                        } catch (Exception e) {
                            Log.w(TAG, "Uncaught exception during dispatch: " + e.toString());
                            e.printStackTrace();
                            try {
                                exceptionHandler.run();
                            } catch (Exception ex) {
                                Log.w(TAG, "Exception during exception handling: " + ex.toString());
                                ex.printStackTrace();
                            }
                        }
                    }
                };
                executer.execute(exceptionHandledRunnable);
            } else
                executer.execute(runnable);
        } else {
            try {
                runnable.run();
            } catch (Exception e) {
                Log.w(TAG, "Exception during dispatch: " + e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns the value for the specified argument from the url.
     *
     * @param url      The URL to search.
     * @param argument The argument to extract.
     * @return The value of the specified argument, or null if the argument cannot be found.
     */
    public static String getArgumentValueFromUrl(String url, String argument) {
        try {
            // http://stackoverflow.com/questions/5902090/how-to-extract-parameters-from-a-given-url
            // Pattern p = Pattern.compile("r=([^&]+)");
            // Pattern p = Pattern.compile("(?<=repositoryId=).*?(?=&|$)");
            Pattern p = Pattern.compile(argument + "=([^&]+)");
            Matcher m = p.matcher(url);
            if (m.find()) {
                Log.i(TAG, "Found argument  " + argument + " in url " + url);
                return m.group(1);
            } else
                Log.i(TAG, "Could not find argument  " + argument + " in url " + url);
        } catch (PatternSyntaxException ex) {
            Log.w(TAG, "PatternSyntaxException: " + ex.toString());
        }
        return null;
    }

    /**
     * Forces Dynamix to accept all self-signed certificates.
     */
    public static void acceptAllSelfSignedSSLcertificates() {
        /*
         * For details see: http://stackoverflow.com/questions/2642777/android-trusting-all-
		 * certificates-using-httpclient-over-https This idea may work:
		 * http://jcalcote.wordpress.com/2010/06/22/managing-a-dynamic-java-trust-store/
		 */
        Log.i(TAG, "Setting acceptAllSelfSignedSSLcertificates");
        initCertManagement();
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        }};
        HostnameVerifier hv = new HostnameVerifier() {
            /*
             * Problem: This call appears to be cached after the first call (non-Javadoc)
             * @see javax.net.ssl.HostnameVerifier#verify(java.lang.String, javax.net.ssl.SSLSession)
             */
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                Log.w(TAG, "HostnameVerifier warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
                return true;
            }
        };
        // Install the all-trusting trust manager
        try {
            // Re-initialize the SSLContext with the new values
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            sc.getServerSessionContext().setSessionTimeout(1);
            sc.getClientSessionContext().setSessionTimeout(1);
            sc.getServerSessionContext().setSessionCacheSize(1);
            sc.getClientSessionContext().setSessionCacheSize(1);
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
            Log.i(TAG, "SSL Server Session Timeout is: " + sc.getServerSessionContext().getSessionTimeout());
            Log.i(TAG, "SSL Client Session Timeout is: " + sc.getClientSessionContext().getSessionTimeout());
        } catch (Exception e) {
            Log.e(TAG, "SSLContext Error: " + e.toString());
        }
    }

    /**
     * Forces Dynamix to deny all self-signed certificates.
     */
    public static void denyAllSelfSignedSSLcertificates() {
        Log.i(TAG, "Setting denyAllSelfSignedSSLcertificates");
        initCertManagement();
        try {
            Log.i(TAG, "Invalidating existing SSLSessions");
            Enumeration serverContext = sc.getServerSessionContext().getIds();
            while (serverContext.hasMoreElements()) {
                SSLSession session = sc.getServerSessionContext().getSession((byte[]) serverContext.nextElement());
                Log.i(TAG, "Invalidating server session: " + session);
                session.getSessionContext().setSessionTimeout(1);
                session.invalidate();
            }
            Enumeration clientContext = sc.getClientSessionContext().getIds();
            while (clientContext.hasMoreElements()) {
                SSLSession session = sc.getClientSessionContext().getSession((byte[]) clientContext.nextElement());
                Log.i(TAG, "Invalidating client session: " + session);
                session.getSessionContext().setSessionTimeout(1);
                session.invalidate();
            }
            // Re-initialize the SSLContext with the new values
            sc.init(null, getDefaultTrustManagers(), new java.security.SecureRandom());
            sc.getServerSessionContext().setSessionTimeout(1);
            sc.getClientSessionContext().setSessionTimeout(1);
            sc.getServerSessionContext().setSessionCacheSize(1);
            sc.getClientSessionContext().setSessionCacheSize(1);
            HttpsURLConnection.setDefaultHostnameVerifier(defaultHostnameVerifier);
            HttpsURLConnection.setDefaultSSLSocketFactory(defaultSSLSocketFactory);
            Authenticator.setDefault(null);
            CookieHandler.setDefault(null);
            ResponseCache.setDefault(null);
            System.gc();
        } catch (Exception e) {
            Log.e(TAG, "SSLContext Error: " + e.toString());
        }
    }

    /**
     * Returns the VersionInfo associated with the current Android platform.
     */
    public static VersionInfo getAndroidVersionInfo() {
        // Grab the raw release version string
        String rawRelease = Build.VERSION.RELEASE;
        // Make sure we got something
        if (rawRelease != null && rawRelease.length() > 0) {
            // Strip out any embedded letters (e.g., for beta releases)
            rawRelease = rawRelease.replaceAll("[^\\d.]", "");
            VersionInfo v = VersionInfo.createVersionInfo(rawRelease);
            if (v != null) {
                Log.d(TAG, "Creating Android version from string " + rawRelease);
                return v;
            } else
                Log.w(TAG, "Could not create VersionInfo from string: " + rawRelease);
        }
        Log.w(TAG, "Unknown Android Version! Using Synthetic Android Version 100");
        return new VersionInfo(100, 0, 0);
    }

    /**
     * Returns the VersionInfo associated with the current Android platform.
     */
    public static VersionInfo getAndroidVersionInfo2() {
        /*
		 * http://developer.android.com/reference/android/os/Build.VERSION_CODES.html
		 * http://stackoverflow.com/questions/3423754/retrieving-android-api-version-programmatically
		 */
        String sdkString = Build.VERSION.SDK;
        int deprecated_sdk = Integer.parseInt(sdkString);
        if (deprecated_sdk < 4) {
            switch (deprecated_sdk) {
                case 1:
                    return new VersionInfo(1, 0, 0);
                case 2:
                    return new VersionInfo(1, 1, 0);
                case 3:
                    return new VersionInfo(1, 5, 0);
                default:
                    throw new RuntimeException("Unsupported Android SDK: " + Build.VERSION.SDK);
            }
        } else {
            switch (Build.VERSION.SDK_INT) {
                case 4:
                    return new VersionInfo(1, 6, 0);
                case 5:
                    return new VersionInfo(2, 0, 0);
                case 6:
                    return new VersionInfo(2, 0, 1);
                case 7:
                    return new VersionInfo(2, 1, 0);
                case 8:
                    return new VersionInfo(2, 2, 0);
                case 9:
                    return new VersionInfo(2, 3, 0);
                case 10:
                    return new VersionInfo(2, 3, 3);
                case 11:
                    return new VersionInfo(3, 0, 0);
                case 12:
                    return new VersionInfo(3, 1, 0);
                case 13:
                    return new VersionInfo(3, 2, 0);
                case 14:
                    return new VersionInfo(4, 0, 0);
                case 15:
                    return new VersionInfo(4, 0, 3);
                case 16:
                    return new VersionInfo(4, 1, 0);
                case 17:
                    return new VersionInfo(4, 2, 0);
                case 18:
                    return new VersionInfo(4, 3, 0);
                case 19:
                    return new VersionInfo(4, 4, 0);
                default: {
                    Log.w(TAG, "Android version is above our highest known version: " + Build.VERSION.SDK);
                    return new VersionInfo(4, 2, 0);
                }
            }
        }
    }

    /**
     * Returns the primary data directory path, including the trailing slash.
     */
    public static String getDataDirectoryPath() {
        return context.getDir("data", Context.MODE_PRIVATE).getPath() + "/";
    }

    /**
     * General utility for getting an Enum from a String.
     *
     * @param <T>
     * @param c      The Enum Class
     * @param string The String of the Enum value to get
     * @return The Enum value indicated by the String; or null if the value can't be found
     * http://stackoverflow.com/questions/604424/java-enum-converting-string -to-enum
     */
    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) throws Exception {
        if (c != null && string != null) {
            try {
                return Enum.valueOf(c, string.trim().toUpperCase());
            } catch (IllegalArgumentException ex) {
                throw new Exception("Cannot get string " + string + " from enum " + c);
            }
        } else
            throw new Exception("getEnumFromString encoutered null in either string " + string + " or class " + c);
    }

    /**
     * Recursively walk a directory tree and return a List of all Files found; the List is sorted using
     * File.compareTo().
     *
     * @param aStartingDir is a valid directory, which can be read.
     */
    static public List<File> getFileListing(String path) throws FileNotFoundException {
        File aStartingDir = new File(path);
        validateDirectory(aStartingDir);
        List<File> result = getFileListingNoSort(aStartingDir);
        Collections.sort(result);
        return result;
    }

    /**
     * http://nex-otaku-en.blogspot.com/2010/12/android-put-listview-in-scrollview.html
     *
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren2(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static synchronized void showUpdateAlert(final Activity context, final boolean required, String message,
                                                    final String url) {
        if (!showingUpdateAlert) {
            showingUpdateAlert = true;
            Log.i(TAG, "showUpdateAlert: " + message);
            if (context != null && !DynamixService.isEmbedded()) {
                AlertDialog dialog = new AlertDialog.Builder(context).create();
                dialog.setMessage(message);
                Uri uri = null;
                try {
                    if (url != null && url.length() > 0)
                        uri = Uri.parse(url);
                } catch (Exception e1) {
                    Log.w(TAG, "Could not parse URL: " + url);
                }
                if (required) {
                    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Close Dynamix", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showingUpdateAlert = false;
                            System.exit(0);
                        }
                    });
                } else {
                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Dismiss", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            showingUpdateAlert = false;
                        }
                    });
                }
                if (uri != null) {
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Update Dynamix", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            context.startActivity(browserIntent);
                            if (required)
                                System.exit(0);
                            showingUpdateAlert = false;
                        }
                    });
                }
                try {
                    dialog.show();
                } catch (Exception e) {
                    Log.w(TAG, "Could not show alert: " + e);
                    showingUpdateAlert = false;
                }
            } else
                Log.w(TAG, "Cannot show dialog, no Activity context provided");
        }
    }

    /**
     * Displays the incoming message to the user along options to close or restart Dynamix.
     *
     * @param context The Context of the caller
     * @param message The message to display
     * @param exit    If true, Dynamix is closed; if false, Dynamix is restarted.
     */
    public static void showGlobalAlert(final Activity context, String message, final boolean exit) {
        Log.i(TAG, "showGlobalAlert: " + message);
        if (context != null && !DynamixService.isEmbedded()) {
            try {
                AlertDialog dialog = new AlertDialog.Builder(context).create();
                dialog.setMessage(message);
                if (exit) {
                    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Close Dynamix", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DynamixService.destroyFramework(true, false);
                        }
                    });
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Restart Dynamix", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DynamixService.destroyFramework(true, true);
                        }
                    });
                } else {
                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                dialog.show();
            } catch (Exception e) {
                Log.w(TAG, "Could not show alert: " + e);
            }
        } else
            Log.w(TAG, "Cannot show dialog, no Activity context provided");
    }

    /**
     * Scans the incoming ContextPlugin for configuration errors, outputing descriptive log messages as needed.
     *
     * @param plug The ContextPlugin to validate Returns true if the ContextPlugin successfully validates (i.e. contained
     *             no errors); false otherwise
     */
    public static boolean validateContextPlugin(ContextPlugin plug) {
        // Log.d(TAG, "validateContextPlugin for: " + plug);
        if (plug != null) {
            if (plug.getId() != null && plug.getId().length() > 0) {
                if (plug.getName() != null && plug.getName().length() > 0) {
                    if (plug.getSupportedContextTypes() == null || !plug.getSupportedContextTypes().isEmpty()) {
                        if (plug.getVersion() != null) {
                            return true;
                        } else
                            Log.w(TAG, "ContextPlugin missing version!");
                    } else
                        Log.w(TAG, "ContextPlugin must have at least one supported context type");
                } else
                    Log.w(TAG, "ContextPlugin must have a name");
            } else
                Log.w(TAG, "ContextPlugin must have an id");
        } else
            Log.w(TAG, "ContextPlugin must not be null");
        Log.w(TAG, "ContextPlugin was INVALID: " + plug);
        return false;
    }

    /**
     * Utility for initializing certificate management. This method also initializes the SSLContext on the first call.
     */
    private static void initCertManagement() {
        if (sc == null) {
            try {
				/*
				 * Note that we need TLS on Android (not SSL)
				 */
                sc = SSLContext.getInstance("TLS");
                sc.init(null, getDefaultTrustManagers(), new java.security.SecureRandom());
                sc.getServerSessionContext().setSessionTimeout(1);
                sc.getClientSessionContext().setSessionTimeout(1);
                sc.getServerSessionContext().setSessionCacheSize(1);
                sc.getClientSessionContext().setSessionCacheSize(1);
            } catch (Exception e) {
                Log.e(TAG, "Could not get SSLContext: " + e);
            }
        }
        if (defaultHostnameVerifier == null) {
            defaultHostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
        }
        if (defaultSSLSocketFactory == null) {
            defaultSSLSocketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();
        }
        if (defaultTrustManagers == null) {
            try {
                TrustManagerFactory factory = TrustManagerFactory
                        .getInstance(TrustManagerFactory.getDefaultAlgorithm());
                factory.init(KeyStore.getInstance(KeyStore.getDefaultType()));
                defaultTrustManagers = factory.getTrustManagers();
            } catch (Exception e1) {
                Log.w(TAG, "Could not get default trust managers: " + e1);
            }
        }
    }

    /**
     * Returns the default TrustManagers.
     */
    private static TrustManager[] getDefaultTrustManagers() {
        TrustManager[] tManagers = null;
        try {
            KeyStore ts = KeyStore.getInstance(KeyStore.getDefaultType());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ts);
            tManagers = tmf.getTrustManagers();
        } catch (Exception e2) {
            Log.e(TAG, "Could not access default TrustManagers: " + e2);
        }
        return tManagers;
    }

    /**
     * Utility method that returns the files recursively from a starting directory.
     *
     * @param aStartingDir The directory to start at.
     */
    static private List<File> getFileListingNoSort(File aStartingDir) throws FileNotFoundException {
        List<File> result = new ArrayList<File>();
        File[] filesAndDirs = aStartingDir.listFiles();
        List<File> filesDirs = Arrays.asList(filesAndDirs);
        for (File file : filesDirs) {
            result.add(file); // always add, even if directory
            if (!file.isFile()) {
                // must be a directory
                // recursive call!
                List<File> deeperList = getFileListingNoSort(file);
                result.addAll(deeperList);
            }
        }
        return result;
    }

    /**
     * Validates the specified directory. Directory is valid if it exists, does not represent a file, and can be read.
     */
    static private void validateDirectory(File aDirectory) throws FileNotFoundException {
        if (aDirectory == null) {
            throw new IllegalArgumentException("Directory should not be null.");
        }
        if (!aDirectory.exists()) {
            throw new FileNotFoundException("Directory does not exist: " + aDirectory);
        }
        if (!aDirectory.isDirectory()) {
            throw new IllegalArgumentException("Is not a directory: " + aDirectory);
        }
        if (!aDirectory.canRead()) {
            throw new IllegalArgumentException("Directory cannot be read: " + aDirectory);
        }
    }

    /**
     * Returns the current CPU load for the specified process.
     *
     * @param pid The process to check
     */
    public static float readCpuUsage(int pid) {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/" + pid + "/stat", "r");
            String load = reader.readLine();
            String[] toks = load.split(" ");
            long idle1 = Long.parseLong(toks[5]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[4])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);
            try {
                Thread.sleep(360);
            } catch (Exception e) {
            }
            reader.seek(0);
            load = reader.readLine();
            reader.close();
            toks = load.split(" ");
            long idle2 = Long.parseLong(toks[5]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[4])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);
            return (float) (cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));
        } catch (IOException ex) {
            Log.w(TAG, "Could not read CPU Usage: " + ex);
        }
        return 0;
    }

    /**
     * Returns a list of all running process (limit 1000).
     */
    public static List<ActivityManager.RunningTaskInfo> getRunningProcesses() {
        ActivityManager mgr = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        return mgr.getRunningTasks(1000);
    }

    /**
     * Returns a sorted List of DynamixApplication(s).
     *
     * @param unsortedList The List to sort.
     */
    public static List<DynamixApplication> getSortedAppList(List<DynamixApplication> unsortedList) {
        Collections.sort(unsortedList, new Comparator<DynamixApplication>() {
            @Override
            public int compare(DynamixApplication lhs, DynamixApplication rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        return unsortedList;
    }

    /**
     * Returns a sorted List of ContextPlugin(s).
     *
     * @param unsortedList The List to sort.
     */
    public static List<ContextPlugin> getSortedContextPluginList(List<ContextPlugin> unsortedList) {
        Collections.sort(unsortedList, new Comparator<ContextPlugin>() {
            @Override
            public int compare(ContextPlugin lhs, ContextPlugin rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        return unsortedList;
    }

    /**
     * Returns a sorted List of PendingContextPlugin(s).
     *
     * @param unsortedList The List to sort.
     */
    public static List<PendingContextPlugin> getSortedDiscoveredPluginList(List<PendingContextPlugin> unsortedList) {
        if (unsortedList != null && unsortedList.size() > 0)
            Collections.sort(unsortedList, new Comparator<PendingContextPlugin>() {
                @Override
                public int compare(PendingContextPlugin lhs, PendingContextPlugin rhs) {
                    return lhs.getPendingContextPlugin().getName()
                            .compareToIgnoreCase(rhs.getPendingContextPlugin().getName());
                }
            });
        return unsortedList;
    }

    /**
     * Returns true if the specified runtime can launch a user interface; false otherwise.
     *
     * @param app           The app wishing to launch the interface.
     * @param wrapper       The target runtime wrapper.
     * @param frameworkCall True if this call was initiated by Dynamix; false otherweise
     */
    public static boolean checkPluginInterfaceLaunchable(DynamixApplication app, String contextType,
                                                         ContextPluginInformation plug, boolean frameworkCall) {
        // ContextPlugin plug = wrapper.getParentPlugin();
        if (plug != null) {
            // Make sure the plug-in is installed
            if (plug.isInstalled()) {
                // Make sure the plug-in is enabled
                if (plug.isEnabled()) {
                    // Check for framework or admin calls
                    if (frameworkCall || app.isAdmin()) {
                        return true;
                    } else {
                        // Make sure app can launch the plug-in's UI
                        if (app.getFirewallRule(contextType, plug) != null
                                && app.getFirewallRule(contextType, plug).isAccessGranted()) {
                            return true;
                        } else
                            return false;
                    }
                } else
                    Log.w(TAG, "Plugin is disabled " + plug);
            } else
                Log.w(TAG, "Plugin is not installed " + plug);
        } else
            Log.w(TAG, "checkPluginInterfaceLaunchable: Null plugin!");
        return false;
    }

    /**
     * Pretty print the directory tree and its file names.
     *
     * @param folder must be a folder.
     * @return
     */
    public static String printDirectoryTree(File folder) {
        if (!folder.isDirectory()) {
            throw new IllegalArgumentException("folder is not a Directory");
        }
        int indent = 0;
        StringBuilder sb = new StringBuilder();
        printDirectoryTree(folder, indent, sb);
        return sb.toString();
    }

    private static void printDirectoryTree(File folder, int indent, StringBuilder sb) {
        if (!folder.isDirectory()) {
            throw new IllegalArgumentException("folder is not a Directory");
        }
        sb.append(getIndentString(indent));
        sb.append("+--");
        sb.append(folder.getName());
        sb.append("/");
        sb.append("\n");
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                printDirectoryTree(file, indent + 1, sb);
            } else {
                printFile(file, indent + 1, sb);
            }
        }
    }

    private static void printFile(File file, int indent, StringBuilder sb) {
        sb.append(getIndentString(indent));
        sb.append("+--");
        sb.append(file.getName());
        sb.append("\n");
    }

    private static String getIndentString(int indent) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            sb.append("|  ");
        }
        return sb.toString();
    }

    /*
     * This method is used to wrap the incoming object with OSGi-compatible proxies, which enable a non-OSGi application
     * to utilize dynamically loaded classes (e.g., context event POJOs from ContextPlugins). The method recursively
     * introspects the incoming Object and replaces all fields of type Parcelable with OSGi-compatible proxied versions
     * using Java reflection (including all fields on all objects in the original object's Hierarchy). Supports proxying
     * of individual fields and Collection-based fields (not Map fields, however).
     */
    public static Object createDeepProxy(Object object, boolean isRoot) throws IllegalArgumentException,
            IllegalAccessException, InstantiationException {
        // Make sure Dynamix has an instance of the hosting client's class-loader
        if (DynamixService.hasEmbeddedHostClassLoader()) {
            // Grab the IContextInfo.class name for IContextInfo bridging
            String contextInfoClass = IContextInfo.class.getPackage().getName();
            for (Field f : getAllFields(object.getClass())) {
                // Remember if the field was accessible
                boolean originallyAccessable = f.isAccessible();
                // Make the field accessible, if necessary
                if (!originallyAccessable)
                    f.setAccessible(true);
                // Grab the field
                Object field = f.get(object);
                // Check if the field implements Parcelable
                if (implementsInterface(field, Parcelable.class)) {
                    // We also bridge the IContextInfo implementation class (if possible)
                    String implimentationClass = field.getClass().getPackage().getName();
                    // Create the ApiBridge
                    ApiBridge apiBridge = null;
                    // Handle implementations of IContextInfo
                    if (implementsInterface(field, IContextInfo.class)) {
                        apiBridge = ApiBridge.getApiBridge(DynamixService.getEmbeddedHostClassLoader(),
                                contextInfoClass, implimentationClass);
                    } else {
                        apiBridge = ApiBridge.getApiBridge(DynamixService.getEmbeddedHostClassLoader(),
                                implimentationClass);
                    }
                    // Continue introspection of the Parcelable object
                    field = createDeepProxy(field, false);
                    // Update the field
                    f.set(object, apiBridge.bridge(field, true));
                }
                // For a collection, check if it holds Parcelables
                if (implementsInterface(field, Collection.class)) {
                    Collection<?> embeddedCollection = null;
                    Collection<?> coll = (Collection<?>) f.get(object);
                    if (coll != null && !coll.isEmpty()) {
                        // Test the first object in the collection for implementation of Parcelable
                        Iterator<?> itr = coll.iterator();
                        while (itr.hasNext()) {
                            Object inter = itr.next();
                            if (implementsInterface(inter, Parcelable.class)) {
                                // We have a match, so update the embeddedCollection and break
                                embeddedCollection = coll;
                                break;
                            }
                        }
                    }
                    if (embeddedCollection != null) {
                        // Create an instance of the original collection type for later injection
                        @SuppressWarnings("rawtypes")
                        Collection proxiedCollection = coll.getClass().newInstance();
                        // Proxy each Parcelable in the embeddedCollection
                        for (Object p : embeddedCollection) {
                            // We also bridge the IContextInfo implementation class (if possible)
                            String implimentationClass = p.getClass().getPackage().getName();
                            // Create the ApiBridge
                            ApiBridge apiBridge = null;
                            // Handle implementations of IContextInfo
                            if (implementsInterface(field, IContextInfo.class)) {
                                apiBridge = ApiBridge.getApiBridge(DynamixService.getEmbeddedHostClassLoader(),
                                        contextInfoClass, implimentationClass);
                            } else {
                                apiBridge = ApiBridge.getApiBridge(DynamixService.getEmbeddedHostClassLoader(),
                                        implimentationClass);
                            }
                            // Add the proxied class to the proxiedCollection
                            proxiedCollection.add(apiBridge.bridge(p, true));
                            // Introspect the IContextInfo object for more embedded IContextInfo objects
                            p = createDeepProxy(p, false);
                        }
                        // Update the field with the proxiedCollection
                        f.set(object, proxiedCollection);
                    }
                    // Restore original accessible state
                    f.setAccessible(originallyAccessable);
                }
            }
            // TODO: Also handle Maps with embedded Parcelables?
            if (isRoot) {
                // We also bridge the IContextInfo implementation class (if possible)
                String rootClass = object.getClass().getPackage().getName();
                // Create the ApiBridge
                ApiBridge apiBridge = null;
                // Handle implementations of IContextInfo
                if (implementsInterface(object, IContextInfo.class)) {
                    apiBridge = ApiBridge.getApiBridge(DynamixService.getEmbeddedHostClassLoader(), contextInfoClass,
                            rootClass);
                } else {
                    apiBridge = ApiBridge.getApiBridge(DynamixService.getEmbeddedHostClassLoader(), rootClass);
                }
                // Bridge the root object
                object = apiBridge.bridge(object, true);
            }
        } else
            Log.w(TAG, "No host class-loader set, unable to proxy object!");
        return object;
    }

    /**
     * Returns true if the Object implements the specified interface; false otherwise.
     *
     * @param object The Object to check.
     * @param interf The interface to check.
     */
    private static Boolean implementsInterface(Object object, Class interf) {
        return interf.isInstance(object);
    }

    /**
     * Returns all the fields of the specified Class, and it's super-classes.
     *
     * @param type The class to extract fields from.
     */
    private static List<Field> getAllFields(Class<?> type) {
        List<Field> fields = new ArrayList<Field>();
        for (Field field : type.getDeclaredFields()) {
            fields.add(field);
        }
        if (type.getSuperclass() != null) {
            fields.addAll(getAllFields(type.getSuperclass()));
        }
        return fields;
    }
}