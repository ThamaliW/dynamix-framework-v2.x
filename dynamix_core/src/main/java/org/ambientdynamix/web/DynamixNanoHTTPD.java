/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import android.util.Log;

import org.ambientdynamix.core.DynamixPreferences;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import fi.iki.elonen.NanoHTTPD;

/**
 * Extension of NanoHTTPD that provides additional Dynamix functionality (e.g., accessing the underlying socket associated with sessions).
 *
 * @author Darren Carlson
 */
public class DynamixNanoHTTPD extends NanoHTTPD {
    final static String TAG = DynamixNanoHTTPD.class.getSimpleName();
    private List<DynamixClientHandler> handlers = new ArrayList<>();

    public DynamixNanoHTTPD(int port) {
        super(port);
    }

    @Override
    protected ClientHandler createClientHandler(final Socket finalAccept, final InputStream inputStream) {
        /**
         * Note that NanoHTTPD sends us a PlainSocketImpl$PlainSocketInputStream here for the inputStream argument.
         */
        DynamixClientHandler handler = new DynamixClientHandler(inputStream, finalAccept);
        synchronized (handlers) {
            // Cache input stream, if needed
            if (!handlers.contains(handler)) {
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.i(TAG, "DynamixNanoHTTPD createClientHandler created " + handler + " for socket " + ": " + printSocket(handler.getAcceptSocket()));
                handlers.add(handler);
            }
        }
        // Return the DynamixClientHandler
        return handler;
    }

    /**
     * Returns the Socket associated with the session, or null if not found.
     */
    public Socket getSocket(IHTTPSession session) throws Exception {
        synchronized (handlers) {
            for (DynamixClientHandler handler : handlers) {
                if (handler.hasSession() && handler.getSession() == session)
                    if (DynamixPreferences.isDetailedLoggingEnabled())
                        Log.d(TAG, "DynamixNanoHTTPD Found socket for session " + session + ": " + printSocket(handler.getAcceptSocket()));
                return handler.getAcceptSocket();
            }
        }
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.d(TAG, "DynamixNanoHTTPD Could not find socket for session " + session);
        return null;
    }

    /**
     * Utility for outputting info regarding sockets.
     */
    private String printSocket(Socket s) {
        if (s != null)
            return s.toString() + ": bound=" + s.isBound() + ", connected=" + s.isConnected();
        else
            return "Null socket";
    }

    /**
     * We extend ClientHandler to provide access to the associated session and socket.
     * TODO: Make sure to update this class if the base class changes!
     */
    private class DynamixClientHandler extends ClientHandler {
        private final InputStream inputStream;
        private final Socket acceptSocket;
        private HTTPSession session;

        public HTTPSession getSession() {
            return session;
        }

        public boolean hasSession() {
            return session != null;
        }

        public Socket getAcceptSocket() {
            return acceptSocket;
        }

        public InputStream getOriginalInputStream() {
            return inputStream;
        }

        public DynamixClientHandler(InputStream inputStream, Socket acceptSocket) {
            super(inputStream, acceptSocket);
            this.inputStream = inputStream;
            this.acceptSocket = acceptSocket;
        }

        @Override
        public void close() {
            safeClose(this.inputStream);
            safeClose(this.acceptSocket);
        }

        @Override
        public void run() {
            OutputStream outputStream = null;
            try {
                outputStream = this.acceptSocket.getOutputStream();
                TempFileManager tempFileManager = getTempFileManagerFactory().create();
                session = new HTTPSession(tempFileManager, this.inputStream, outputStream, this.acceptSocket.getInetAddress());
                while (!this.acceptSocket.isClosed()) {
                    session.execute();
                }
            } catch (Exception e) {
                // When the socket is closed by the client,
                // we throw our own SocketException
                // to break the "keep alive" loop above. If
                // the exception was anything other
                // than the expected SocketException OR a
                // SocketTimeoutException, print the
                // stacktrace
                if (!(e instanceof SocketException && "NanoHttpd Shutdown".equals(e.getMessage())) && !(e instanceof SocketTimeoutException)) {
                    Log.e(TAG, "DynamixNanoHTTPD Communication with the client broken, or an bug in the handler code", e);
                }
            } finally {
                safeClose(outputStream);
                safeClose(this.inputStream);
                safeClose(this.acceptSocket);
                DynamixNanoHTTPD.this.asyncRunner.closed(this);
                synchronized (handlers) {
                    handlers.remove(this);
                }
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG, "DynamixNanoHTTPD Closed DynamixClientHandler " + this + " with socket " + ": " + printSocket(getAcceptSocket()));
            }
        }
    }

    /**
     * We define safeClose here so that the DynamixClientHandler can access it.
     * TODO: Make sure to update this method if the base class changes!
     */
    private static final void safeClose(Object closeable) {
        try {
            if (closeable != null) {
                if (closeable instanceof Closeable) {
                    ((Closeable) closeable).close();
                } else if (closeable instanceof Socket) {
                    ((Socket) closeable).close();
                } else if (closeable instanceof ServerSocket) {
                    ((ServerSocket) closeable).close();
                } else {
                    throw new IllegalArgumentException("Unknown object to close");
                }
            }
        } catch (IOException e) {
            Log.w(TAG, "Could not safeClose: ", e);
        }
    }
}
