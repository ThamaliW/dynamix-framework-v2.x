/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.ambientdynamix.api.application.*;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.core.DynamixPreferences;
import org.ambientdynamix.core.WebFacadeBinder;
import org.ambientdynamix.remote.RemotePairing;
import org.ambientdynamix.security.CryptoUtils;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

/**
 * Provides a REST interface for web clients connecting to Dynamix.
 * 
 * @author Darren Carlson
 * 
 */
public class RESTHandler {
	// Rest endpoints
	public static final String DYNAMIX_BIND = "/dynamixbind";
	public static final String DYNAMIX_UNBIND = "/dynamixunbind";
	public static final String DYNAMIX_PAIR = "/pair";
	public static final String DYNAMIX_UNPAIR = "/unpair";
	public static final String IS_DYNAMIX_TOKEN_VALID = "/isdynamixtokenvalid";
	public static final String DYNAMIX_VERSION = "/dynamixversion";
	public static final String EVENT_CALLBACK = "/eventcallback";
	public static final String CHECK_DYNAMIX_ACTIVE = "/isdynamixactive";
	public static final String IS_DYNAMIX_SESSION_OPEN = "/isdynamixsessionopen";
	public static final String OPEN_DYNAMIX_SESSION = "/opendynamixsession";
	public static final String SET_DYNAMIX_SESSION_LISTENER = "/setdynamixsessionlistener";
	public static final String CLOSE_DYNAMIX_SESSION = "/closedynamixsession";
	public static final String CREATE_CONTEXT_HANDLER = "/createcontexthandler";
	public static final String REMOVE_CONTEXT_HANDLER = "/removecontexthandler";
	public static final String ADD_CONTEXT_SUPPORT = "/addcontextsupport";
	public static final String ADD_CONFIGURED_CONTEXT_SUPPORT = "/addconfiguredcontextsupport";
	public static final String REMOVE_CONTEXT_SUPPORT_FOR_TYPE = "/removecontextsupportforcontexttype";
	public static final String REMOVE_CONTEXT_SUPPORT_FOR_ID = "/removecontextsupportforsupportid";
	public static final String REMOVE_ALL_CONTEXT_SUPPORT = "/removeallcontextsupport";
	public static final String CONTEXT_REQUEST = "/contextrequest";
	public static final String CONFIGURED_CONTEXT_REQUEST = "/configuredcontextrequest";
	public static final String GET_CONTEXT_SUPPORT = "/getcontextsupport";
	public static final String GET_ALL_CONTEXT_PLUG_INS = "/getallcontextplugininformation";
	public static final String GET_ALL_CONTEXT_PLUG_INS_FOR_TYPE = "/getallcontextplugininformationfortype";
	public static final String GET_INSTALLED_CONTEXT_PLUG_INS = "/getinstalledcontextplugininformation";
	public static final String GET_CONTEXT_PLUG_IN = "/getcontextplugininformation";
	public static final String RESEND_CACHED_EVENTS = "/resendcachedcontextevents";
	public static final String OPEN_CONTEXT_PLUGIN_CONFIGURATION_VIEW = "/opencontextpluginconfigurationview";
	public static final String OPEN_DEFAULT_CONTEXT_PLUGIN_CONFIGURATION_VIEW = "/opendefaultcontextpluginconfigurationview";
	public static final String HELLO = "/hello";
	// Private data
	private WebFacadeBinder facade;
	private WebConnector connector;
	private final String TAG = this.getClass().getSimpleName();

	/**
	 * Creates a RESTHandler.
	 * 
	 * @param facade
	 *            The WebFacadeBinder for this handler to use.
	 */
	public RESTHandler(WebFacadeBinder facade, WebConnector connector) {
		this.facade = facade;
		this.connector = connector;
	}

	/**
	 * Process a web client request using the Dynamix REST interface.
	 */
	public Response processRequest(final WebAppManager<String> wMgr, IHTTPSession session, String decryptedUri)
			throws Exception {
		// Prep the incoming URI
		String uri = URLDecoder.decode(decryptedUri, "UTF-8");
		// session.setUri(URLDecoder.decode(session.getUri().toLowerCase(Locale.US).trim(), "UTF-8"));
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.v(TAG, "Started processRequest for uri: " + uri + " and params " + session.getParms());
		/*
		 * Handle CONFIGURED_CONTEXT_REQUEST first, which may be any REST method type (e.g., POST, PUT, GET, etc).
		 */
		if (uri.equalsIgnoreCase(CONFIGURED_CONTEXT_REQUEST)) {
			// Extract required arguments
			String pluginId = session.getParms().get("pluginId");
			String pluginVersion = session.getParms().get("pluginVersion");
			String contextType = session.getParms().get("contextType");
			String contextHandlerId = session.getParms().get("contextHandlerId");
			String callbackId = session.getParms().get("callbackId");
			if (contextHandlerId != null) {
				// Check for context handler
				ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
				if (handler != null) {
					if (callbackId != null) {
						/*
						 * Handle the configuredContextRequest and return the result. During the call, we setup the
						 * config Bundle using the incoming Web data.
						 */
						IContextRequestCallback callback = new WebContextRequestCallback(wMgr, callbackId);
						handler.contextRequest(
								pluginId,
								pluginVersion,
								contextType,
								createConfigurationBundle(session.getMethod(), uri, session.getHeaders(),
										session.getParms()), callback);
					} else {
						handler.contextRequest(
								pluginId,
								contextType,
								createConfigurationBundle(session.getMethod(), uri, session.getHeaders(),
										session.getParms()));
					}
				} else {
					// Fail on handler not found
					sendFailure(wMgr, callbackId, "Context handler not found for id " + contextHandlerId,
							ErrorCodes.STATE_ERROR);
				}
			} else {
				sendMissingParam(wMgr, callbackId, "contextHandlerId");
			}
		}
		// Handle ADD_CONFIGURED_CONTEXT_SUPPORT, which may be any REST method type (e.g., POST, PUT, GET, etc).
		else if (uri.equalsIgnoreCase(ADD_CONFIGURED_CONTEXT_SUPPORT)) {
			// Extract required arguments
			String contextHandlerId = session.getParms().get("contextHandlerId");
			String contextType = session.getParms().get("contextType");
			String pluginId = session.getParms().get("pluginId");
			String pluginVersion = session.getParms().get("pluginVersion");
			// Extract optional arguments
			String callbackId = session.getParms().get("callbackId");
			if (contextHandlerId != null && contextType != null && pluginId != null) {
				ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
				if (handler != null) {
					/*
					 * Handle special case of multiple plug-ins with the same context type
					 */
					List<VersionedPlugin> plugs = getVersionedPlugins(pluginId, pluginVersion);
					IContextSupportCallback callback = null;
					if (callbackId != null)
						callback = new WebContextSupportCallback(wMgr, callbackId);
					String contextListenerId = session.getParms().get("contextListenerId");
					IContextListener listener = null;
					if (contextListenerId != null)
						listener = new WebContextListener(wMgr, contextListenerId);
					// Note: This method is null safe
					handler.addVersionedContextSupport(
							plugs,
							contextType,
							createConfigurationBundle(session.getMethod(), uri, session.getHeaders(),
									session.getParms()), listener, callback);
				} else {
					// Fail on handler not found
					sendFailure(wMgr, callbackId, "Context handler not found for id " + contextHandlerId,
							ErrorCodes.STATE_ERROR);
				}
			} else {
				sendMissingParam(wMgr, callbackId, "contextHandlerId", "contextType", "pluginId");
			}
		}
		// Handle GET-based requests
		else if (session.getMethod() == Method.GET) {
			// Handle EVENT_CALLBACK
			if (uri.equalsIgnoreCase(EVENT_CALLBACK)) {
				// Check if we need to wait for an event to send
				if (wMgr.isEmpty()) {
					/*
					 * Wait for events to arrive; however, after 10 seconds we send HTTP_NOTFOUND so that the XHR
					 * request doesn't timeout. Javascript will then call us back to wait for events.
					 */
					wMgr.waitForEvent(10000);
				}
				// Check for an event to send
				if (wMgr.isEmpty()) {
					// No event, so send 404
					return connector.createErrorResponse("NOT_FOUND", Status.NOT_FOUND, ErrorCodes.NOT_FOUND);
				} else {
					/*
					 * Access and send the event's command. This is already encrypted through the asynchroneous callback
					 * mechanism, see WebHandler.
					 */
					String command = wMgr.poll();
					return connector.createOkResponse(command);
				}
			}
			// Handle DYNAMIX_VERSION
			else if (uri.equalsIgnoreCase(DYNAMIX_VERSION)) {
				return encryptResponse(wMgr, facade.getDynamixVersion().toString());
			}
			// Handle CHECK_DYNAMIX_ACTIVE
			else if (uri.equalsIgnoreCase(CHECK_DYNAMIX_ACTIVE)) {
				return encryptResponse(wMgr, Boolean.toString(facade.isDynamixActive()));
			}
			// Handle CREATE_CONTEXT_HANDLER
			else if (uri.equalsIgnoreCase(CREATE_CONTEXT_HANDLER)) {
				// Extract required arguments
				String callbackId = session.getParms().get("callbackId");
				if (callbackId != null) {
					IContextHandlerCallback callback = new WebContextHandlerCallback(wMgr, callbackId);
					facade.createContextHandler(callback);
				} else {
					sendMissingParam(wMgr, callbackId, "callbackId");
				}
			}
			// Handle REMOVE_CONTEXT_HANDLER
			else if (uri.equalsIgnoreCase(REMOVE_CONTEXT_HANDLER)) {
				// Extract required arguments
				String contextHandlerId = session.getParms().get("contextHandlerId");
				// callbackId is optional
				String callbackId = session.getParms().get("callbackId");
				if (contextHandlerId != null) {
					// Check for context handler
					ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
					if (handler != null) {
						ICallback callback = session.getParms().get("callbackId") == null ? null : new WebCallback(
								wMgr, callbackId);
						facade.removeContextHandlerWithCallback(handler.getContextHandler(), callback);
					} else {
						// Fail on handler not found
						sendFailure(wMgr, callbackId, "Context handler not found for id " + contextHandlerId,
								ErrorCodes.STATE_ERROR);
					}
				} else {
					sendMissingParam(wMgr, callbackId, "contextHandlerId");
				}
			}
			// Handle ADD_CONTEXT_SUPPORT
			else if (uri.equalsIgnoreCase(ADD_CONTEXT_SUPPORT)) {
				// Extract required arguments
				String contextHandlerId = session.getParms().get("contextHandlerId");
				String contextType = session.getParms().get("contextType");
				String pluginId = session.getParms().get("pluginId");
				String pluginVersion = session.getParms().get("pluginVersion");
				// Extract optional arguments
				String callbackId = session.getParms().get("callbackId");
				if (contextHandlerId != null && contextType != null && pluginId != null) {
					// Check for context handler
					ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
					if (handler != null) {
						/*
						 * Handle special case of multiple plug-ins with the same context type
						 */
						List<VersionedPlugin> plugs = getVersionedPlugins(pluginId, pluginVersion);
						IContextSupportCallback callback = null;
						if (callbackId != null)
							callback = new WebContextSupportCallback(wMgr, callbackId);
						String contextListenerId = session.getParms().get("contextListenerId");
						IContextListener listener = null;
						if (contextListenerId != null)
							listener = new WebContextListener(wMgr, contextListenerId);
						// Note: This method is null safe
						handler.addVersionedContextSupport(plugs, contextType, null, listener, callback);
					} else {
						// Fail on handler not found
						sendFailure(wMgr, callbackId, "Context handler not found for id " + contextHandlerId,
								ErrorCodes.STATE_ERROR);
					}
				} else {
					sendMissingParam(wMgr, callbackId, "contextHandlerId", "contextType", "pluginId");
				}
			}
			// Handle REMOVE_CONTEXT_SUPPORT_FOR_TYPE
			else if (uri.equalsIgnoreCase(REMOVE_CONTEXT_SUPPORT_FOR_TYPE)) {
				// Extract required arguments
				String contextHandlerId = session.getParms().get("contextHandlerId");
				// Extract optional arguments
				String callbackId = session.getParms().get("callbackId");
				if (contextHandlerId != null) {
					// Check for context handler
					ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
					if (handler != null) {
						String contextType = session.getParms().get("contextType");
						if (contextType != null) {
							if (callbackId == null) {
								handler.removeContextSupport(contextType);
							} else {
								handler.removeContextSupport(contextType, new WebCallback(wMgr, callbackId));
							}
						} else {
							sendMissingParam(wMgr, callbackId, "contextType");
						}
					} else {
						// Fail on handler not found
						sendFailure(wMgr, callbackId, "Context handler not found for id " + contextHandlerId,
								ErrorCodes.STATE_ERROR);
					}
				} else {
					sendMissingParam(wMgr, callbackId, "contextHandlerId");
				}
			}
			// Handle REMOVE_CONTEXT_SUPPORT_FOR_ID
			else if (uri.equalsIgnoreCase(REMOVE_CONTEXT_SUPPORT_FOR_ID)) {
				// Extract required arguments
				String contextHandlerId = session.getParms().get("contextHandlerId");
				// Extract optional arguments
				String callbackId = session.getParms().get("callbackId");
				if (contextHandlerId != null) {
					// Check for context handler
					ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
					if (handler != null) {
						String supportId = session.getParms().get("supportId");
						if (supportId != null) {
							boolean found = false;
							// Search for the context support to remove
							for (ContextSupportInfo supInfo : handler.getContextSupport()) {
								if (supInfo.getSupportId().equalsIgnoreCase(supportId)) {
									if (callbackId == null) {
										handler.removeContextSupport(supInfo);
									} else {
										handler.removeContextSupport(supInfo, new WebCallback(wMgr, callbackId));
									}
									found = true;
									break;
								}
							}
							if (!found) {
								// Fail on context support not found
								sendFailure(wMgr, callbackId, "Context support not found for id " + supportId,
										ErrorCodes.STATE_ERROR);
							}
						} else {
							sendMissingParam(wMgr, callbackId, "supportId");
						}
					} else {
						// Fail on handler not found
						sendFailure(wMgr, callbackId, "Context handler not found for id " + contextHandlerId,
								ErrorCodes.STATE_ERROR);
					}
				} else {
					sendMissingParam(wMgr, callbackId, "contextHandlerId");
				}
			}
			// Handle REMOVE_ALL_CONTEXT_SUPPORT
			else if (uri.equalsIgnoreCase(REMOVE_ALL_CONTEXT_SUPPORT)) {
				// Extract required arguments
				String contextHandlerId = session.getParms().get("contextHandlerId");
				// Extract optional arguments
				String callbackId = session.getParms().get("callbackId");
				if (contextHandlerId != null) {
					// Check for context handler
					ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
					if (handler != null) {
						if (callbackId == null) {
							handler.removeAllContextSupport();
						} else {
							handler.removeAllContextSupport(new WebCallback(wMgr, callbackId));
						}
					} else {
						// Fail on handler not found
						sendFailure(wMgr, callbackId, "Context handler not found for id " + contextHandlerId,
								ErrorCodes.STATE_ERROR);
					}
				} else {
					sendMissingParam(wMgr, callbackId, "contextHandlerId");
				}
			}
			// Handle CONTEXT_REQUEST
			else if (uri.equalsIgnoreCase(CONTEXT_REQUEST)) {
				// Extract required arguments
				String contextHandlerId = session.getParms().get("contextHandlerId");
				// Extract optional arguments
				String callbackId = session.getParms().get("callbackId");
				if (contextHandlerId != null) {
					// Check for context handler
					ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
					if (handler != null) {
						String pluginId = session.getParms().get("pluginId");
						String contextType = session.getParms().get("contextType");
						String pluginVersion = session.getParms().get("pluginVersion");
						if (pluginId != null && contextType != null && callbackId != null) {
							IContextRequestCallback callback = new WebContextRequestCallback(wMgr, callbackId);
							handler.contextRequest(pluginId, pluginVersion, contextType, callback);
						} else {
							sendMissingParam(wMgr, callbackId, "pluginId", "contextType", "callbackId");
						}
					} else {
						// Fail on handler not found
						sendFailure(wMgr, callbackId, "Context handler not found for id " + contextHandlerId,
								ErrorCodes.STATE_ERROR);
					}
				} else {
					sendMissingParam(wMgr, callbackId, "contextHandlerId");
				}
			}
			// Handle OPEN_DYNAMIX_SESSION
			else if (uri.equalsIgnoreCase(OPEN_DYNAMIX_SESSION)) {
				// Extract optional arguments.
				final String callbackId = session.getParms().get("callbackId");
				ISessionCallback callback = callbackId == null ? null : new SessionCallback() {
					@Override
					public void onSuccess(DynamixFacade facade) throws RemoteException {
						sendSuccess(wMgr, callbackId);
					}

					@Override
					public void onFailure(String message, int errorCode) throws RemoteException {
						sendFailure(wMgr, callbackId, message, errorCode);
					}
				};
				String sessionListenerId = session.getParms().get("sessionListenerId");
				ISessionListener sessionListener = sessionListenerId == null ? null : new WebSessionListener(wMgr,
						sessionListenerId);
				// Note: openSessionWithListenerAndCallback notifies, even if the session is already open.
				facade.openSessionWithListenerAndCallback(sessionListener, callback);
			}
			// Handle CLOSE_DYNAMIX_SESSION
			else if (uri.equalsIgnoreCase(CLOSE_DYNAMIX_SESSION)) {
				// callbackId is optional
				String callbackId = session.getParms().get("callbackId");
				ICallback callback = session.getParms().get("callbackId") == null ? null : new WebCallback(wMgr,
						callbackId);
				// Note: closeSessionWithCallback notifies, even if the session is already closed.
				facade.closeSessionWithCallback(callback);
			}
			// Handle SET_DYNAMIX_SESSION_LISTENER
			else if (uri.equalsIgnoreCase(SET_DYNAMIX_SESSION_LISTENER)) {
				// Extract required arguments
				String sessionListenerId = session.getParms().get("sessionListenerId");
				// callbackId is optional
				String callbackId = session.getParms().get("callbackId");
				if (sessionListenerId != null) {
					ISessionListener sessionListener = new WebSessionListener(wMgr, sessionListenerId);
					facade.setSessionListener(sessionListener);
					sendSuccess(wMgr, callbackId);
				} else {
					sendMissingParam(wMgr, callbackId, "sessionListenerId");
				}
			}
			// Handle OPEN_DEFAULTCONTEXT_PLUGIN_CONFIGURATION_VIEW
			else if (uri.equalsIgnoreCase(OPEN_DEFAULT_CONTEXT_PLUGIN_CONFIGURATION_VIEW)) {
				// Extract required arguments
				String pluginId = session.getParms().get("pluginId");
				// pluginVersion is optional
				String pluginVersion = session.getParms().get("pluginVersion");
				// callbackId is optional
				String callbackId = session.getParms().get("callbackId");
				if (pluginId != null) {
					Result result = facade.openDefaultContextPluginConfigurationView(pluginId, pluginVersion);
					if (result.wasSuccessful())
						sendSuccess(wMgr, callbackId);
					else
						sendFailure(wMgr, callbackId, result.getMessage(), result.getErrorCode());
				} else {
					sendMissingParam(wMgr, callbackId, "pluginId", "pluginVersion");
				}
			}
			// Handle OPEN_DEFAULTCONTEXT_PLUGIN_CONFIGURATION_VIEW
			else if (uri.equalsIgnoreCase(OPEN_CONTEXT_PLUGIN_CONFIGURATION_VIEW)) {
				// Extract required arguments
				String pluginId = session.getParms().get("pluginId");
				// pluginVersion is optional
				String pluginVersion = session.getParms().get("pluginVersion");
				// callbackId is optional
				String callbackId = session.getParms().get("callbackId");
				if (pluginId != null) {
					Result result = facade.openContextPluginConfigurationView(
							pluginId,
							pluginVersion,
							createConfigurationBundle(session.getMethod(), uri, session.getHeaders(),
									session.getParms()));
					if (result.wasSuccessful())
						sendSuccess(wMgr, callbackId);
					else
						sendFailure(wMgr, callbackId, result.getMessage(), result.getErrorCode());
				} else {
					sendMissingParam(wMgr, callbackId, "pluginId", "pluginVersion");
				}
			}
			// Handle GET_CONTEXT_SUPPORT
			else if (uri.equalsIgnoreCase(GET_CONTEXT_SUPPORT)) {
				// Extract required arguments
				String contextHandlerId = session.getParms().get("contextHandlerId");
				String callbackId = session.getParms().get("callbackId");
				if (contextHandlerId != null && callbackId != null) {
					// Check for context handler
					ContextHandler handler = wMgr.getContextHandler(contextHandlerId);
					if (handler != null) {
						WebContextSupportQueryCallback callback = new WebContextSupportQueryCallback(wMgr, callbackId);
						ContextSupportResult result = handler.getContextSupportResult();
						if (result.wasSuccessful()) {
							callback.onSuccess(result.getContextSupportInfo());
						} else {
							callback.onFailure(result.getMessage(), result.getErrorCode());
						}
					} else {
						// Fail on handler not found
						return errorResponse(wMgr, "Context handler not found for id " + contextHandlerId,
								Status.BAD_REQUEST, ErrorCodes.NOT_FOUND);
					}
				} else {
					sendMissingParam(wMgr, callbackId, "contextHandlerId");
				}
			}
			// Handle GET_CONTEXT_PLUG_IN
			else if (uri.equalsIgnoreCase(GET_CONTEXT_PLUG_IN)) {
				// Extract required arguments
				String pluginId = session.getParms().get("pluginId");
				String callbackId = session.getParms().get("callbackId");
				if (pluginId != null && callbackId != null) {
					ContextPluginInformationResult result = facade.getContextPluginInformation(pluginId);
					WebContextPluginQueryCallback callback = new WebContextPluginQueryCallback(wMgr, callbackId);
					if (result.wasSuccessful()) {
						callback.onSuccess(result.getContextPluginInformation());
					} else {
						callback.onFailure(result.getMessage(), result.getErrorCode());
					}
				} else {
					sendMissingParam(wMgr, callbackId, "pluginId", "callbackId");
				}
			}
			// Handle GET_ALL_CONTEXT_PLUG_INS
			else if (uri.equalsIgnoreCase(GET_ALL_CONTEXT_PLUG_INS)) {
				// Extract required arguments
				String callbackId = session.getParms().get("callbackId");
				if (callbackId != null) {
					ContextPluginInformationResult result = facade.getAllContextPluginInformation();
					WebContextPluginQueryCallback callback = new WebContextPluginQueryCallback(wMgr, callbackId);
					if (result.wasSuccessful()) {
						callback.onSuccess(result.getContextPluginInformation());
					} else {
						callback.onFailure(result.getMessage(), result.getErrorCode());
					}
				} else {
					sendMissingParam(wMgr, callbackId, "callbackId");
				}
			}
			// Handle GET_INSTALLED_CONTEXT_PLUG_INS
			else if (uri.equalsIgnoreCase(GET_INSTALLED_CONTEXT_PLUG_INS)) {
				// Extract required arguments
				String callbackId = session.getParms().get("callbackId");
				if (callbackId != null) {
					ContextPluginInformationResult result = facade.getInstalledContextPluginInformation();
					WebContextPluginQueryCallback callback = new WebContextPluginQueryCallback(wMgr, callbackId);
					if (result.wasSuccessful()) {
						callback.onSuccess(result.getContextPluginInformation());
					} else {
						callback.onFailure(result.getMessage(), result.getErrorCode());
					}
				} else {
					sendMissingParam(wMgr, callbackId, "callbackId");
				}
			}
			// Handle GET_ALL_CONTEXT_PLUG_INS_FOR_TYPE
			else if (uri.equalsIgnoreCase(GET_ALL_CONTEXT_PLUG_INS_FOR_TYPE)) {
				// Extract required arguments
				String contextType = session.getParms().get("contextType");
				String callbackId = session.getParms().get("callbackId");
				if (contextType != null && callbackId != null) {
					ContextPluginInformationResult result = facade.getAllContextPluginInformationForType(contextType);
					WebContextPluginQueryCallback callback = new WebContextPluginQueryCallback(wMgr, callbackId);
					if (result.wasSuccessful()) {
						callback.onSuccess(result.getContextPluginInformation());
					} else {
						callback.onFailure(result.getMessage(), result.getErrorCode());
					}
				} else {
					sendMissingParam(wMgr, callbackId, "contextType", "callbackId");
				}
			}
			// Error: No valid REST end-point found
			else {
				return errorResponse(wMgr, "REST endpoint invalid: " + uri, Status.BAD_REQUEST,
						ErrorCodes.NOT_SUPPORTED);
			}
		} else {
			// Invalid request
			return errorResponse(wMgr, "Invalid request: " + uri, Status.BAD_REQUEST, ErrorCodes.NOT_SUPPORTED);
		}
		Log.v(TAG, "Completed processRequest for uri: " + uri);
		return connector.createOkResponse();
	}

	private Response encryptResponse(WebAppManager<String> wMgr, String message) {
		try {
			String response = message;
			RemotePairing pairing = wMgr.getDynamixApp().getRemotePairing();
			if (pairing != null)
				response = CryptoUtils
						.encryptStringAES(pairing.getEncryptionKey(), pairing.getEncryptionKey(), message);
			return connector.createOkResponse(response);
		} catch (Exception e) {
			return connector.createErrorResponse("Security Error: " + e.getMessage(), Status.INTERNAL_ERROR,
					ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
		}
	}

	private Response errorResponse(WebAppManager<String> wMgr, String message, Status status, int errorCode) {
		Log.w(TAG, message);
		// Create a map for return values
		Map<Object, Object> jsonMap = new HashMap<Object, Object>();
		jsonMap.put("message", message);
		jsonMap.put("errorCode", errorCode);
		try {
			String response = message;
			/*
			 * Handle remotely paired case, if needed. Note that WebAppManagers will not have an associated 
			 * Dynamix Application if the caller hasn't opened a session for the first time.
			 */
			if (wMgr.hasDynamixApplication() && wMgr.getDynamixApp().isRemotelyPaired()) {
				RemotePairing pairing = wMgr.getDynamixApp().getRemotePairing();
				response = CryptoUtils.encryptStringAES(pairing.getEncryptionKey(), pairing.getEncryptionKey(),
						WebUtils.encodeMapAsJson(jsonMap));
			}
			return connector.createErrorResponse(response, status, errorCode);
		} catch (Exception e) {
			return connector.createErrorResponse("DYNAMIX_FRAMEWORK_ERROR", Status.INTERNAL_ERROR,
					ErrorCodes.DYNAMIX_FRAMEWORK_ERROR);
		}
	}

	/*
	 * Helper method that creates a configuration Bundle for Web requests.
	 */
	private Bundle createConfigurationBundle(Method method, String uri, Map<String, String> headers,
			Map<String, String> parms) throws Exception {
		// Create a Bundle to hold the request config
		Bundle b = new Bundle();
		// Set WEB_REQUEST to true
		b.putBoolean(PluginConstants.WEB_REQUEST, true);
		// Add the WEB_REQUEST_METHOD method
		b.putString(PluginConstants.WEB_REQUEST_METHOD, method.name());
		// Add the WEB_REQUEST_URI
		b.putString(PluginConstants.WEB_REQUEST_URI, uri);
		// Add the WEB_REQUEST_CONTENT_TYPE
		b.putString(PluginConstants.WEB_REQUEST_CONTENT_TYPE, headers.get("content-type"));
		// Add the WEB_REQUEST_CONTENT_LEGNTH
		b.putString(PluginConstants.WEB_REQUEST_CONTENT_LENGTH, headers.get("content-length"));
		// Add the WEB_REQUEST_CONTENT
		b.putSerializable(PluginConstants.WEB_REQUEST_CONTENT, new HashMap<String, String>(parms));
		// Extract the parms into the bundle as key-value pairs
		for (Object key : parms.keySet()) {
			// Make sure params don't overwrite Dynamix keys
			if (!(key.toString().contains(PluginConstants.WEB_REQUEST)
					|| key.toString().contains(PluginConstants.WEB_REQUEST_METHOD)
					|| key.toString().contains(PluginConstants.WEB_REQUEST_URI)
					|| key.toString().contains(PluginConstants.WEB_REQUEST_CONTENT_TYPE)
					|| key.toString().contains(PluginConstants.WEB_REQUEST_CONTENT_LENGTH) || key.toString().contains(
					PluginConstants.WEB_REQUEST_CONTENT)))
				b.putString(key.toString(), parms.get(key).toString());
			else {
				Log.w(TAG, "Illegal request param (ignoring): " + key);
			}
		}
		return b;
	}

	/*
	 * Utility that sends a failed response for requests that are missing parameters using the incoming callbackId.
	 */
	private void sendMissingParam(WebAppManager<String> wMgr, String callbackId, String... missingParams) {
		if (callbackId != null) {
			WebCallback callback = new WebCallback(wMgr, callbackId);
			try {
				StringBuilder params = new StringBuilder();
				for (String param : missingParams) {
					params.append(param + " ");
				}
				Log.w(TAG, "Missing some or all of these required parameters: " + params.toString());
				callback.onFailure("Missing some or all of these required parameters: " + params.toString(),
						ErrorCodes.MISSING_PARAMETERS);
			} catch (RemoteException e) {
				Log.w(TAG, "Exception sending onFailure: " + e);
			}
		} else
			Log.w(TAG, "Web call was missing params and did not provide a callback!");
	}

	/*
	 * Utility for sending onFailure to web clients using the incoming callbackId.
	 */
	private void sendSuccess(WebAppManager<String> wMgr, String callbackId) {
		if (callbackId != null) {
			WebCallback callback = new WebCallback(wMgr, callbackId);
			try {
				callback.onSuccess();
			} catch (RemoteException e) {
				Log.w(TAG, "Exception sending sendSuccess: " + e);
			}
		}
	}

	/*
	 * Utility for sending onFailure to web clients using the incoming callbackId.
	 */
	private void sendFailure(WebAppManager<String> wMgr, String callbackId, String errorMessage, int errorCode) {
		if (callbackId != null) {
			WebCallback callback = new WebCallback(wMgr, callbackId);
			try {
				callback.onFailure(errorMessage, errorCode);
			} catch (RemoteException e) {
				Log.w(TAG, "Exception sending onFailure: " + e);
			}
		}
	}

	/*
	 * Utility that extracts a comma separated list of plugin ids into a list of VersionedPlugin. If a single plug-in is
	 * present, the optionalVersionString is used to optionally set the version on a single plug-in; however, if
	 * multiple plug-ins are present, they are unversioned, meaning the latest version of each will be used.
	 */
	private List<VersionedPlugin> getVersionedPlugins(String pluginIdString, String optionalVersionString) {
		List<VersionedPlugin> plugs = new ArrayList<VersionedPlugin>();
		if (pluginIdString.contains(",")) {
			// Remove whitespace and non visible characters such as tab
			pluginIdString = pluginIdString.replaceAll("\\s+", "");
			// Split out the plug-in ids
			for (String plugin : pluginIdString.split("\\,")) {
				plugs.add(new VersionedPlugin(plugin));
			}
		} else {
			// VersionedPlugin is null safe for pluginVersion
			plugs.add(new VersionedPlugin(pluginIdString, optionalVersionString));
		}
		return plugs;
	}
}
