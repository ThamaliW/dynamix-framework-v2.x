/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextPluginInformation;

/**
 * Callback for context support installation.
 *
 * @author Darren Carlson
 */
interface IContextSupportCallback
{
	/**
	 * Notification that context support has been added.
	 * @param supportInfo Information about the context support that was added.
	 */
	oneway void onSuccess(in ContextSupportInfo supportInfo);
	
	/**
	 * Notification of operation progress.
	 * @param percentComplete The progress percentage (0-100).
	 */
	oneway void onProgress(in int percentComplete);
	
	/**
	 * Notification that the operation generated warnings.
	 * @param message The warning message.
	 * @param errorCode The warning code (see Dynamix error codes for details).
	 */
	oneway void onWarning(in String message, in int errorCode);
	
	/**
	 * Notification that the operation failed.
	 * @param message The error message.
	 * @param errorCode The error code (see Dynamix error codes for details).
	 */
	oneway void onFailure(in String message, in int errorCode);
}
