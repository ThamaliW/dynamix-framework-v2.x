/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Provides read-only repository information.
 * 
 * @author Darren Carlson
 *
 */
public class RepositoryInfo implements Parcelable {
	public static String SIMPLE_FILE_SOURCE = "SIMPLE_FILE_SOURCE";
	public static String SIMPLE_NETWORK_SOURCE = "SIMPLE_NETWORK_SOURCE";
	public static String NEXUS_LUCENE_SOURCE = "NEXUS_LUCENE_SOURCE";
	public static String NEXUS_INDEX_SOURCE = "NEXUS_INDEX_SOURCE";
	private String id;
	private String alias;
	private String url;
	private String type;
	private boolean enabled;
	private boolean dynamixRepo;

	/**
	 * Creates a RepositoryInfo.
	 */
	public RepositoryInfo(String id, String alias, String url, String type, boolean enabled, boolean dynamixRepo) {
		this.id = id;
		this.alias = alias;
		this.url = url;
		this.type = type;
		this.enabled = enabled;
		this.dynamixRepo = dynamixRepo;
	}

	/**
	 * Returns the id of this repository.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Returns the alias of the server (i.e. a descriptive server name used for display to the user).
	 */
	public String getAlias() {
		return this.alias;
	}

	/**
	 * Returns the URL of the server.
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Returns the type of the server.
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Returns true if this is an official Dynamix repo; false otherwise.
	 */
	public boolean isDynamixRepo() {
		return this.dynamixRepo;
	}

	/**
	 * Returns true if this repo is enabled; false otherwise.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Returns true if this source is on a network; false otherwise (e.g., file-system).
	 */
	public boolean isNetworkSource() {
		return !type.equalsIgnoreCase(SIMPLE_FILE_SOURCE);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(id);
		out.writeString(alias);
		out.writeString(url);
		out.writeString(type);
		Utils.writeBoolean(out, enabled);
		Utils.writeBoolean(out, dynamixRepo);
	}

	private RepositoryInfo(Parcel in) {
		this.id = in.readString();
		this.alias = in.readString();
		this.url = in.readString();
		this.type = in.readString();
		this.enabled = Utils.readBoolean(in);
		this.dynamixRepo = Utils.readBoolean(in);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// Check for null
		if (candidate == null)
			return false;
		// Determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Check if the urls are the same
		RepositoryInfo other = (RepositoryInfo) candidate;
		if (other.getId().equalsIgnoreCase(this.getId()))
			return true;
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + getId().hashCode();
		return result;
	}

	/**
	 * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
	 */
	public static final Parcelable.Creator<RepositoryInfo> CREATOR = new Parcelable.Creator<RepositoryInfo>() {
		@Override
		public RepositoryInfo createFromParcel(Parcel in) {
			try {
				return new RepositoryInfo(in);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public RepositoryInfo[] newArray(int size) {
			return new RepositoryInfo[size];
		}
	};
}
