/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.RemoteException;

/**
 * Base implementation of ISessionListener.Stub with no functionality. Override methods as needed.
 *
 * @author Darren Carlson
 * @see ISessionListener
 */
public class SessionListener extends ISessionListener.Stub {
    private ISessionListener injected;

    public SessionListener() {
    }

    public SessionListener(ISessionListener injected) {
        this.injected = injected;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onSessionOpened(String sessionId) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onSessionOpened(sessionId);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onSessionClosed() throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onSessionClosed();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onDynamixFrameworkActive() throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onDynamixFrameworkActive();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onDynamixFrameworkInactive() throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onDynamixFrameworkInactive();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextPluginDiscoveryStarted() throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextPluginDiscoveryStarted();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextPluginDiscoveryFinished() throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextPluginDiscoveryFinished();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextPluginError(ContextPluginInformation plugin, String message, int errorCode)
            throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextPluginError(plugin, message, errorCode);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextPluginEnabled(ContextPluginInformation plugin) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextPluginEnabled(plugin);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextPluginDisabled(ContextPluginInformation plugin) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextPluginDisabled(plugin);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextPluginInstalled(ContextPluginInformation plugin) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextPluginInstalled(plugin);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextPluginUninstalled(ContextPluginInformation plugin) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextPluginUninstalled(plugin);
    }
}
