/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * A reference to a piece of runnable code that plug-ins can provide to be executed directly within the Dynamix UI. Note
 * that the specified runtime method *must* be be a part of the plug-ins runtime class.
 * 
 * @author Darren Carlson
 *
 */
public class DynamixFeature implements Parcelable {
	private String plugId = "";
	private VersionInfo version;
	private String name = "";
	private String description = "";
	private String runtimeMethod = "";
	private List<String> categories = new ArrayList<String>();

	/**
	 * Creates a DynamixFeature.
	 */
	public DynamixFeature(String plugId, VersionInfo version, String name, String description, String runtimeMethod) {
		this.plugId = plugId;
		this.name = name;
		this.version = version;
		this.description = description;
		this.runtimeMethod = runtimeMethod;
	}

	/**
	 * Creates a DynamixFeature.
	 */
	public DynamixFeature(String plugId, VersionInfo version, String name, String description, String runtimeMethod,
			List<String> categories) {
		this(plugId, version, name, description, runtimeMethod);
		if (categories != null)
			this.categories = categories;
	}

	/**
	 * Creates a DynamixFeature.
	 */
	public DynamixFeature(String plugId, VersionInfo version, String name, String description, String runtimeMethod,
			String categoryString) {
		this(plugId, version, name, description, runtimeMethod);
		/*
		 * Parse incoming categories. May be a CSV.
		 */
		if (categories != null && !categories.isEmpty()) {
			for (String cat : categoryString.split("\\,")) {
				categories.add(cat);
			}
		}
	}

	/**
	 * Returns this features runtime method. Note that this method must be part of the plug-in's runtime class.
	 */
	public String getRuntimeMethod() {
		return runtimeMethod;
	}

	/**
	 * Returns the plug-in id associated to this feature.
	 */
	public String getPlugId() {
		return plugId;
	}

	/**
	 * Returns the plug-in's version associated to this feature.
	 */
	public VersionInfo getPluginVersion() {
		return version;
	}

	/**
	 * Returns the friendly name of this feature.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the description of this feature.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the extras associated with this feature.
	 */
	public List<String> getCategories() {
		return this.categories;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(plugId);
		out.writeParcelable(version, 0);
		out.writeString(name);
		out.writeString(description);
		out.writeString(runtimeMethod);
		out.writeList(categories);
	}

	/**
	 * Creates a DynamixFeature from a Parcel.
	 */
	private DynamixFeature(Parcel in) {
		plugId = in.readString();
		version = in.readParcelable(getClass().getClassLoader());
		name = in.readString();
		description = in.readString();
		runtimeMethod = in.readString();
		in.readList(categories, null);
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
	 */
	public static final Parcelable.Creator<DynamixFeature> CREATOR = new Parcelable.Creator<DynamixFeature>() {
		@Override
		public DynamixFeature createFromParcel(Parcel in) {
			return new DynamixFeature(in);
		}

		@Override
		public DynamixFeature[] newArray(int size) {
			return new DynamixFeature[size];
		}
	};
}