/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtaa copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Wrapper for IContextHandler instances that provides the method overloading that raw AIDL types lack.
 *
 * @author Darren Carlson and Max Pagel
 * @see IContextHandler
 */
public class ContextHandler implements Parcelable {
    public static Parcelable.Creator<ContextHandler> CREATOR = new Parcelable.Creator<ContextHandler>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
         * previously been written by Parcelable.writeToParcel().
         */
        public ContextHandler createFromParcel(Parcel in) {
            return new ContextHandler(in);
        }

        /**
         * Create a new array of the Parcelable class.
         */
        public ContextHandler[] newArray(int size) {
            return new ContextHandler[size];
        }
    };
    // The local IContextHandler instance
    private IContextHandler handler;
    private UUID handlerId = UUID.randomUUID();

    /**
     * Returns the associated IContextHandler wrapped by this facade.
     */
    public IContextHandler getContextHandler() {
        return handler;
    }

    /**
     * Returns this handler's unique id.
     */
    public UUID getId() {
        return this.handlerId;
    }

    /**
     * Creates a ContextHandlerFacade for the handler.
     */
    public ContextHandler(IContextHandler handler) {
        this.handler = handler;
    }

    /**
     * Adds context support for the specified context type using the specified plug-and listener.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param listener    The context listener.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(String pluginId, String contextType, IContextListener listener,
                                  IContextSupportCallback callback) throws RemoteException {
        getContextHandler().addPluginContextSupportWithListenerAndCallback(pluginId, contextType, listener, callback);
    }

    /**
     * Adds context support for the specified context type using the specified plug-and listener.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param listener    The context listener.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(String pluginId, String contextType, ContextListener listener,
                                  ContextSupportCallback callback) throws RemoteException {
        getContextHandler().addPluginContextSupportWithListenerAndCallback(pluginId, contextType, listener, callback);
    }

    /**
     * Adds context support for the specified context type using the specified plug-in.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(String pluginId, String contextType, IContextSupportCallback callback)
            throws RemoteException {
        getContextHandler().addPluginContextSupportWithCallback(pluginId, contextType, callback);
    }

    /**
     * Adds context support for the specified context type using the specified plug-in.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(String pluginId, String contextType, ContextSupportCallback callback)
            throws RemoteException {
        getContextHandler().addPluginContextSupportWithCallback(pluginId, contextType, callback);
    }

    /**
     * Adds context support for the specified context type using the specified plug-and listener.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param listener    The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(String pluginId, String contextType, IContextListener listener)
            throws RemoteException {
        return getContextHandler().addPluginContextSupportWithListener(pluginId, contextType, listener);
    }

    /**
     * Adds context support for the specified context type using the specified plug-and listener.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param listener    The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(String pluginId, String contextType, ContextListener listener)
            throws RemoteException {
        return getContextHandler().addPluginContextSupportWithListener(pluginId, contextType, listener);
    }

    /**
     * Adds context support for the specified context type using the specified plug-in.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(String pluginId, String contextType) throws RemoteException {
        return getContextHandler().addPluginContextSupport(pluginId, contextType);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plug-in. The results of this
     * method are returned via the callback. Context requests may be of several types and are plug-specific (see the
     * plug-documentation for details). For some plug-ins, a context request may returns specific contextual information
     * obtained from the environment. For other plug-ins, a context request may be a change the underlying contextual
     * situation (e.g., playing a media file on a nearby media renderer). See the plugin's documentation for
     * configuration options that can be included the contextConfig Bundle.
     *
     * @param pluginId       The id of the plugthat should perform the context request.
     * @param contextType    The type of context info to interact with.
     * @param config         A plug-specific Bundle of context request configuration options.
     * @param callback       The callback for the request.
     * @param statusCallback A callback for receiving context support installation status updates.
     */
    public void contextRequest(String pluginId, String contextType, Bundle config, IContextRequestCallback callback,
                               IContextSupportCallback statusCallback) throws RemoteException {
        getContextHandler().configuredPluginContextRequestWithCallbackAndStatus(pluginId, contextType, config,
                callback, statusCallback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plug-in. The results of this
     * method are returned via the callback. Context requests may be of several types and are plug-specific (see the
     * plug-documentation for details). For some plug-ins, a context request may returns specific contextual information
     * obtained from the environment. For other plug-ins, a context request may be a change the underlying contextual
     * situation (e.g., playing a media file on a nearby media renderer). See the plugin's documentation for
     * configuration options that can be included the contextConfig Bundle.
     *
     * @param pluginId       The id of the plugthat should perform the context request.
     * @param contextType    The type of context info to interact with.
     * @param config         A plug-specific Bundle of context request configuration options.
     * @param callback       The callback for the request.
     * @param statusCallback A callback for receiving context support installation status updates.
     */
    public void contextRequest(String pluginId, String contextType, Bundle config, ContextRequestCallback callback,
                               ContextSupportCallback statusCallback) throws RemoteException {
        getContextHandler().configuredPluginContextRequestWithCallbackAndStatus(pluginId, contextType, config,
                callback, statusCallback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plug-in. The results of this
     * method are returned via the callback. Context requests may be of several types and are plug-specific (see the
     * plug-documentation for details). For some plug-ins, a context request may returns specific contextual information
     * obtained from the environment. For other plug-ins, a context request may be a change the underlying contextual
     * situation (e.g., playing a media file on a nearby media renderer). See the plugin's documentation for
     * configuration options that can be included the contextConfig Bundle.
     *
     * @param pluginId    The id of the plugthat should perform the context request.
     * @param contextType The type of context info to interact with.
     * @param config      A plug-specific Bundle of context request configuration options.
     * @param callback    The callback for the request.
     */
    public void contextRequest(String pluginId, String contextType, Bundle config, IContextRequestCallback callback)
            throws RemoteException {
        getContextHandler().configuredPluginContextRequestWithCallback(pluginId, contextType, config, callback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plug-in. The results of this
     * method are returned via the callback. Context requests may be of several types and are plug-specific (see the
     * plug-documentation for details). For some plug-ins, a context request may returns specific contextual information
     * obtained from the environment. For other plug-ins, a context request may be a change the underlying contextual
     * situation (e.g., playing a media file on a nearby media renderer). See the plugin's documentation for
     * configuration options that can be included the contextConfig Bundle.
     *
     * @param pluginId    The id of the plugthat should perform the context request.
     * @param contextType The type of context info to interact with.
     * @param config      A plug-specific Bundle of context request configuration options.
     * @param callback    The callback for the request.
     */
    public void contextRequest(String pluginId, String contextType, Bundle config, ContextRequestCallback callback)
            throws RemoteException {
        getContextHandler().configuredPluginContextRequestWithCallback(pluginId, contextType, config, callback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plugin. If you need results,
     * use a context request method with a callback. Context requests may be of several types and are plug-specific (see
     * the plug-documentation for details). For some plug-ins, a context request may returns specific contextual
     * information obtained from the environment. For other plug-ins, a context request may be a change the underlying
     * contextual situation (e.g., playing a media file on a nearby media renderer). See the plugin's documentation for
     * configuration options that can be included the contextConfig Bundle.
     *
     * @param pluginId    The id of the plugthat should perform the context request.
     * @param contextType The type of context info to interact with.
     * @param config      A plug-specific Bundle of context request configuration options.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public void contextRequest(String pluginId, String contextType, Bundle config) throws RemoteException {
        getContextHandler().configuredPluginContextRequest(pluginId, contextType, config);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugand contextType. This method will automatically install context support, if needed. The results of this
     * method are returned via the callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-types).
     *
     * @param pluginId       The id of the plugthat should perform the context request.
     * @param contextType    The type of context info to interact with.
     * @param callback       The callback for the request.
     * @param statusCallback A callback for receiving context support installation status updates.
     */
    public void contextRequest(String pluginId, String contextType, IContextRequestCallback callback,
                               IContextSupportCallback statusCallback) throws RemoteException {
        getContextHandler().pluginContextRequestWithCallbackAndStatus(pluginId, contextType, callback, statusCallback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugand contextType. This method will automatically install context support, if needed. The results of this
     * method are returned via the callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-types).
     *
     * @param pluginId       The id of the plugthat should perform the context request.
     * @param contextType    The type of context info to interact with.
     * @param callback       The callback for the request.
     * @param statusCallback A callback for receiving context support installation status updates.
     */
    public void contextRequest(String pluginId, String contextType, ContextRequestCallback callback,
                               IContextSupportCallback statusCallback) throws RemoteException {
        getContextHandler().pluginContextRequestWithCallbackAndStatus(pluginId, contextType, callback, statusCallback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugand contextType. This method will automatically install context support, if needed. The results of this
     * method are returned via the callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-types).
     *
     * @param pluginId    The id of the plugthat should perform the context request.
     * @param contextType The type of context info to interact with.
     * @param callback    The callback for the request.
     */
    public void contextRequest(String pluginId, String contextType, IContextRequestCallback callback)
            throws RemoteException {
        getContextHandler().pluginContextRequestWithCallback(pluginId, contextType, callback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugand contextType. This method will automatically install context support, if needed. The results of this
     * method are returned via the callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-types).
     *
     * @param pluginId    The id of the plugthat should perform the context request.
     * @param contextType The type of context info to interact with.
     * @param callback    The callback for the request.
     */
    public void contextRequest(String pluginId, String contextType, ContextRequestCallback callback)
            throws RemoteException {
        getContextHandler().pluginContextRequestWithCallback(pluginId, contextType, callback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugand contextType. This method will automatically install context support, if needed. If you need results, use
     * a context request method with a callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-types).
     *
     * @param pluginId    The id of the plugthat should perform the context request.
     * @param contextType The type of context info to interact with.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result contextRequest(String pluginId, String contextType) throws RemoteException {
        return getContextHandler().pluginContextRequest(pluginId, contextType);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugin and contextType. This method will automatically install context support, if needed. If you need results,
     * use a context request method with a callback. Note that this method is only available for ContextPlugins that
     * support programmatic access (i.e., reactive context plug-in types).
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result contextRequest(String pluginId, String pluginVersion, String contextType) throws RemoteException {
        return getContextHandler().versionedPluginContextRequest(pluginId, pluginVersion, contextType);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugin and contextType. This method will automatically install context support, if needed. The results of this
     * method are returned via the callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-in types).
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @param callback      The callback for the request.
     */
    public void contextRequest(String pluginId, String pluginVersion, String contextType,
                               IContextRequestCallback callback) throws RemoteException {
        getContextHandler().versionedPluginContextRequestWithCallback(pluginId, pluginVersion, contextType, callback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugin and contextType. This method will automatically install context support, if needed. The results of this
     * method are returned via the callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-in types).
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @param callback      The callback for the request.
     */
    public void contextRequest(String pluginId, String pluginVersion, String contextType,
                               ContextRequestCallback callback) throws RemoteException {
        getContextHandler().versionedPluginContextRequestWithCallback(pluginId, pluginVersion, contextType, callback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugin and contextType. This method will automatically install context support, if needed. The results of this
     * method are returned via the callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-in types).
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @param callback      The callback for the request.
     * @param status        A callback for receiving context support installation status updates.
     */
    public void contextRequest(String pluginId, String pluginVersion, String contextType,
                               IContextRequestCallback callback, IContextSupportCallback status) throws RemoteException {
        getContextHandler().versionedPluginContextRequestWithCallbackAndStatus(pluginId, pluginVersion, contextType,
                callback, status);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified
     * plugin and contextType. This method will automatically install context support, if needed. The results of this
     * method are returned via the callback. Note that this method is only available for ContextPlugins that support
     * programmatic access (i.e., reactive context plug-in types).
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @param callback      The callback for the request.
     * @param status        A callback for receiving context support installation status updates.
     */
    public void contextRequest(String pluginId, String pluginVersion, String contextType,
                               IContextRequestCallback callback, ContextSupportCallback status) throws RemoteException {
        getContextHandler().versionedPluginContextRequestWithCallbackAndStatus(pluginId, pluginVersion, contextType,
                callback, status);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plugin. If you need results,
     * use a context request method with a callback. Context requests may be of several types and are plug-in specific
     * (see the plug-in documentation for details). For some plug-ins, a context request may returns specific contextual
     * information obtained from the environment. For other plug-ins, a context request may be a change in the
     * underlying contextual situation (e.g., playing a media file on a nearby media renderer). See the plugin's
     * documentation for configuration options that can be included in the contextConfig Bundle.
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param contextType   The type of context info to interact with.
     * @param pluginVersion The plug-in version to use.
     * @param contextConfig A plug-in specific Bundle of context request configuration options.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result contextRequest(String pluginId, String pluginVersion, String contextType, Bundle contextConfig)
            throws RemoteException {
        return getContextHandler().configuredVersionedPluginContextRequest(pluginId, pluginVersion, contextType,
                contextConfig);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plug-in. The results of this
     * method are returned via the callback. Context requests may be of several types and are plug-in specific (see the
     * plug-in documentation for details). For some plug-ins, a context request may returns specific contextual
     * information obtained from the environment. For other plug-ins, a context request may be a change in the
     * underlying contextual situation (e.g., playing a media file on a nearby media renderer). See the plugin's
     * documentation for configuration options that can be included in the contextConfig Bundle.
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @param contextConfig A plug-in specific Bundle of context request configuration options.
     * @param callback      The callback for the request.
     */
    public void contextRequest(String pluginId, String pluginVersion, String contextType, Bundle contextConfig,
                               IContextRequestCallback callback) throws RemoteException {
        getContextHandler().configuredVersionedPluginContextRequestWithCallback(pluginId, pluginVersion, contextType,
                contextConfig, callback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plug-in. The results of this
     * method are returned via the callback. Context requests may be of several types and are plug-in specific (see the
     * plug-in documentation for details). For some plug-ins, a context request may returns specific contextual
     * information obtained from the environment. For other plug-ins, a context request may be a change in the
     * underlying contextual situation (e.g., playing a media file on a nearby media renderer). See the plugin's
     * documentation for configuration options that can be included in the contextConfig Bundle.
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @param contextConfig A plug-in specific Bundle of context request configuration options.
     * @param callback      The callback for the request.
     */
    public void contextRequest(String pluginId, String pluginVersion, String contextType, Bundle contextConfig,
                               ContextRequestCallback callback) throws RemoteException {
        getContextHandler().configuredVersionedPluginContextRequestWithCallback(pluginId, pluginVersion, contextType,
                contextConfig, callback);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plug-in. The results of this
     * method are returned via the callback. Context requests may be of several types and are plug-in specific (see the
     * plug-in documentation for details). For some plug-ins, a context request may returns specific contextual
     * information obtained from the environment. For other plug-ins, a context request may be a change in the
     * underlying contextual situation (e.g., playing a media file on a nearby media renderer). See the plugin's
     * documentation for configuration options that can be included in the contextConfig Bundle.
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @param contextConfig A plug-in specific Bundle of context request configuration options.
     * @param callback      The callback for the request.
     * @param status        A callback for receiving context support installation status updates.
     */
    public void contextRequest(String pluginId, String pluginVersion, String contextType, Bundle contextConfig,
                               IContextRequestCallback callback, IContextSupportCallback status) throws RemoteException {
        getContextHandler().configuredVersionedPluginContextRequestWithCallbackAndStatus(pluginId, pluginVersion,
                contextType, contextConfig, callback, status);
    }

    /**
     * Requests that Dynamix perform a dedicated context interaction using the specified plug-in. The results of this
     * method are returned via the callback. Context requests may be of several types and are plug-in specific (see the
     * plug-in documentation for details). For some plug-ins, a context request may returns specific contextual
     * information obtained from the environment. For other plug-ins, a context request may be a change in the
     * underlying contextual situation (e.g., playing a media file on a nearby media renderer). See the plugin's
     * documentation for configuration options that can be included in the contextConfig Bundle.
     *
     * @param pluginId      The id of the plugin that should perform the context request.
     * @param pluginVersion The plug-in version to use.
     * @param contextType   The type of context info to interact with.
     * @param contextConfig A plug-in specific Bundle of context request configuration options.
     * @param callback      The callback for the request.
     * @param status        A callback for receiving context support installation status updates.
     */
    public void contextRequest(String pluginId, String pluginVersion, String contextType, Bundle contextConfig,
                               IContextRequestCallback callback, ContextSupportCallback status) throws RemoteException {
        getContextHandler().configuredVersionedPluginContextRequestWithCallbackAndStatus(pluginId, pluginVersion,
                contextType, contextConfig, callback, status);
    }

    /**
     * Removes all context support.
     *
     * @param callback An optional callback that can be used to track the success or failure of this request.
     */
    public void removeAllContextSupport(ICallback callback) throws RemoteException {
        getContextHandler().removeAllContextSupportWithCallback(callback);
    }

    /**
     * Removes all context support.
     *
     * @param callback An optional callback that can be used to track the success or failure of this request.
     */
    public void removeAllContextSupport(Callback callback) throws RemoteException {
        getContextHandler().removeAllContextSupportWithCallback(callback);
    }

    /**
     * Removes all context support for the specified contextType.
     *
     * @param contextType The context support type to remove.
     */
    public void removeContextSupport(String contextType) throws RemoteException {
        getContextHandler().removeContextSupportForContextType(contextType);
    }

    /**
     * Removes previously added context support.
     *
     * @param contextSupportInfo The context support to remove.
     * @param callback           An optional callback that can be used to track the success or failure of this request.
     */
    public void removeContextSupport(ContextSupportInfo contextSupportInfo, ICallback callback) throws RemoteException {
        getContextHandler().removeContextSupportWithCallback(contextSupportInfo, callback);
    }

    /**
     * Removes previously added context support.
     *
     * @param contextSupportInfo The context support to remove.
     * @param callback           An optional callback that can be used to track the success or failure of this request.
     */
    public void removeContextSupport(ContextSupportInfo contextSupportInfo, Callback callback) throws RemoteException {
        getContextHandler().removeContextSupportWithCallback(contextSupportInfo, callback);
    }

    /**
     * Removes all context support for the specified contextType.
     *
     * @param contextType The context support type to remove.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void removeContextSupport(String contextType, ICallback callback) throws RemoteException {
        getContextHandler().removeContextSupportForContextTypeWithCallback(contextType, callback);
    }

    /**
     * Removes all context support for the specified contextType.
     *
     * @param contextType The context support type to remove.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void removeContextSupport(String contextType, Callback callback) throws RemoteException {
        getContextHandler().removeContextSupportForContextTypeWithCallback(contextType, callback);
    }

    /**
     * Resends all ContextEvents (of the specified contextType) that have been cached by Dynamix for the listener.
     * ContextEvents are provided to applications according to Dynamix authentication and security policies defined by
     * Dynamix users. Note that the requesting application will only received context info they are subscribed to.
     * Events from this method are returned via this listener's onContextEvent method.
     *
     * @param contextListener The listener to re-send context events for.
     * @param contextType     The type of ContextEvent to return.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result resendAllCachedContextEvents(IContextListener contextListener, String contextType) throws RemoteException {
        return getContextHandler().resendAllTypedCachedContextEvents(contextListener, contextType);
    }

    /**
     * Resends all ContextEvents (of the specified contextType) that have been cached by Dynamix for the listener.
     * ContextEvents are provided to applications according to Dynamix authentication and security policies defined by
     * Dynamix users. Note that the requesting application will only received context info they are subscribed to.
     * Events from this method are returned via this listener's onContextEvent method.
     *
     * @param contextListener The listener to re-send context events for.
     * @param contextType     The type of ContextEvent to return.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result resendAllCachedContextEvents(ContextListener contextListener, String contextType) throws RemoteException {
        return getContextHandler().resendAllTypedCachedContextEvents(contextListener, contextType);
    }

    /**
     * Resends all ContextEvents (of the specified contextType) that have been cached by Dynamix for the listener.
     * ContextEvents are provided to applications according to Dynamix authentication and security policies defined by
     * Dynamix users. Note that the requesting application will only received context info they are subscribed to.
     * Events from this method are returned via this listener's onContextEvent method.
     *
     * @param contextListener The listener to re-send context events for.
     * @param contextType     The type of ContextEvent to return.
     * @param milliseconds    Time window for retrieving cached context events (in milliseconds)
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result resendCachedContextEvents(IContextListener contextListener, String contextType, int milliseconds)
            throws RemoteException {
        return getContextHandler().resendTypedCachedContextEvents(contextListener, contextType, milliseconds);
    }

    /**
     * Resends all ContextEvents (of the specified contextType) that have been cached by Dynamix for the listener.
     * ContextEvents are provided to applications according to Dynamix authentication and security policies defined by
     * Dynamix users. Note that the requesting application will only received context info they are subscribed to.
     * Events from this method are returned via this listener's onContextEvent method.
     *
     * @param contextListener The listener to re-send context events for.
     * @param contextType     The type of ContextEvent to return.
     * @param milliseconds    Time window for retrieving cached context events (in milliseconds)
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result resendCachedContextEvents(ContextListener contextListener, String contextType, int milliseconds)
            throws RemoteException {
        return getContextHandler().resendTypedCachedContextEvents(contextListener, contextType, milliseconds);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(String pluginId, String contextType, Bundle config) throws RemoteException {
        return getContextHandler().addPluginConfiguredContextSupport(pluginId, contextType, config);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(List<String> pluginIds, String contextType, Bundle config) throws RemoteException {
        return getContextHandler().addPluginsConfiguredContextSupport(pluginIds, contextType, config);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param callback    A callback that can be used to track the success or failure of this request.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public void addContextSupport(List<String> pluginIds, String contextType, Bundle config,
                                  IContextSupportCallback callback) throws RemoteException {
        getContextHandler().addPluginsConfiguredContextSupportWithCallback(pluginIds, contextType, config, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param callback    A callback that can be used to track the success or failure of this request.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public void addContextSupport(List<String> pluginIds, String contextType, Bundle config,
                                  ContextSupportCallback callback) throws RemoteException {
        getContextHandler().addPluginsConfiguredContextSupportWithCallback(pluginIds, contextType, config, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(String pluginId, String contextType, Bundle config, IContextSupportCallback callback)
            throws RemoteException {
        getContextHandler().addPluginConfiguredContextSupportWithCallback(pluginId, contextType, config, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(String pluginId, String contextType, Bundle config, ContextSupportCallback callback)
            throws RemoteException {
        getContextHandler().addPluginConfiguredContextSupportWithCallback(pluginId, contextType, config, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param listener    The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(String pluginId, String contextType, Bundle config, IContextListener listener)
            throws RemoteException {
        return getContextHandler().addPluginConfiguredContextSupportWithListener(pluginId, contextType, config,
                listener);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param listener    The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(String pluginId, String contextType, Bundle config, ContextListener listener)
            throws RemoteException {
        return getContextHandler().addPluginConfiguredContextSupportWithListener(pluginId, contextType, config,
                listener);
    }

    /**
     * Adds context support using the specified plug-in, context type, configuration Bundle and listener.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param listener    The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(List<String> pluginIds, String contextType, Bundle config, IContextListener listener)
            throws RemoteException {
        return getContextHandler().addPluginsConfiguredContextSupportWithListener(pluginIds, contextType, config,
                listener);
    }

    /**
     * Adds context support using the specified plug-in, context type, configuration Bundle and listener.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param listener    The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(List<String> pluginIds, String contextType, Bundle config, ContextListener listener)
            throws RemoteException {
        return getContextHandler().addPluginsConfiguredContextSupportWithListener(pluginIds, contextType, config,
                listener);
    }

    /**
     * Adds context support using the specified plug-in, context type and listener.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param listener    The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(List<String> pluginIds, String contextType, IContextListener listener)
            throws RemoteException {
        return getContextHandler().addPluginsConfiguredContextSupportWithListener(pluginIds, contextType, null,
                listener);
    }

    /**
     * Adds context support using the specified plug-in, context type and listener.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param listener    The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result addContextSupport(List<String> pluginIds, String contextType, ContextListener listener)
            throws RemoteException {
        return getContextHandler().addPluginsConfiguredContextSupportWithListener(pluginIds, contextType, null,
                listener);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param listener    The context listener.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(String pluginId, String contextType, Bundle config, IContextListener listener,
                                  IContextSupportCallback callback) throws RemoteException {
        getContextHandler().addPluginConfiguredContextSupportWithListenerAndCallback(pluginId, contextType, config,
                listener, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId    The plug-to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param listener    The context listener.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(String pluginId, String contextType, Bundle config, ContextListener listener,
                                  ContextSupportCallback callback) throws RemoteException {
        getContextHandler().addPluginConfiguredContextSupportWithListenerAndCallback(pluginId, contextType, config,
                listener, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param listener    The context listener.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(List<String> pluginIds, String contextType, Bundle config, IContextListener listener,
                                  IContextSupportCallback callback) throws RemoteException {
        getContextHandler().addPluginsConfiguredContextSupportWithListenerAndCallback(pluginIds, contextType, config,
                listener, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginIds   The plug-ins to use.
     * @param contextType A String describing the requested context type. Note that the contextType string must reference a
     *                    valid context type string (as described the developer documentation).
     * @param config      A Bundle describing the requested context support. See the developer documentation for a description
     *                    of available configuration options.
     * @param listener    The context listener.
     * @param callback    A callback that can be used to track the success or failure of this request.
     */
    public void addContextSupport(List<String> pluginIds, String contextType, Bundle config, ContextListener listener,
                                  ContextSupportCallback callback) throws RemoteException {
        getContextHandler().addPluginsConfiguredContextSupportWithListenerAndCallback(pluginIds, contextType, config,
                listener, callback);
    }

    /**
     * Adds context support for the specified context type using the specified plug-in.
     *
     * @param pluginId      The plug-to use.
     * @param pluginVersion The plug-version to use.
     * @param contextType   A String describing the requested context type. Note that the contextType string must reference a
     *                      valid context type string (as described the developer documentation).
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addContextSupport(String pluginId, String pluginVersion, String contextType) throws RemoteException {
        return getContextHandler().addVersionedPluginContextSupport(pluginId, pluginVersion, contextType);
    }

    /**
     * Adds context support for the specified context type using the specified plug-in.
     *
     * @param pluginId      The plug-to use.
     * @param pluginVersion The plug-version to use.
     * @param contextType   A String describing the requested context type. Note that the contextType string must reference a
     *                      valid context type string (as described the developer documentation).
     * @param callback      A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addContextSupport(String pluginId, String pluginVersion, String contextType,
                                  IContextSupportCallback callback) throws RemoteException {
        getContextHandler()
                .addVersionedPluginContextSupportWithCallback(pluginId, pluginVersion, contextType, callback);
    }

    /**
     * Adds context support for the specified context type using the specified plug-in.
     *
     * @param pluginId      The plug-to use.
     * @param pluginVersion The plug-version to use.
     * @param contextType   A String describing the requested context type. Note that the contextType string must reference a
     *                      valid context type string (as described the developer documentation).
     * @param callback      A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addContextSupport(String pluginId, String pluginVersion, String contextType,
                                  ContextSupportCallback callback) throws RemoteException {
        getContextHandler()
                .addVersionedPluginContextSupportWithCallback(pluginId, pluginVersion, contextType, callback);
    }

    /**
     * Adds context support for the specified context type using the specified plug-and listener.
     *
     * @param pluginId      The plug-to use.
     * @param pluginVersion The plug-version to use.
     * @param contextType   A String describing the requested context type. Note that the contextType string must reference a
     *                      valid context type string (as described the developer documentation).
     * @param listener      The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addContextSupport(String pluginId, String pluginVersion, String contextType, IContextListener listener)
            throws RemoteException {
        return getContextHandler().addVersionedPluginContextSupportWithListener(pluginId, pluginVersion, contextType,
                listener);
    }

    /**
     * Adds context support for the specified context type using the specified plug-and listener.
     *
     * @param pluginId      The plug-to use.
     * @param pluginVersion The plug-version to use.
     * @param contextType   A String describing the requested context type. Note that the contextType string must reference a
     *                      valid context type string (as described the developer documentation).
     * @param listener      The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addContextSupport(String pluginId, String pluginVersion, String contextType, ContextListener listener)
            throws RemoteException {
        return getContextHandler().addVersionedPluginContextSupportWithListener(pluginId, pluginVersion, contextType,
                listener);
    }

    /**
     * Adds context support for the specified context type using the specified plug-and listener.
     *
     * @param pluginId      The plug-to use.
     * @param pluginVersion The plug-version to use.
     * @param contextType   A String describing the requested context type. Note that the contextType string must reference a
     *                      valid context type string (as described the developer documentation).
     * @param listener      The context listener.
     * @param callback      A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addContextSupport(String pluginId, String pluginVersion, String contextType, IContextListener listener,
                                  IContextSupportCallback callback) throws RemoteException {
        getContextHandler().addVersionedPluginContextSupportWithListenerAndCallback(pluginId, pluginVersion,
                contextType, listener, callback);
    }

    /**
     * Adds context support for the specified context type using the specified plug-and listener.
     *
     * @param pluginId      The plug-to use.
     * @param pluginVersion The plug-version to use.
     * @param contextType   A String describing the requested context type. Note that the contextType string must reference a
     *                      valid context type string (as described the developer documentation).
     * @param listener      The context listener.
     * @param callback      A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addContextSupport(String pluginId, String pluginVersion, String contextType, ContextListener listener,
                                  ContextSupportCallback callback) throws RemoteException {
        getContextHandler().addVersionedPluginContextSupportWithListenerAndCallback(pluginId, pluginVersion,
                contextType, listener, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId             The plug-to use.
     * @param pluginVersion        The plug-version to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addVersionedContextSupport(String pluginId, String pluginVersion, String contextType,
                                             Bundle contextSupportConfig) throws RemoteException {
        return getContextHandler().addVersionedPluginConfiguredContextSupport(pluginId, pluginVersion, contextType,
                contextSupportConfig);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId             The plug-to use.
     * @param pluginVersion        The plug-version to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param callback             A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addVersionedContextSupport(String pluginId, String pluginVersion, String contextType,
                                           Bundle contextSupportConfig, IContextSupportCallback callback) throws RemoteException {
        getContextHandler().addVersionedPluginConfiguredContextSupportWithCallback(pluginId, pluginVersion,
                contextType, contextSupportConfig, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId             The plug-to use.
     * @param pluginVersion        The plug-version to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param callback             A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addVersionedContextSupport(String pluginId, String pluginVersion, String contextType,
                                           Bundle contextSupportConfig, ContextSupportCallback callback) throws RemoteException {
        getContextHandler().addVersionedPluginConfiguredContextSupportWithCallback(pluginId, pluginVersion,
                contextType, contextSupportConfig, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId             The plug-to use.
     * @param pluginVersion        The plug-version to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param listener             The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addVersionedContextSupport(String pluginId, String pluginVersion, String contextType,
                                             Bundle contextSupportConfig, IContextListener listener) throws RemoteException {
        return getContextHandler().addVersionedPluginConfiguredContextSupportWithListener(pluginId, pluginVersion,
                contextType, contextSupportConfig, listener);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId             The plug-to use.
     * @param pluginVersion        The plug-version to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param listener             The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addVersionedContextSupport(String pluginId, String pluginVersion, String contextType,
                                             Bundle contextSupportConfig, ContextListener listener) throws RemoteException {
        return getContextHandler().addVersionedPluginConfiguredContextSupportWithListener(pluginId, pluginVersion,
                contextType, contextSupportConfig, listener);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId             The plug-to use.
     * @param pluginVersion        The plug-version to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param listener             The context listener.
     * @param callback             A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addVersionedContextSupport(String pluginId, String pluginVersion, String contextType,
                                           Bundle contextSupportConfig, IContextListener listener, IContextSupportCallback callback)
            throws RemoteException {
        getContextHandler().addVersionedPluginConfiguredContextSupportWithListenerAndCallback(pluginId, pluginVersion,
                contextType, contextSupportConfig, listener, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param pluginId             The plug-to use.
     * @param pluginVersion        The plug-version to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param listener             The context listener.
     * @param callback             A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addVersionedContextSupport(String pluginId, String pluginVersion, String contextType,
                                           Bundle contextSupportConfig, ContextListener listener, ContextSupportCallback callback)
            throws RemoteException {
        getContextHandler().addVersionedPluginConfiguredContextSupportWithListenerAndCallback(pluginId, pluginVersion,
                contextType, contextSupportConfig, listener, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param plugins              The plug-ins to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addVersionedContextSupport(List<VersionedPlugin> plugins, String contextType,
                                             Bundle contextSupportConfig) throws RemoteException {
        return getContextHandler().addVersionedPluginsConfiguredContextSupport(plugins, contextType,
                contextSupportConfig);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param plugins              The plug-ins to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param callback             A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addVersionedContextSupport(List<VersionedPlugin> plugins, String contextType,
                                           Bundle contextSupportConfig, IContextSupportCallback callback) throws RemoteException {
        getContextHandler().addVersionedPluginsConfiguredContextSupportWithCallback(plugins, contextType,
                contextSupportConfig, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param plugins              The plug-ins to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param callback             A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addVersionedContextSupport(List<VersionedPlugin> plugins, String contextType,
                                           Bundle contextSupportConfig, ContextSupportCallback callback) throws RemoteException {
        getContextHandler().addVersionedPluginsConfiguredContextSupportWithCallback(plugins, contextType,
                contextSupportConfig, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param plugins              The plug-ins to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param listener             The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addVersionedContextSupport(List<VersionedPlugin> plugins, String contextType,
                                             Bundle contextSupportConfig, IContextListener listener) throws RemoteException {
        return getContextHandler().addVersionedPluginsConfiguredContextSupportWithListener(plugins, contextType,
                contextSupportConfig, listener);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param plugins              The plug-ins to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param listener             The context listener.
     * @return A Result indicating if Dynamix accepted the request or not.
     * @throws RemoteException
     */
    public Result addVersionedContextSupport(List<VersionedPlugin> plugins, String contextType,
                                             Bundle contextSupportConfig, ContextListener listener) throws RemoteException {
        return getContextHandler().addVersionedPluginsConfiguredContextSupportWithListener(plugins, contextType,
                contextSupportConfig, listener);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param plugins              The plug-ins to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param listener             The context listener.
     * @param callback             A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addVersionedContextSupport(List<VersionedPlugin> plugins, String contextType,
                                           Bundle contextSupportConfig, IContextListener listener, IContextSupportCallback callback)
            throws RemoteException {
        getContextHandler().addVersionedPluginsConfiguredContextSupportWithListenerAndCallback(plugins, contextType,
                contextSupportConfig, listener, callback);
    }

    /**
     * Adds context support using the specified plug-in, context type and configuration Bundle.
     *
     * @param plugins              The plug-ins to use.
     * @param contextType          A String describing the requested context type. Note that the contextType string must reference a
     *                             valid context type string (as described the developer documentation).
     * @param contextSupportConfig A Bundle describing the requested context support. See the developer documentation for a description
     *                             of available configuration options.
     * @param listener             The context listener.
     * @param callback             A callback that can be used to track the success or failure of this request.
     * @throws RemoteException
     */
    public void addVersionedContextSupport(List<VersionedPlugin> plugins, String contextType,
                                           Bundle contextSupportConfig, ContextListener listener, ContextSupportCallback callback)
            throws RemoteException {
        getContextHandler().addVersionedPluginsConfiguredContextSupportWithListenerAndCallback(plugins, contextType,
                contextSupportConfig, listener, callback);
    }

    /**
     * Resends all ContextEvents that have been cached by Dynamix for the listener. ContextEvents are provided to
     * applications according to Dynamix authentication and security policies defined by Dynamix users. Note that the
     * requesting application will only received context info they are subscribed to. Events from this method are
     * returned via this listener's onContextEvent method.
     *
     * @param listener The listener to re-send context events for.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result resendAllCachedContextEvents(IContextListener listener) throws RemoteException {
        return getContextHandler().resendAllCachedContextEvents(listener);
    }

    /**
     * Resends all ContextEvents that have been cached by Dynamix for the listener. ContextEvents are provided to
     * applications according to Dynamix authentication and security policies defined by Dynamix users. Note that the
     * requesting application will only received context info they are subscribed to. Events from this method are
     * returned via this listener's onContextEvent method.
     *
     * @param listener The listener to re-send context events for.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result resendAllCachedContextEvents(ContextListener listener) throws RemoteException {
        return getContextHandler().resendAllCachedContextEvents(listener);
    }

    /**
     * Resends the ContextEvent entities that have been cached for the listener withthe specified past number of
     * milliseconds. If the number of milliseconds is longer than the max cache time, then all cached events are
     * returned. ContextEvents are provided to applications according to Dynamix authentication and security policies.
     * Note that the requesting application will only received context info they are subscribed to. Events from this
     * method are returned via this listener's onContextEvent method.
     *
     * @param listener      The listener to re-send context events for.
     * @param previousMills The time (milliseconds) to retrieve past events.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result resendCachedContextEvents(IContextListener listener, int previousMills) throws RemoteException {
        return getContextHandler().resendCachedContextEvents(listener, previousMills);
    }

    /**
     * Resends the ContextEvent entities that have been cached for the listener withthe specified past number of
     * milliseconds. If the number of milliseconds is longer than the max cache time, then all cached events are
     * returned. ContextEvents are provided to applications according to Dynamix authentication and security policies.
     * Note that the requesting application will only received context info they are subscribed to. Events from this
     * method are returned via this listener's onContextEvent method.
     *
     * @param listener      The listener to re-send context events for.
     * @param previousMills The time (milliseconds) to retrieve past events.
     * @return A Result indicating if Dynamix accepted the request or not.
     */
    public Result resendCachedContextEvents(ContextListener listener, int previousMills) throws RemoteException {
        return getContextHandler().resendCachedContextEvents(listener, previousMills);
    }

    /**
     * Returns the context support that has been registered by this handler (as a result).
     */
    public ContextSupportResult getContextSupportResult() throws RemoteException {
        return getContextHandler().getContextSupport();
    }

    /**
     * Returns the context support that has been registered by this handler (as a list).
     */
    public List<ContextSupportInfo> getContextSupport() throws RemoteException {
        ContextSupportResult result = getContextHandler().getContextSupport();
        if (result != null)
            return result.getContextSupportInfo();
        else
            return new ArrayList<ContextSupportInfo>();
    }

    /**
     * Returns the list of registered context listeners (as a result).
     */
    public IContextListenerResult getContextListenerResult() throws RemoteException {
        return getContextHandler().getContextListenerResult();
    }

    /**
     * Returns the list of registered context listeners (as a list).
     */
    public List<IContextListener> getContextListeners() throws RemoteException {
        IContextListenerResult result = getContextHandler().getContextListenerResult();
        if (result != null)
            return result.getIContextListenerList();
        else
            return new ArrayList<IContextListener>();
    }

    /**
     * Removes all previously added context support for all listeners, regardless of contextType.
     */
    public void removeAllContextSupport() throws RemoteException {
        getContextHandler().removeAllContextSupport();
    }

    /**
     * Removes previously added context support.
     *
     * @param info The context support to remove.
     */
    public void removeContextSupport(ContextSupportInfo info) throws RemoteException {
        getContextHandler().removeContextSupport(info);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStrongBinder(handler.asBinder());
    }

    // Creates a ContextHandlerFacade from the Parcel
    private ContextHandler(Parcel in) {
        IBinder binder = in.readStrongBinder();
        handler = IContextHandler.Stub.asInterface(binder);
    }
}
